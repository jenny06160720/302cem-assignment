package com.example.navigation

import android.content.Intent
import android.util.Log
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.runner.screenshot.Screenshot.capture
import com.example.navigation.ui.others.OtherPresenter
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import junit.framework.Assert
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotSame

@RunWith(AndroidJUnit4::class)
class OtherPresenterTest {
    private var presenter: OtherPresenter? = null

    @RelaxedMockK
    lateinit var mView: OtherPresenter.View

    @Before
    fun setUp() {
        MockKAnnotations.init(this);
        presenter = OtherPresenter(mView)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    //  1. Test method onClickParkingInfo
//    @Test
//    fun onClickRefuelRecord_success() {
//        //Given
//        var mockIntnet = slot<Intent>()
//        //When
//        presenter?.onClickRefuelRecord()
//        // Then
//        verify(exactly = 1) {
//            mView.showRefuelRecord(capture(mockIntnet))
//        }
//        assertEquals(mockIntnet.captured.toString(),
//                "Intent { cmp=com.example.navigation/.ui.others.refuel_record.RefuelRecord }")
//    }
//
//    @Test
//    fun onClickRefuelRecord_CompareNoEqual() {
//        //Given
//        var mockIntnet = slot<Intent>()
//        //When
//        presenter?.onClickRefuelRecord()
//        // Then
//        verify(exactly = 1) {
//            mView.showRefuelRecord(capture(mockIntnet))
//        }
//        Assert.assertNotSame(mockIntnet.captured.toString(),
//                "Intent { cmp=com.example.navigation/.ui.others.refuel_record.RefuelRecor ")
//    }

    //  2. Test method onClickParkingInfo
//    @Test
//    fun onClickMaintenanceRecord_success() {
//        //Given
//        var mockIntnet = slot<Intent>()
//        //When
//        presenter?.onClickMaintenanceRecord()
//        // Then
//        verify(exactly = 1) {
//            mView.showMaintenanceRecord(capture(mockIntnet))
//        }
//        assertEquals(mockIntnet.captured.toString(),
//                "Intent { cmp=com.example.navigation/.ui.others.maintenance_record.MaintenanceRecord }")
//    }
//
//    @Test
//    fun onClickMaintenanceRecord_CompareNoEqual() {
//        //Given
//        var mockIntnet = slot<Intent>()
//        //When
//        presenter?.onClickMaintenanceRecord()
//        // Then
//        verify(exactly = 1) {
//            mView.showMaintenanceRecord(capture(mockIntnet))
//        }
//        Assert.assertNotSame(mockIntnet.captured.toString(),
//                "Intent { cmp=com.example.navigation/.ui.others.maintenance_record.MaintenanceRecord ")
//    }

    //  3. Test method onClickParkingInfo
    @Test
    fun onClickLicenseInfo() {
        //Given
        var mockIntnet = slot<Intent>()
        //When
        presenter?.onClickLicenseInfo()
        // Then
        verify(exactly = 1) {
            mView.showLicenseInfo(capture(mockIntnet))
        }
    }

    @Test
    fun onClickLicenseInfo_success() {
        //Given
        var mockIntnet = slot<Intent>()
        //When
        presenter?.onClickLicenseInfo()
        // Then
        verify(exactly = 1) {
            mView.showLicenseInfo(capture(mockIntnet))
        }
        assertEquals(mockIntnet.captured.toString(),
                "Intent { cmp=com.example.navigation/.ui.others.vehicle_info.ViewAllRecord.VehicleInfo }")
    }

    @Test
    fun onClickLicenseInfo_CompareNoEqual() {
        //Given
        var mockIntnet = slot<Intent>()
        //When
        presenter?.onClickLicenseInfo()
        // Then
        verify(exactly = 1) {
            mView.showLicenseInfo(capture(mockIntnet))
        }
        Assert.assertNotSame(mockIntnet.captured.toString(),
                "Intent { cmp=com.example.navigation/.ui.others.car_detail.car_detail ")
    }

    //  4. Test method onClickParkingInfo
    @Test
    fun test_onClickParkingInfo() {
        //Given
        var mockIntnet = slot<Intent>()
        //When
        presenter?.onClickParkingInfo()
        // Then
        verify(exactly = 1) {
            mView.showParkingInfo(capture(mockIntnet))
        }
    }

    @Test
    fun test_onClickParkingInfo__success() {
        //Given
        var mockIntnet = slot<Intent>()
        //When
        presenter?.onClickParkingInfo()
        // Then
        verify(exactly = 1) {
            mView.showParkingInfo(capture(mockIntnet))
        }
        assertEquals(mockIntnet.captured.toString(),
                "Intent { cmp=com.example.navigation/.ui.others.parking_info.ParkingInfo }")
    }

    @Test
    fun test_onClickParkingInfo__CompareNotSame() {
        //Given
        var mockIntnet = slot<Intent>()
        //When
        presenter?.onClickParkingInfo()
        // Then
        verify(exactly = 1) {
            mView.showParkingInfo(capture(mockIntnet))
        }
        assertNotSame(mockIntnet.captured.toString(),
                "Intent { cmp=com.example.navigation/.ui.others.parking_info.ParkingInf }")
    }
}