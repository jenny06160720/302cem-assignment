package com.example.navigation


import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class UI_Create_New_Vehicle_Record {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Rule
    @JvmField
    var mGrantPermissionRule =
            GrantPermissionRule.grant(
                    "android.permission.ACCESS_FINE_LOCATION")

    @Test
    fun uI_Create_New_Vehicle_Record() {
        val bottomNavigationItemView = onView(
                allOf(withId(R.id.navigation_others), withContentDescription("OTHERS"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nav_view),
                                        0),
                                2),
                        isDisplayed()))
        bottomNavigationItemView.perform(click())

        val materialButton = onView(
                allOf(withId(R.id.bt_licenseInfo), withText("Vehicle Info"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nav_host_fragment),
                                        0),
                                0),
                        isDisplayed()))
        materialButton.perform(click())

        val materialButton2 = onView(
                allOf(withId(R.id.bt_Create), withText("Create"),
                        childAtPosition(
                                allOf(withId(R.id.layout_Create),
                                        childAtPosition(
                                                withId(R.id.activity_main),
                                                0)),
                                0),
                        isDisplayed()))
        materialButton2.perform(click())

        val appCompatEditText = onView(
                allOf(withId(R.id.et_licenseNumber),
                        childAtPosition(
                                allOf(withId(R.id.layout_licenseNumber),
                                        childAtPosition(
                                                withClassName(`is`("androidx.constraintlayout.widget.ConstraintLayout")),
                                                1)),
                                1),
                        isDisplayed()))
        appCompatEditText.perform(replaceText("123"), closeSoftKeyboard())

        val appCompatEditText2 = onView(
                allOf(withId(R.id.et_type),
                        childAtPosition(
                                allOf(withId(R.id.layout_type),
                                        childAtPosition(
                                                withClassName(`is`("androidx.constraintlayout.widget.ConstraintLayout")),
                                                2)),
                                1),
                        isDisplayed()))
        appCompatEditText2.perform(replaceText("123"), closeSoftKeyboard())

        val appCompatEditText3 = onView(
                allOf(withId(R.id.et_annualFee),
                        childAtPosition(
                                allOf(withId(R.id.layout_annualFee),
                                        childAtPosition(
                                                withClassName(`is`("androidx.constraintlayout.widget.ConstraintLayout")),
                                                3)),
                                1),
                        isDisplayed()))
        appCompatEditText3.perform(replaceText("1"), closeSoftKeyboard())

        val appCompatEditText4 = onView(
                allOf(withId(R.id.et_licenseExpiryDate),
                        childAtPosition(
                                allOf(withId(R.id.layout_licenseExpiryDate),
                                        childAtPosition(
                                                withClassName(`is`("androidx.constraintlayout.widget.ConstraintLayout")),
                                                4)),
                                1),
                        isDisplayed()))
        appCompatEditText4.perform(click())

        val materialButton3 = onView(
                allOf(withId(android.R.id.button1), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.ScrollView")),
                                        0),
                                3)))
        materialButton3.perform(scrollTo(), click())

        val appCompatEditText5 = onView(
                allOf(withId(R.id.et_licenseReminderDate),
                        childAtPosition(
                                allOf(withId(R.id.layout_licenseReminderDate),
                                        childAtPosition(
                                                withClassName(`is`("androidx.constraintlayout.widget.ConstraintLayout")),
                                                5)),
                                1),
                        isDisplayed()))
        appCompatEditText5.perform(click())

        val materialButton4 = onView(
                allOf(withId(android.R.id.button1), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.ScrollView")),
                                        0),
                                3)))
        materialButton4.perform(scrollTo(), click())

        val appCompatEditText6 = onView(
                allOf(withId(R.id.et_policyNumber),
                        childAtPosition(
                                allOf(withId(R.id.layout_policyNumber),
                                        childAtPosition(
                                                withClassName(`is`("androidx.constraintlayout.widget.ConstraintLayout")),
                                                7)),
                                1),
                        isDisplayed()))
        appCompatEditText6.perform(click())

        val appCompatEditText7 = onView(
                allOf(withId(R.id.et_policyNumber),
                        childAtPosition(
                                allOf(withId(R.id.layout_policyNumber),
                                        childAtPosition(
                                                withClassName(`is`("androidx.constraintlayout.widget.ConstraintLayout")),
                                                7)),
                                1),
                        isDisplayed()))
        appCompatEditText7.perform(replaceText("13212"), closeSoftKeyboard())

        val appCompatEditText8 = onView(
                allOf(withId(R.id.et_insuranceExpiryDate),
                        childAtPosition(
                                allOf(withId(R.id.layout_insuranceExpiryDate),
                                        childAtPosition(
                                                withClassName(`is`("androidx.constraintlayout.widget.ConstraintLayout")),
                                                8)),
                                1),
                        isDisplayed()))
        appCompatEditText8.perform(click())

        val materialButton5 = onView(
                allOf(withId(android.R.id.button1), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.ScrollView")),
                                        0),
                                3)))
        materialButton5.perform(scrollTo(), click())

        val appCompatEditText9 = onView(
                allOf(withId(R.id.et_insuranceReminderDate),
                        childAtPosition(
                                allOf(withId(R.id.layout_insuranceReminderDate),
                                        childAtPosition(
                                                withClassName(`is`("androidx.constraintlayout.widget.ConstraintLayout")),
                                                9)),
                                1),
                        isDisplayed()))
        appCompatEditText9.perform(click())

        val materialButton6 = onView(
                allOf(withId(android.R.id.button1), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.ScrollView")),
                                        0),
                                3)))
        materialButton6.perform(scrollTo(), click())

        val appCompatEditText10 = onView(
                allOf(withId(R.id.et_lastEngineOilChange),
                        childAtPosition(
                                allOf(withId(R.id.layout_lastEngineOilChange),
                                        childAtPosition(
                                                withClassName(`is`("androidx.constraintlayout.widget.ConstraintLayout")),
                                                11)),
                                1),
                        isDisplayed()))
        appCompatEditText10.perform(click())

        val materialButton7 = onView(
                allOf(withId(android.R.id.button1), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.ScrollView")),
                                        0),
                                3)))
        materialButton7.perform(scrollTo(), click())

        val appCompatEditText11 = onView(
                allOf(withId(R.id.et_remindEngineOilChange),
                        childAtPosition(
                                allOf(withId(R.id.layout_remindEngineOilChange),
                                        childAtPosition(
                                                withClassName(`is`("androidx.constraintlayout.widget.ConstraintLayout")),
                                                12)),
                                1),
                        isDisplayed()))
        appCompatEditText11.perform(click())

        val materialButton8 = onView(
                allOf(withId(android.R.id.button1), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.ScrollView")),
                                        0),
                                3)))
        materialButton8.perform(scrollTo(), click())

        val appCompatEditText12 = onView(
                allOf(withId(R.id.et_lastGearboxOilChange),
                        childAtPosition(
                                allOf(withId(R.id.layout_lastGearboxOilChange),
                                        childAtPosition(
                                                withClassName(`is`("androidx.constraintlayout.widget.ConstraintLayout")),
                                                13)),
                                1),
                        isDisplayed()))
        appCompatEditText12.perform(click())

        val materialButton9 = onView(
                allOf(withId(android.R.id.button1), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.ScrollView")),
                                        0),
                                3)))
        materialButton9.perform(scrollTo(), click())

        val appCompatEditText13 = onView(
                allOf(withId(R.id.et_remindGearboxOilChange),
                        childAtPosition(
                                allOf(withId(R.id.layout_remindGearboxOilChange),
                                        childAtPosition(
                                                withClassName(`is`("androidx.constraintlayout.widget.ConstraintLayout")),
                                                14)),
                                1),
                        isDisplayed()))
        appCompatEditText13.perform(click())

        val materialButton10 = onView(
                allOf(withId(android.R.id.button1), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.ScrollView")),
                                        0),
                                3)))
        materialButton10.perform(scrollTo(), click())

        val materialButton11 = onView(
                allOf(withId(R.id.bt_Create), withText("Create Record"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                15),
                        isDisplayed()))
        materialButton11.perform(click())

        val materialButton12 = onView(
                allOf(withId(android.R.id.button1), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.buttonPanel),
                                        0),
                                3)))
        materialButton12.perform(scrollTo(), click())
    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
