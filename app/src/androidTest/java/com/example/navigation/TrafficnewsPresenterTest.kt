package com.example.navigation

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.navigation.ui.news.Trafficnews
import com.example.navigation.ui.news.TrafficnewsPresenter
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import junit.framework.Assert.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class TrafficnewsPresenterTest {
    private var presenter: TrafficnewsPresenter? = null

    @RelaxedMockK
    lateinit var mView: TrafficnewsPresenter.View

    lateinit var model: Trafficnews

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        model = Trafficnews()
        presenter = TrafficnewsPresenter(model, mView)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    // 1. test method callAPI
    @Test
    fun test_callAPI() {
//        // Given
//        val capture = slot<Trafficnews>()
//        every { model.getXmlData(presenter)} just runs
//        // When
        presenter?.callAPI();
//        // Then
//        verify(exactly = 1) {
//            capture.captured.incidenT_DETAIL_CN
//            println("Trafficnews: ${presenter?.incidenT_DETAIL_CN}")
//        }
    }

    // 2. test method setINCIDENT_NUMBER
    @Test
    fun test_ShowINCIDENT_NUMBER_success() {
//        println("model before presenter  $model")
        // Given`
        println("vView $mView")
        val mockINCIDENT_NUMBER = "IN-20-04553"
        // when
        presenter?.setINCIDENT_NUMBER(mockINCIDENT_NUMBER)
//        println("model after presenter  $model")
        // Then
        assertEquals(presenter?.incidenT_NUMBER, mockINCIDENT_NUMBER)
        println("test_ShowINCIDENT_NUMBER_success: ${presenter?.incidenT_NUMBER}")
        verify(exactly = 1) {
            mView.updatetvNewsNo(mockINCIDENT_NUMBER)
        }
    }

    @Test
    fun test_ShowINCIDENT_NUMBER_CompareNoEqual() {
        // Given`
        val mockINCIDENT_NUMBER = "IN-20-04553"
        // when
        presenter?.setINCIDENT_NUMBER(mockINCIDENT_NUMBER)
        // Then
        assertNotSame(presenter?.incidenT_NUMBER, "IN-20-04554")
        println("test_ShowINCIDENT_NUMBER_CompareNoequal: ${presenter?.incidenT_NUMBER}")
        verify(exactly = 1) {
            mView.updatetvNewsNo(mockINCIDENT_NUMBER)
        }
    }

    @Test
    fun test_ShowINCIDENT_NUMBER_IsNull() {
        // Given
        val mockINCIDENT_NUMBER = null
        // when
        presenter?.setINCIDENT_NUMBER(mockINCIDENT_NUMBER)
        // Then
        assertEquals(presenter?.incidenT_NUMBER, null)
        println("test_ShowINCIDENT_NUMBER_IsNull: ${presenter?.incidenT_NUMBER}")
        verify(exactly = 1) {
            mView.updatetvNewsNo(mockINCIDENT_NUMBER)
        }
    }

//    @Test
//    fun test_getINCIDENT_NUMBER_Case_ModelIsNull(){
//        // Given
//        trafficNewPresenter_ModelIsNull()
//        val mockINCIDENT_NUMBER = null
//        // When
//        println("Model is null = $model")
//        presenter?.setINCIDENT_NUMBER(mockINCIDENT_NUMBER)
//        // Then
//        verify(exactly = 1) {
//            mView.updatetvNewsNo(mockINCIDENT_NUMBER)
//        }
//    }

    // 3. test method setINCIDENT_HEADING_CN
//    @Test
//    fun test_setINCIDENT_HEADING_CN_success() {
//        // Given`
//        println("vView $mView")
//        val mockINCIDENT_HEADING_CN = "道路事故"
//        // when
//        presenter?.setINCIDENT_HEADING_CN(mockINCIDENT_HEADING_CN)
//        // Then
//        assertEquals(presenter?.incidenT_HEADING_CN, mockINCIDENT_HEADING_CN)
//        println("test_setINCIDENT_HEADING_CN_success: ${presenter?.incidenT_HEADING_CN}")
//        verify(exactly = 1) {
//            mView.updatetvNewsHeader_CN(mockINCIDENT_HEADING_CN)
//        }
//    }
//
//    @Test
//    fun test_setINCIDENT_HEADING_CN_CompareNoEqual() {
//        // Given`
//        val mockINCIDENT_HEADING_CN = "道路事故"
//        // when
//        presenter?.setINCIDENT_HEADING_CN(mockINCIDENT_HEADING_CN)
//        // Then
//        assertNotSame(presenter?.incidenT_HEADING_CN, "道路事故一")
//        println("test_setINCIDENT_HEADING_CN_CompareNoEqual: ${presenter?.incidenT_HEADING_CN}")
//        verify(exactly = 1) {
//            mView.updatetvNewsHeader_CN(mockINCIDENT_HEADING_CN)
//        }
//    }
//
//    @Test
//    fun test_setINCIDENT_HEADING_CN_IsNull() {
//        // Given
//        val mockINCIDENT_HEADING_CN = null
//        // when
//        presenter?.setINCIDENT_HEADING_CN(mockINCIDENT_HEADING_CN)
//        // Then
//        assertEquals(presenter?.incidenT_HEADING_CN, null)
//        println("test_setINCIDENT_HEADING_CN_IsNull: ${presenter?.incidenT_HEADING_CN}")
//        verify(exactly = 1) {
//            mView.updatetvNewsHeader_CN(mockINCIDENT_HEADING_CN)
//        }
//    }

    // 4. test method setINCIDENT_HEADING_EN
    @Test
    fun test_setINCIDENT_HEADING_EN_success() {
        // Given`
        println("vView $mView")
        val test = slot<String>()
        val mockINCIDENT_HEADING_EN = "Road Incident"
        println("test: ${presenter?.incidenT_HEADING_EN}")
        // when
        presenter?.setINCIDENT_HEADING_EN(mockINCIDENT_HEADING_EN)
        // Then
        assertEquals(presenter?.incidenT_HEADING_EN, mockINCIDENT_HEADING_EN)
        println("test_setINCIDENT_HEADING_EN_success: ${presenter?.incidenT_HEADING_EN}")
        verify(exactly = 1) {
            mView.updatetvNewsHeader_EN("$mockINCIDENT_HEADING_EN ")
        }
    }

    @Test
    fun test_setINCIDENT_HEADING_EN_CompareNoEqual() {
        // Given`
        val mockINCIDENT_HEADING_EN = "Road Incident "
        // when
        presenter?.setINCIDENT_HEADING_EN(mockINCIDENT_HEADING_EN)
        // Then
        assertNotSame(presenter?.incidenT_HEADING_EN, "Road Inciden ")
        println("test_setINCIDENT_HEADING_EN_CompareNoEqual: ${presenter?.incidenT_HEADING_EN}")
        verify(exactly = 1) {
            mView.updatetvNewsHeader_EN("$mockINCIDENT_HEADING_EN ")
        }
    }

    @Test
    fun test_setINCIDENT_HEADING_EN_IsNull() {
        // Given
        val mockINCIDENT_HEADING_EN = null
        // when
        presenter?.setINCIDENT_HEADING_EN(mockINCIDENT_HEADING_EN)
        // Then
        assertEquals(presenter?.incidenT_HEADING_EN, null)
        println("test_setINCIDENT_HEADING_EN_IsNull: ${presenter?.incidenT_HEADING_EN}")
        verify(exactly = 0) {
            mView.updatetvNewsHeader_EN(mockINCIDENT_HEADING_EN)
        }
    }

    // 5. test method setINCIDENT_DETAIL_CN
//    @Test
//    fun test_setINCIDENT_DETAIL_CN_success() {
//        // Given`
//        println("vView $mView")
//        val test = slot<String>()
//        val mockINCIDENT_DETAIL_CN = "Traffic Accident"
//        println("test: ${presenter?.incidenT_DETAIL_CN}")
//        // when
//        presenter?.setINCIDENT_DETAIL_CN(mockINCIDENT_DETAIL_CN)
//        // Then
//        assertEquals(presenter?.incidenT_DETAIL_CN, mockINCIDENT_DETAIL_CN)
//        println("test_setINCIDENT_DETAIL_CN_success: ${presenter?.incidenT_DETAIL_CN}")
//        verify(exactly = 1) {
//            mView.updatetvNewsDetail_CN(mockINCIDENT_DETAIL_CN)
//        }
//    }
//
//    @Test
//    fun test_setINCIDENT_DETAIL_CN_CompareNoEqual() {
//        // Given`
//        val mockINCIDENT_DETAIL_CN = "Traffic Accident"
//        // when
//        presenter?.setINCIDENT_DETAIL_CN(mockINCIDENT_DETAIL_CN)
//        // Then
//        assertNotSame(presenter?.incidenT_DETAIL_CN, "Traffic Acciden")
//        println("test_setINCIDENT_DETAIL_CN_CompareNoEqual: ${presenter?.incidenT_DETAIL_CN}")
//        verify(exactly = 1) {
//            mView.updatetvNewsDetail_CN(mockINCIDENT_DETAIL_CN)
//        }
//    }
//
//    @Test
//    fun test_setINCIDENT_DETAIL_CN_IsNull() {
//        // Given
//        val mockINCIDENT_DETAIL_CN = null
//        // when
//        presenter?.setINCIDENT_DETAIL_CN(mockINCIDENT_DETAIL_CN)
//        // Then
//        assertEquals(presenter?.incidenT_DETAIL_CN, null)
//        println("test_setINCIDENT_DETAIL_CN_IsNull: ${presenter?.incidenT_DETAIL_CN}")
//        verify(exactly = 1) {
//            mView.updatetvNewsDetail_CN(mockINCIDENT_DETAIL_CN)
//        }
//    }

    // 6. test method setINCIDENT_DETAIL_EN
    @Test
    fun test_setINCIDENT_DETAIL_EN_success() {
        // Given`
        println("vView $mView")
        val test = slot<String>()
        val mockINCIDENT_DETAIL_EN = "Road Incident"
        println("test: ${presenter?.incidenT_DETAIL_EN}")
        // when
        presenter?.setINCIDENT_DETAIL_EN(mockINCIDENT_DETAIL_EN)
        // Then
        assertEquals(presenter?.incidenT_DETAIL_EN, mockINCIDENT_DETAIL_EN)
        println("test_setINCIDENT_DETAIL_EN_success: ${presenter?.incidenT_DETAIL_EN}")
        verify(exactly = 1) {
            mView.updatetvNewsDetail_EN("$mockINCIDENT_DETAIL_EN ")
        }
    }

    @Test
    fun test_setINCIDENT_DETAIL_EN_CompareNoEqual() {
        // Given`
        val mockINCIDENT_DETAIL_EN = "Road Incident"
        // when
        presenter?.setINCIDENT_DETAIL_EN(mockINCIDENT_DETAIL_EN)
        // Then
        assertNotSame(presenter?.incidenT_DETAIL_EN, "Road Inciden ")
        println("test_setINCIDENT_DETAIL_EN_CompareNoEqual: ${presenter?.incidenT_DETAIL_EN}")
        verify(exactly = 1) {
            mView.updatetvNewsDetail_EN("$mockINCIDENT_DETAIL_EN ")
        }
    }

    @Test
    fun test_setINCIDENT_DETAIL_EN_IsNull() {
        // Given
        val mockINCIDENT_DETAIL_EN = null
        // when
        presenter?.setINCIDENT_DETAIL_EN(mockINCIDENT_DETAIL_EN)
        // Then
        assertEquals(presenter?.incidenT_DETAIL_EN, null)
        println("test_setINCIDENT_DETAIL_EN_IsNull: ${presenter?.incidenT_DETAIL_EN}")
        verify(exactly = 0) {
            mView.updatetvNewsDetail_EN(mockINCIDENT_DETAIL_EN)
        }
    }

    // 7. test method setANNOUNCEMENT_DATE
    @Test
    fun test_setANNOUNCEMENT_DATE_success() {
        // Given`
        println("vView $mView")
        val test = slot<String>()
        val mockANNOUNCEMENT_DATE = "2020-12-29T18:11:00"
        println("test: ${presenter?.announcemenT_DATE}")
        // when
        presenter?.setANNOUNCEMENT_DATE(mockANNOUNCEMENT_DATE)
        // Then
        assertEquals(presenter?.announcemenT_DATE, mockANNOUNCEMENT_DATE)
        println("test_setANNOUNCEMENT_DATE_success: ${presenter?.announcemenT_DATE}")
        verify(exactly = 1) {
            mView.updatetvNewsDate(mockANNOUNCEMENT_DATE)
        }
    }

    @Test
    fun test_setANNOUNCEMENT_DATE_CompareNoEqual() {
        // Given`
        val mockANNOUNCEMENT_DATE = "2020-12-29T18:11:00"
        // when
        presenter?.setANNOUNCEMENT_DATE(mockANNOUNCEMENT_DATE)
        // Then
        assertNotSame(presenter?.announcemenT_DATE, "2020-12-29T18:11:0")
        println("test_setANNOUNCEMENT_DATE_CompareNoEqual: ${presenter?.announcemenT_DATE}")
        verify(exactly = 1) {
            mView.updatetvNewsDate(mockANNOUNCEMENT_DATE)
        }
    }

    @Test
    fun test_setANNOUNCEMENT_DATE_IsNull() {
        // Given
        val mockANNOUNCEMENT_DATE = null
        // when
        presenter?.setANNOUNCEMENT_DATE(mockANNOUNCEMENT_DATE)
        // Then
        assertEquals(presenter?.announcemenT_DATE, null)
        println("test_setANNOUNCEMENT_DATE_IsNull: ${presenter?.announcemenT_DATE}")
        verify(exactly = 1) {
            mView.updatetvNewsDate(mockANNOUNCEMENT_DATE)
        }
    }

    // 8. test method setCONTENT_CN
//    @Test
//    fun test_setCONTENT_CN_success() {
//        // Given`
//        println("vView $mView")
//        val mockCONTENT_CN = "Road Incident"
//        println("test: ${presenter?.contenT_CN}")
//        // when
//        presenter?.setCONTENT_CN(mockCONTENT_CN)
//        // Then
//        assertEquals(presenter?.contenT_CN, mockCONTENT_CN)
//        println("test_setCONTENT_CN_success: ${presenter?.contenT_CN}")
//        verify(exactly = 1) {
//            mView.updatetvNewsContent_CN("\n$mockCONTENT_CN")
//        }
//    }
//
//    @Test
//    fun test_setCONTENT_CN_CompareNoEqual() {
//        // Given`
//        val mockCONTENT_CN = "Road Incident"
//        // when
//        presenter?.setCONTENT_CN(mockCONTENT_CN)
//        // Then
//        assertNotSame(presenter?.contenT_CN, "Road Inciden ")
//        println("test_setCONTENT_CN_CompareNoEqual: ${presenter?.contenT_CN}")
//        verify(exactly = 1) {
//            mView.updatetvNewsContent_CN("\n$mockCONTENT_CN")
//        }
//    }
//
//    @Test
//    fun test_setCONTENT_CN_IsNull() {
//        // Given
//        val mockCONTENT_CN = null
//        // when
//        presenter?.setCONTENT_CN(mockCONTENT_CN)
//        // Then
//        assertEquals(presenter?.contenT_CN, null)
//        println("test_setCONTENT_CN_IsNull: ${presenter?.contenT_CN}")
//        verify(exactly = 0) {
//            mView.updatetvNewsContent_CN(mockCONTENT_CN)
//        }
//    }

    // 9. test method setCONTENT_EN
    @Test
    fun test_setCONTENT_EN_success() {
        // Given`
        println("vView $mView")
        val mockCONTENT_EN = "Road Incident"
        println("test: ${presenter?.contenT_EN}")
        // when
        presenter?.setCONTENT_EN(mockCONTENT_EN)
        // Then
        assertEquals(presenter?.contenT_EN, mockCONTENT_EN)
        println("test_setCONTENT_EN_success: ${presenter?.contenT_EN}")
        verify(exactly = 1) {
            mView.updatetvNewsContent_EN("\n$mockCONTENT_EN")
        }
    }

    @Test
    fun test_setCONTENT_EN_CompareNoEqual() {
        // Given`
        val mockCONTENT_EN = "Road Incident"
        // when
        presenter?.setCONTENT_EN(mockCONTENT_EN)
        // Then
        assertNotSame(presenter?.contenT_EN, "Road Inciden ")
        println("test_setCONTENT_EN_CompareNoEqual: ${presenter?.contenT_EN}")
        verify(exactly = 1) {
            mView.updatetvNewsContent_EN("\n$mockCONTENT_EN")
        }
    }

    @Test
    fun test_setCONTENT_EN_IsNull() {
        // Given
        val mockCONTENT_EN = null
        // when
        presenter?.setCONTENT_EN(mockCONTENT_EN)
        // Then
        assertEquals(presenter?.contenT_EN, null)
        println("test_setCONTENT_EN_IsNull: ${presenter?.contenT_EN}")
        verify(exactly = 0) {
            mView.updatetvNewsContent_EN(mockCONTENT_EN)
        }
    }

    // 10. test method setResponseCodeError
    @Test
    fun test_setResponseCodeError_caseErrorCode_100() {
        // Given
        val errorCode = 100
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "100 Continue")
        println("test_setResponseCodeError_caseErrorCode_100: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_101() {
        // Given
        val errorCode = 101
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "101 Switching Protocol")
        println("test_setResponseCodeError_caseErrorCode_101: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_102() {
        // Given
        val errorCode = 102
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "102 Processing")
        println("test_setResponseCodeError_caseErrorCode_102: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_103() {
        // Given
        val errorCode = 103
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "103 Early Hints")
        println("test_setResponseCodeError_caseErrorCode_103: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_201() {
        // Given
        val errorCode = 201
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "201 Created")
        println("test_setResponseCodeError_caseErrorCode_201: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_202() {
        // Given
        val errorCode = 202
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "202 Accepted")
        println("test_setResponseCodeError_caseErrorCode_202: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_203() {
        // Given
        val errorCode = 203
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "203 Non-Authoritative Information")
        println("test_setResponseCodeError_caseErrorCode_203: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_204() {
        // Given
        val errorCode = 204
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "204 No Content")
        println("test_setResponseCodeError_caseErrorCode_204: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_205() {
        // Given
        val errorCode = 205
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "205 Reset Content")
        println("test_setResponseCodeError_caseErrorCode_205: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_206() {
        // Given
        val errorCode = 206
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "206 Partial Content")
        println("test_setResponseCodeError_caseErrorCode_206: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_207() {
        // Given
        val errorCode = 207
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "207 Multi-Status (WebDAV)")
        println("test_setResponseCodeError_caseErrorCode_207: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_208() {
        // Given
        val errorCode = 208
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "208 Multi-Status (WebDAV)")
        println("test_setResponseCodeError_caseErrorCode_208: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_226() {
        // Given
        val errorCode = 226
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "226 IM Used (HTTP Delta encoding)")
        println("test_setResponseCodeError_caseErrorCode_226: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_300() {
        // Given
        val errorCode = 300
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "300 Multiple Choice")
        println("test_setResponseCodeError_caseErrorCode_300: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_301() {
        // Given
        val errorCode = 301
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "301 Moved Permanently")
        println("test_setResponseCodeError_caseErrorCode_301: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_302() {
        // Given
        val errorCode = 302
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "302 Found")
        println("test_setResponseCodeError_caseErrorCode_302: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_303() {
        // Given
        val errorCode = 303
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "303 See Other")
        println("test_setResponseCodeError_caseErrorCode_303: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_304() {
        // Given
        val errorCode = 304
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "304 Not Modified")
        println("test_setResponseCodeError_caseErrorCode_304: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_305() {
        // Given
        val errorCode = 305
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "305 Use Proxy")
        println("test_setResponseCodeError_caseErrorCode_305: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_306() {
        // Given
        val errorCode = 306
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "306 unused")
        println("test_setResponseCodeError_caseErrorCode_306: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_307() {
        // Given
        val errorCode = 307
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "307 Temporary Redirect")
        println("test_setResponseCodeError_caseErrorCode_307: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_308() {
        // Given
        val errorCode = 308
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "308 Permanent Redirect")
        println("test_setResponseCodeError_caseErrorCode_308: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_400() {
        // Given
        val errorCode = 400
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "400 Bad Request")
        println("test_setResponseCodeError_caseErrorCode_400: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_401() {
        // Given
        val errorCode = 401
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "401 Unauthorized")
        println("test_setResponseCodeError_caseErrorCode_401: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_402() {
        // Given
        val errorCode = 402
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "402 Payment Required")
        println("test_setResponseCodeError_caseErrorCode_402: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_403() {
        // Given
        val errorCode = 403
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "403 Forbidden")
        println("test_setResponseCodeError_caseErrorCode_403: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_404() {
        // Given
        val errorCode = 404
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "404 Not Found")
        println("test_setResponseCodeError_caseErrorCode_404: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_405() {
        // Given
        val errorCode = 405
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "405 Method Not Allowed")
        println("test_setResponseCodeError_caseErrorCode_405: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_406() {
        // Given
        val errorCode = 406
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "406 Not Acceptable")
        println("test_setResponseCodeError_caseErrorCode_406: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_407() {
        // Given
        val errorCode = 407
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "407 Proxy Authentication Required")
        println("test_setResponseCodeError_caseErrorCode_407: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_408() {
        // Given
        val errorCode = 408
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "408 Request Timeout")
        println("test_setResponseCodeError_caseErrorCode_408: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_409() {
        // Given
        val errorCode = 409
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "409 Conflictt")
        println("test_setResponseCodeError_caseErrorCode_409: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_410() {
        // Given
        val errorCode = 410
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "410 Gone")
        println("test_setResponseCodeError_caseErrorCode_410: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_411() {
        // Given
        val errorCode = 411
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "411 Length Required")
        println("test_setResponseCodeError_caseErrorCode_411: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_412() {
        // Given
        val errorCode = 412
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "412 Precondition Failed")
        println("test_setResponseCodeError_caseErrorCode_412: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_413() {
        // Given
        val errorCode = 413
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "413 Payload Too Large")
        println("test_setResponseCodeError_caseErrorCode_413: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_414() {
        // Given
        val errorCode = 414
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "414 URI Too Long")
        println("test_setResponseCodeError_caseErrorCode_414: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_415() {
        // Given
        val errorCode = 415
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "415 Unsupported Media Type")
        println("test_setResponseCodeError_caseErrorCode_415: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_416() {
        // Given
        val errorCode = 416
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "416 Requested Range Not Satisfiable")
        println("test_setResponseCodeError_caseErrorCode_416: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_417() {
        // Given
        val errorCode = 417
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "417 Expectation Failed")
        println("test_setResponseCodeError_caseErrorCode_417: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_418() {
        // Given
        val errorCode = 418
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "418 I'm a teapot")
        println("test_setResponseCodeError_caseErrorCode_418: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_421() {
        // Given
        val errorCode = 421
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "421 Misdirected Request")
        println("test_setResponseCodeError_caseErrorCode_421: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_422() {
        // Given
        val errorCode = 422
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "422 Unprocessable Entity")
        println("test_setResponseCodeError_caseErrorCode_422: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_423() {
        // Given
        val errorCode = 423
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "423 Locked")
        println("test_setResponseCodeError_caseErrorCode_423: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_424() {
        // Given
        val errorCode = 424
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "424 Failed Dependency")
        println("test_setResponseCodeError_caseErrorCode_424: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_426() {
        // Given
        val errorCode = 426
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "426 Upgrade Required")
        println("test_setResponseCodeError_caseErrorCode_426: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_428() {
        // Given
        val errorCode = 428
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "428 Precondition Required")
        println("test_setResponseCodeError_caseErrorCode_428: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_429() {
        // Given
        val errorCode = 429
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "429 Too Many Requests")
        println("test_setResponseCodeError_caseErrorCode_429: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_431() {
        // Given
        val errorCode = 431
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "431 Request Header Fields Too Large")
        println("test_setResponseCodeError_caseErrorCode_431: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_451() {
        // Given
        val errorCode = 451
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "451 Unavailable For Legal Reasons")
        println("test_setResponseCodeError_caseErrorCode_451: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_500() {
        // Given
        val errorCode = 500
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "500 Internal Server Error")
        println("test_setResponseCodeError_caseErrorCode_500: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_501() {
        // Given
        val errorCode = 501
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "501 Not Implemented")
        println("test_setResponseCodeError_caseErrorCode_501: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_502() {
        // Given
        val errorCode = 502
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "502 Bad Gateway")
        println("test_setResponseCodeError_caseErrorCode_502: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_503() {
        // Given
        val errorCode = 503
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "503 Service Unavailable")
        println("test_setResponseCodeError_caseErrorCode_503: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_504() {
        // Given
        val errorCode = 504
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "504 Gateway Timeout")
        println("test_setResponseCodeError_caseErrorCode_504: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_505() {
        // Given
        val errorCode = 505
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "505 HTTP Version Not Supported")
        println("test_setResponseCodeError_caseErrorCode_505: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_506() {
        // Given
        val errorCode = 506
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "506 Variant Also Negotiates")
        println("test_setResponseCodeError_caseErrorCode_506: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_507() {
        // Given
        val errorCode = 507
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "507 Insufficient Storage")
        println("test_setResponseCodeError_caseErrorCode_507: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_508() {
        // Given
        val errorCode = 508
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "508 Loop Detected")
        println("test_setResponseCodeError_caseErrorCode_508: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_510() {
        // Given
        val errorCode = 510
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "510 Not Extended")
        println("test_setResponseCodeError_caseErrorCode_510: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_511() {
        // Given
        val errorCode = 511
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertEquals(presenter?.responseCodeError, "511 Network Authentication Required")
        println("test_setResponseCodeError_caseErrorCode_511: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_notsame() {
        // Given
        val errorCode = 511
        val num = 4
        val captureError = slot<String>()

        // When
        presenter?.setResponseCodeError(errorCode)

        // Then
        assertNotSame(presenter?.responseCodeError, "511 Network Authentication Require")
        println("test_setResponseCodeError_caseErrorCode_511: ${presenter?.responseCodeError}")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.responseCodeError, num)
        }
    }

    @Test
    fun test_setResponseCodeError_caseErrorCode_isnull() {
        // Given
        val errorCode = null
        val num = 4

        // When
        errorCode?.let { presenter?.setResponseCodeError(it) }

        // Then
        assertEquals(presenter?.responseCodeError, null)
        println("test_setResponseCodeError_caseErrorCode_511: ${presenter?.responseCodeError}")
        verify(exactly = 0) {
            mView.showErrorMessage(errorCode, num)
        }
    }


//    private fun trafficNewPresenter_withModel() {
//        presenter = TrafficnewsPresenter(mView, model)
//    }

//    private fun trafficNewPresenter_ModelIsNull() {
//        presenter = TrafficnewsPresenter(null, mView)
//    }

//    private fun trafficNewPresenter_viewIsNull() {
//        presenter = TrafficnewsPresenter(null)
//    }
}