package com.example.navigation

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.navigation.ui.home.favourite.CreateFavourite.FavouriteCreatePresenter
import com.example.navigation.ui.home.record.ViewRecord.RecordPresenter
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.impl.annotations.RelaxedMockK
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class FavouriteCreatePresenterTest {
    private var presenter: FavouriteCreatePresenter? = null

    @RelaxedMockK
    lateinit var recordPresenter: RecordPresenter

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        presenter = FavouriteCreatePresenter()
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    @Test
    fun test_callAPI_create(){
        // Given
        var mockmData = "s123"
        // When
        presenter?.callAPI_create(recordPresenter, mockmData)
        // Then
    }


}