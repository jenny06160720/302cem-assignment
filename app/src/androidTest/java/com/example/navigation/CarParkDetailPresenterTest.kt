package com.example.navigation

import androidx.test.espresso.NoMatchingViewException
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.navigation.ui.others.parking_info.CarParkDetailModel
import com.example.navigation.ui.others.parking_info.CarParkDetailPresenter
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import junit.framework.Assert
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotSame

@RunWith(AndroidJUnit4::class)
class CarParkDetailPresenterTest {
    private var presenter: CarParkDetailPresenter? = null

    @RelaxedMockK
    lateinit var mView: CarParkDetailPresenter.View

    lateinit var model: CarParkDetailModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this);
        presenter = CarParkDetailPresenter(mView)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    //    test method putIncoming
    @Test
    fun test_putIncoming() {
        // Given
        // When
        // Then
    }

    @Test
    fun test_updateTextView_basicInfo_success() {
        // Given
        var mockBasicInfo = "BasicInfo"

        // When
        presenter?.updateTextView_basicInfo(mockBasicInfo)

        // Then
        Assert.assertEquals(presenter?.basicInfo, mockBasicInfo)
        verify(exactly = 1) {
            mView.updateTextView(presenter?.basicInfo)
        }
    }

    @Test
    fun test_updateTextView_basicInfo_NotSame(){
        // Given
        var mockBasicInfo = "BasicInfo"

        // When
        presenter?.updateTextView_basicInfo(mockBasicInfo)

        // Then
        Assert.assertNotSame(presenter?.basicInfo, "BasicInf")
        verify(exactly = 1) {
            mView.updateTextView(presenter?.basicInfo)
        }
    }

    @Test
    fun test_updateTextView_basicInfo_IsNull() {
        // Given
        var mockBasicInfo = null

        // When
        presenter?.updateTextView_basicInfo(mockBasicInfo)

        // Then
        Assert.assertEquals(presenter?.basicInfo, null)
        verify(exactly = 1) {
            mView.updateTextView(presenter?.basicInfo)
        }
    }

    // test method updateTextView_vac
    @Test
    fun test_updateTextView_vac_success() {
        //Given
        var mockVac = "Vac"

        //When
        presenter?.updateTextView_vac(mockVac)

        //Then
        Assert.assertEquals(presenter?.vacancy, mockVac)
        verify(exactly = 1) {
            mView.updateTextView(presenter?.vacancy)
        }
    }

    @Test
    fun test_updateTextView_vac_NotSame() {
        //Given
        var mockVac = "Vac"

        //When
        presenter?.updateTextView_vac(mockVac)

        //Then
        Assert.assertNotSame(presenter?.vacancy, "Va")
        verify(exactly = 1) {
            mView.updateTextView(presenter?.vacancy)
        }
    }

    @Test
    fun test_updateTextView_vac_IsNull() {
        //Given
        var mockVac = null

        //When
        presenter?.updateTextView_vac(mockVac)

        //Then
        Assert.assertEquals(presenter?.vacancy, null)
        verify(exactly = 1) {
            mView.updateTextView(presenter?.vacancy)
        }
    }

    @Test
    fun test_showErrorMsg_No_ID() {
        //Given
        var mockValue = "No ID"

        //When
        presenter?.showErrorMsg(mockValue)

        //Then
        Assert.assertEquals(presenter?.errorMessage, mockValue)
        verify(exactly = 1) {
            mView.showError(presenter?.errorMessage)
        }
    }

    @Test
    fun test_showErrorMsg_No_ID_NotSame() {
        //Given
        var mockValue = "No ID"

        //When
        presenter?.showErrorMsg(mockValue)

        //Then
        Assert.assertNotSame(presenter?.errorMessage, "No")
        verify(exactly = 1) {
            mView.showError(presenter?.errorMessage)
        }
    }

    @Test
    fun test_showErrorMsg_Cannot_Get_JSON() {
        //Given
        var mockValue = "Cannot Get JSON"

        //When
        presenter?.showErrorMsg(mockValue)

        //Then
        Assert.assertEquals(presenter?.errorMessage, mockValue)
        verify(exactly = 1) {
            mView.showError(presenter?.errorMessage)
        }
    }

    @Test
    fun test_showErrorMsg_No_ID_NotSame_NotSame() {
        //Given
        var mockValue = "Cannot Get JSON"

        //When
        presenter?.showErrorMsg(mockValue)

        //Then
        Assert.assertNotSame(presenter?.errorMessage, "Cannot Get JSO")
        verify(exactly = 1) {
            mView.showError(presenter?.errorMessage)
        }
    }

    @Test
    fun test_showErrorMsg_IsNull(){
        //Given
        var mockValue = null

        //When
        mockValue?.let { presenter?.showErrorMsg(mockValue) }

        //Then
        Assert.assertEquals(presenter?.errorMessage, null)
        verify(exactly = 0) {
            mView.showError(presenter?.errorMessage)
        }
    }


}