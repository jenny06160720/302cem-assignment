package com.example.navigation

import android.content.Intent
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.navigation.ui.others.parking_info.CarParkModel
import com.example.navigation.ui.others.parking_info.CarParkPresenter
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import junit.framework.Assert
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CarParkPresenterTest {
    private var presenter: CarParkPresenter? = null;

    @RelaxedMockK
    lateinit var mView: CarParkPresenter.View

    lateinit var model: CarParkModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        presenter = CarParkPresenter(mView)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    //    test method addToView
    @Test
    fun test_addToView_success() {
        //Given
        var mockmData = arrayListOf<String>()

        //When
        presenter?.addToView(mockmData)

        //Then
        verify(exactly = 1) {
            mView.addToView(mockmData)
        }
    }

    @Test
    fun test_addToView_fail() {
        //Given
        var mockmData = arrayListOf<String>()

        //When
        presenter?.addToView(mockmData)

        //Then
        Assert.assertNotSame(presenter?.errorMessage, "Error")
        verify(exactly = 1) {
            mView.addToView(mockmData)
        }
    }

    @Test
    fun test_addToView_IsNull() {
        //Given
        var mockmData = null

        //When
        presenter?.addToView(mockmData)

        //Then
        Assert.assertEquals(presenter?.errorMessage, null)
        verify(exactly = 1) {
            mView.addToView(mockmData)
        }
    }

    //    test method sendErrorMessage
    @Test
    fun test_sendErrorMessage_testcase_JSON() {
        //Given
        val str = "JSON"
        //When
        presenter?.sendErrorMessage(str)
        //Then
        Assert.assertEquals(presenter?.errorMessage, "Cannot Get JSON")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.errorMessage)
        }
    }

    @Test
    fun test_sendErrorMessage_testcase_JSON_NotSame() {
        //Given
        val str = "JSON"
        //When
        presenter?.sendErrorMessage(str)
        //Then
        Assert.assertNotSame(presenter?.errorMessage, "Cannot Get JSO")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.errorMessage)
        }
    }

    @Test
    fun test_sendErrorMessage_testcase_NoRecord() {
        //Given
        val str = "CountOfRecord"
        //When
        presenter?.sendErrorMessage(str)
        //Then
        Assert.assertEquals(presenter?.errorMessage, "No Record")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.errorMessage)
        }
    }

    @Test
    fun test_sendErrorMessage_testcase_NoRecord_NotSame() {
        //Given
        val str = "CountOfRecord"
        //When
        presenter?.sendErrorMessage(str)
        //Then
        Assert.assertNotSame(presenter?.errorMessage, "No Recor")
        verify(exactly = 1) {
            mView.showErrorMessage(presenter?.errorMessage)
        }
    }

    @Test
    fun test_sendErrorMessage_testcase_IsNull() {
        //Given
        val str = null
        //When
        str?.let { presenter?.sendErrorMessage(str) }

        //Then
        Assert.assertEquals(presenter?.errorMessage, null)
        verify(exactly = 0) {
            mView.showErrorMessage(presenter?.errorMessage)
        }
    }

    //    test method intent
    @Test
    fun test_intent() {
        //Given
//        var mockIntnet = slot<Intent>()
        var mockIntnet = mockk<Intent>()
        //When
        presenter?.intent(mockIntnet)
        //Then
        verify(exactly = 1) {
            mView.intent(mockIntnet)
        }
        println("caputred = ${mockIntnet.toString()}")
    }


    @Test
    fun test_intent_putextra() {
        //Given
//        var mockIntnet = slot<Intent>()
        var mockIntnet = mockk<Intent>()
        //When
        presenter?.intent(mockIntnet)
        //Then
        verify(exactly = 1) {
            mView.intent(mockIntnet)
        }
        println("caputred = ${mockIntnet.toString()}")
    }
    //  test method callAPI
}