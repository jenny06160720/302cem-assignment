package com.example.navigation

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.navigation.ui.home.favourite.FavouriteRecordModel.Retrofit_Favourite_Model
import com.example.navigation.ui.home.favourite.ViewFavourite.FavouritePresenter
import com.example.navigation.ui.home.record.RecordModel.Retrofit_Record_Model
import com.example.navigation.ui.home.record.ViewRecord.RecordPresenter
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import io.mockk.verify
import junit.framework.Assert
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class FavouritePresenterTest {
    private var presenter: FavouritePresenter? = null

    @RelaxedMockK
    lateinit var mView:FavouritePresenter.View

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        presenter = FavouritePresenter(mView)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    @Test
    fun test_callAPI(){
        // Given
        // When
        presenter?.callAPI()
        // Then
    }

    @Test
    fun test_callAPI_delete(){
        // Given
        var mockmData = "s123"
        // When
        presenter?.callAPI_delete(mockmData)
        // Then
    }

    @Test
    fun test_getResponseSuccess(){
        // Given
        var mockmData =  mockk<List<Retrofit_Favourite_Model>>()
        // When
        presenter?.getResponseSuccess(mockmData)
        // Then
        verify(exactly = 1) {
            mView.addToFavouriteView(mockmData)
        }
    }

    @Test
    fun test_ShowMessage_IsNull_case_1(){
        // Given
        val title = "abc"
        val message = null
        // When
        message?.let { presenter?.ShowMessage(title, message) }
        // Then
        Assert.assertEquals(presenter?.message, null)
        verify(exactly = 0) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_IsNull_case_2(){
        // Given
        val title = null
        val message = "abc"
        // When
        title?.let { presenter?.ShowMessage(title, message) }
        // Then
        Assert.assertEquals(presenter?.title, null)
        verify(exactly = 0) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_IsNull_case_3(){
        // Given
        val title = null
        val message = null
        // When
        title?.let { presenter?.ShowMessage(title, message) }
        message?.let { presenter?.ShowMessage(title, message) }
        // Then
        Assert.assertEquals(presenter?.title, null)
        Assert.assertEquals(presenter?.message, null)
        verify(exactly = 0) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_IsNull_case_4(){
        // Given
        val title = "abc"
        val code = null
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, null)
        verify(exactly = 0) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_Success_case_1(){
        // Given
        val title = "abc"
        val message = "123"
        // When
        message?.let { presenter?.ShowMessage(title, message) }
        // Then
        Assert.assertEquals(presenter?.message, "123")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_Fail_case_1(){
        // Given
        val title = "abc"
        val message = "123"
        // When
        message?.let { presenter?.ShowMessage(title, message) }
        // Then
        Assert.assertNotSame(presenter?.message, "1234")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_1(){
        // Given
        val title = "abc"
        val code = 100
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "100 Continue")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_2(){
        // Given
        val title = "abc"
        val code = 101
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "101 Switching Protocol")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_3(){
        // Given
        val title = "abc"
        val code = 102
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "102 Processing")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_4(){
        // Given
        val title = "abc"
        val code = 103
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "103 Early Hints")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_5(){
        // Given
        val title = "abc"
        val code = 201
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "201 Created")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_6(){
        // Given
        val title = "abc"
        val code = 202
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "202 Accepted")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_7(){
        // Given
        val title = "abc"
        val code = 203
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "203 Non-Authoritative Information")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_8(){
        // Given
        val title = "abc"
        val code = 204
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "204 No Content")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_9(){
        // Given
        val title = "abc"
        val code = 205
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "205 Reset Content")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_10(){
        // Given
        val title = "abc"
        val code = 206
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "206 Partial Content")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_11(){
        // Given
        val title = "abc"
        val code = 207
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "207 Multi-Status (WebDAV)")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_12(){
        // Given
        val title = "abc"
        val code = 208
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "208 Multi-Status (WebDAV)")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_13(){
        // Given
        val title = "abc"
        val code = 226
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "226 IM Used (HTTP Delta encoding)")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_14(){
        // Given
        val title = "abc"
        val code = 300
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "300 Multiple Choice")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_15(){
        // Given
        val title = "abc"
        val code = 301
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "301 Moved Permanently")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_16(){
        // Given
        val title = "abc"
        val code = 302
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "302 Found")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_17(){
        // Given
        val title = "abc"
        val code = 303
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "303 See Other")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_18(){
        // Given
        val title = "abc"
        val code = 304
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "304 Not Modified")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_19(){
        // Given
        val title = "abc"
        val code = 305
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "305 Use Proxy")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_20(){
        // Given
        val title = "abc"
        val code = 306
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "306 unused")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_21(){
        // Given
        val title = "abc"
        val code = 307
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "307 Temporary Redirect")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_22(){
        // Given
        val title = "abc"
        val code = 308
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "308 Permanent Redirect")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_23(){
        // Given
        val title = "abc"
        val code = 400
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "400 Bad Request")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_24(){
        // Given
        val title = "abc"
        val code = 401
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "401 Unauthorized")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_25(){
        // Given
        val title = "abc"
        val code = 402
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "402 Payment Required")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_26(){
        // Given
        val title = "abc"
        val code = 403
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "403 Forbidden")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_27(){
        // Given
        val title = "abc"
        val code = 404
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "404 Not Found")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_28(){
        // Given
        val title = "abc"
        val code = 405
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "405 Method Not Allowed")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_29(){
        // Given
        val title = "abc"
        val code = 406
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "406 Not Acceptable")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_30(){
        // Given
        val title = "abc"
        val code = 407
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "407 Proxy Authentication Required")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_31(){
        // Given
        val title = "abc"
        val code = 407
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "407 Proxy Authentication Required")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_32(){
        // Given
        val title = "abc"
        val code = 408
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "408 Request Timeout")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_33(){
        // Given
        val title = "abc"
        val code = 409
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "409 Conflictt")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_34(){
        // Given
        val title = "abc"
        val code = 410
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "410 Gone")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_35(){
        // Given
        val title = "abc"
        val code = 411
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "411 Length Required")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_36(){
        // Given
        val title = "abc"
        val code = 412
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "412 Precondition Failed")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_37(){
        // Given
        val title = "abc"
        val code = 413
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "413 Payload Too Large")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_38(){
        // Given
        val title = "abc"
        val code = 414
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "414 URI Too Long")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_39(){
        // Given
        val title = "abc"
        val code = 415
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "415 Unsupported Media Type")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_40(){
        // Given
        val title = "abc"
        val code = 416
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "416 Requested Range Not Satisfiable")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_41(){
        // Given
        val title = "abc"
        val code = 417
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "417 Expectation Failed")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_42(){
        // Given
        val title = "abc"
        val code = 418
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "418 I'm a teapot")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_43(){
        // Given
        val title = "abc"
        val code = 421
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "421 Misdirected Request")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_44(){
        // Given
        val title = "abc"
        val code = 422
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "422 Unprocessable Entity")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_45(){
        // Given
        val title = "abc"
        val code = 423
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "423 Locked")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_46(){
        // Given
        val title = "abc"
        val code = 424
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "424 Failed Dependency")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_47(){
        // Given
        val title = "abc"
        val code = 426
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "426 Upgrade Required")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_48(){
        // Given
        val title = "abc"
        val code = 428
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "428 Precondition Required")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_49(){
        // Given
        val title = "abc"
        val code = 429
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "429 Too Many Requests")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_50(){
        // Given
        val title = "abc"
        val code = 431
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "431 Request Header Fields Too Large")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_51(){
        // Given
        val title = "abc"
        val code = 451
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "451 Unavailable For Legal Reasons")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_52(){
        // Given
        val title = "abc"
        val code = 500
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "500 Internal Server Error")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_53(){
        // Given
        val title = "abc"
        val code = 501
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "501 Not Implemented")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_54(){
        // Given
        val title = "abc"
        val code = 502
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "502 Bad Gateway")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_55(){
        // Given
        val title = "abc"
        val code = 503
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "503 Service Unavailable")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_56(){
        // Given
        val title = "abc"
        val code = 504
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "504 Gateway Timeout")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_57(){
        // Given
        val title = "abc"
        val code = 505
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "505 HTTP Version Not Supported")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_58(){
        // Given
        val title = "abc"
        val code = 506
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "506 Variant Also Negotiates")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_59(){
        // Given
        val title = "abc"
        val code = 507
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "507 Insufficient Storage")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_60(){
        // Given
        val title = "abc"
        val code = 508
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "508 Loop Detected")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_61(){
        // Given
        val title = "abc"
        val code = 510
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "510 Not Extended")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_62(){
        // Given
        val title = "abc"
        val code = 511
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "511 Network Authentication Required")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessage_code_Success_case_63(){
        // Given
        val title = "abc"
        val code = 512
        // When
        code?.let { presenter?.ShowMessage(title, code) }
        // Then
        Assert.assertEquals(presenter?.message, "Error")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessageForRefresh_Success(){
        // Given
        val title = "abc"
        val message = "123"
        // When
        message?.let { presenter?.ShowMessage(title, message) }
        // Then
        Assert.assertEquals(presenter?.title, "abc")
        Assert.assertEquals(presenter?.message, "123")
        verify(exactly = 1) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessageForRefresh_IsNull_Case_1(){
        // Given
        val title = null
        val message = "123"
        // When
        title?.let { presenter?.ShowMessage(title, message) }
        // Then
        Assert.assertEquals(presenter?.title, null)
        verify(exactly = 0) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessageForRefresh_IsNull_Case_2(){
        // Given
        val title = "abc"
        val message = null
        // When
        message?.let { presenter?.ShowMessage(title, message) }
        // Then
        Assert.assertEquals(presenter?.message, null)
        verify(exactly = 0) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

    @Test
    fun test_ShowMessageForRefresh_IsNull_Case_3(){
        // Given
        val title = null
        val message = null
        // When
        title?.let { presenter?.ShowMessage(title, message) }
        message?.let { presenter?.ShowMessage(title, message) }
        // Then
        Assert.assertEquals(presenter?.title, null)
        Assert.assertEquals(presenter?.message, null)
        verify(exactly = 0) {
            mView.ShowMessage(presenter?.title,presenter?.message)
        }
    }

}