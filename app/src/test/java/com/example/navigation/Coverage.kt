package com.example.navigation

import com.example.navigation.ui.news.Trafficnews
import com.example.navigation.ui.news.TrafficnewsPresenter
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.impl.annotations.RelaxedMockK
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

class Coverage {
    private var presenter: TrafficnewsPresenter? = null
    @RelaxedMockK
    lateinit var mView: TrafficnewsPresenter.View
    @RelaxedMockK
    lateinit var model: Trafficnews

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        model = Trafficnews()
        presenter = TrafficnewsPresenter(model, mView)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    // 1. test method callAPI
    @Test
    fun test_callAPI() {
//        // Given
//        // When
        presenter?.callAPI();
//        // Then
    }
}