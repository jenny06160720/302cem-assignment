package com.example.navigation.ui.home.favourite.CreateFavourite;

import com.example.navigation.ui.home.record.ViewRecord.RecordPresenter;

public class FavouriteCreatePresenter {
    private FavouriteCreateRepository favouriteCreateRepository;
    public FavouriteCreatePresenter(){
        this.favouriteCreateRepository = new FavouriteCreateRepository();
    }
    public void callAPI_create(RecordPresenter recordPresenter,String record) {
        favouriteCreateRepository.API_create(recordPresenter,record);
    }
}
