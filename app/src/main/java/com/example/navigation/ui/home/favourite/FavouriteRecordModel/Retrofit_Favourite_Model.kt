package com.example.navigation.ui.home.favourite.FavouriteRecordModel

import com.google.gson.annotations.SerializedName

data class Retrofit_Favourite_Model(
        @SerializedName("_id")
        val id: String,
        @SerializedName("Favourite")
        val favourite: String


)
