package com.example.navigation.ui.home.record.RecordModel;

import com.google.gson.annotations.SerializedName;

public class RouteRecordCreate_model {
    @SerializedName("ID")
    public String id;
    @SerializedName("To")
    public String record;

    public RouteRecordCreate_model(String record){
        this.record = record;
    }

    public void setId(String id){
        this.id = id;
    }
    public void setRecord(String record){
        this.record = record;
    }

    public String getId(){
        return this.id;
    }
    public String getRecord(){
        return this.record;
    }
}
