package com.example.navigation.ui.others.parking_info;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.navigation.R;
import java.util.List;

public class CarParkRecyclerViewAdapter extends RecyclerView.Adapter<CarParkRecyclerViewAdapter.ViewHolder> {

        private List<String> mData;
        CarParkPresenter carParkPresenter;

        CarParkRecyclerViewAdapter(List<String> data, CarParkPresenter parkPresenter) {
            mData = data;
            carParkPresenter = parkPresenter;
        }

        // 建立ViewHolder
        class ViewHolder extends RecyclerView.ViewHolder{
            // 宣告元件
            private TextView txtItem;

            ViewHolder(View itemView) {
                super(itemView);
                txtItem = (TextView) itemView.findViewById(R.id.tvCarParkItem);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String id = mData.get(getAdapterPosition()).split("\n")[0];
                        String id_new = "";
                        for (int i = 4; i < id.length(); i++) {
                            id_new += id.charAt(i);
                        }

                        //Intent to car park information
//                        Intent intent = new Intent(view.getContext(), CarParkDetail.class);
//                        intent.putExtra("carParkId", id_new);
//                        intent.putExtra("carParkBasicInfo", mData.get(getAdapterPosition()));
//                        carParkPresenter.intent(intent);
                        carParkPresenter.intent(id_new, mData.get(getAdapterPosition()));
//                        view.getContext().startActivity(intent);
                    }
                });
            }
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // 連結項目布局檔list_item
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
            ViewHolder holder = new ViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            // 設置txtItem要顯示的內容
            holder.txtItem.setText(mData.get(position));
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }
    }
