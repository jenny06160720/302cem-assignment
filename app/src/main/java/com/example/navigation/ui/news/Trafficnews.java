package com.example.navigation.ui.news;

import android.annotation.SuppressLint;
import android.util.Log;
import android.util.Xml;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Trafficnews {
    private String INCIDENT_NUMBER;
    private String INCIDENT_HEADING_EN;
    private String INCIDENT_DETAIL_EN;
    private String ANNOUNCEMENT_DATE;
    private String CONTENT_EN;
    private String ResponseCodeError;

    public Trafficnews(){}

    public void setINCIDENT_NUMBER(String nextText) {
        INCIDENT_NUMBER = nextText;
    }

    public String getINCIDENT_NUMBER() {
        return INCIDENT_NUMBER;
    }

    public void setINCIDENT_HEADING_EN(String nextText) {
        INCIDENT_HEADING_EN = nextText;
    }

    public String getINCIDENT_HEADING_EN() {
        return INCIDENT_HEADING_EN;
    }

    public void setINCIDENT_DETAIL_EN(String nextText) {
        INCIDENT_DETAIL_EN = nextText;
    }

    public String getINCIDENT_DETAIL_EN() {
        return INCIDENT_DETAIL_EN;
    }

    public void setANNOUNCEMENT_DATE(String nextText) {
        ANNOUNCEMENT_DATE = nextText;
    }

    public String getANNOUNCEMENT_DATE() {
        return ANNOUNCEMENT_DATE;
    }

    public void setCONTENT_EN(String nextText) {
        CONTENT_EN = nextText;
    }

    public String getCONTENT_EN() {
        return CONTENT_EN;
    }

    public void setResponseCodeError(String nextText) {
        ResponseCodeError = nextText;
    }

    public String getResponseCodeError() {
        return ResponseCodeError;
    }

    @SuppressLint("UseValueOf")
    public void getXmlData(TrafficnewsPresenter presenter) {
        try {
            URL url = new URL("https://www.td.gov.hk/tc/special_news/trafficnews.xml");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(5000);

            if (conn.getResponseCode() != 200) {
                presenter.setResponseCodeError(conn.getResponseCode());
            } else {
                if (conn.getResponseCode() == 200) {
                    InputStream inputstream = conn.getInputStream();
                    Log.i("test", "Network connection is successful");

                    XmlPullParser xml = Xml.newPullParser();
                    xml.setInput(inputstream, "UTF-8");
                    int event = xml.getEventType();

                    while (event != XmlPullParser.END_DOCUMENT) {
                        switch (event) {
                            case XmlPullParser.START_TAG:
                                String value = xml.getName();
                                if (value.equals("INCIDENT_NUMBER")) {
                                    presenter.setINCIDENT_NUMBER(xml.nextText());
                                } else if (value.equals("INCIDENT_HEADING_EN")) {
                                    presenter.setINCIDENT_HEADING_EN(xml.nextText());
                                } else if (value.equals("INCIDENT_DETAIL_EN")) {
                                    presenter.setINCIDENT_DETAIL_EN(xml.nextText());
                                } else if (value.equals("ANNOUNCEMENT_DATE")) {
                                    presenter.setANNOUNCEMENT_DATE(xml.nextText());
                                } else if (value.equals("CONTENT_EN")) {
                                    presenter.setCONTENT_EN(xml.nextText());
                                }
                        }
                        event = xml.next();
                    }
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
