package com.example.navigation.ui.others.vehicle_info.DeleteRecord;

import com.example.navigation.ui.others.vehicle_info.Get_Request_Interface;
import com.example.navigation.ui.others.vehicle_info.Get_Request_Path_Interface;
import com.example.navigation.ui.others.vehicle_info.Retrofit_Model;
import com.example.navigation.ui.others.vehicle_info.ViewAllRecord.VehicleInfoPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VehicleInfoDeleteRepository {
    public void API_delete(VehicleInfoPresenter vehicleInfoPresenter, String s) {
        Get_Request_Interface request = Get_Request_Path_Interface.retrofit.create(Get_Request_Interface.class);

        Call<Retrofit_Model> delete = request.deleteSelectedInfo(s);

        delete.enqueue(new Callback<Retrofit_Model>() {
            @Override
            public void onResponse(Call<Retrofit_Model> call, Response<Retrofit_Model> response) {
                System.out.println("deleted");
                if(response.code() == 200)
                    vehicleInfoPresenter.afterDelete("Deleted");
                else
                    vehicleInfoPresenter.API_View_Message("Message",response.code());
            }

            @Override
            public void onFailure(Call<Retrofit_Model> call, Throwable t) {
                System.out.println("response error " + t);
                vehicleInfoPresenter.API_View_Message("Message","Fail to connect to data!");
            }
        });
    }
}
