package com.example.navigation.ui.home.favourite.ViewFavourite;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;

import com.example.navigation.R;
import com.example.navigation.ui.home.favourite.FavouriteRecordModel.Retrofit_Favourite_Model;

import java.util.List;

public class FavouriteView extends AppCompatActivity implements FavouritePresenter.View{
    private RecyclerView recyclerView;
    private FavouriteRecyclerViewAdapter favouriteRecyclerViewAdapter;
    private FavouritePresenter favouritePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favourite_recycler);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Favourite");
        favouritePresenter = new FavouritePresenter(this);
        favouritePresenter.callAPI();
    }

    @Override
    public void addToFavouriteView(List<Retrofit_Favourite_Model> list) {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview_favourite);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        favouriteRecyclerViewAdapter = new FavouriteRecyclerViewAdapter(list, favouritePresenter);
        recyclerView.setAdapter(favouriteRecyclerViewAdapter);
    }

    @Override
    public void ShowMessage(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();
    }

    @Override
    public void ShowMessageAfterDelete (String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                startActivity(getIntent());
            }
        });
        builder.create();
        builder.show();
    }
}