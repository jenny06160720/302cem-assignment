package com.example.navigation.ui.home.favourite.ViewFavourite;

import com.example.navigation.ui.home.favourite.FavouriteInterface.Get_Favourite_Request_Interface;
import com.example.navigation.ui.home.favourite.FavouriteInterface.Get_Favourite_Request_Path_Interface;
import com.example.navigation.ui.home.favourite.FavouriteRecordModel.Retrofit_Favourite_Model;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavouriteRepository {
    public void API(FavouritePresenter favouritePresenter){
        Get_Favourite_Request_Interface request = Get_Favourite_Request_Path_Interface.retrofit.create(Get_Favourite_Request_Interface.class);
        Call<List<Retrofit_Favourite_Model>> call = request.getAllLicense();

        call.enqueue(new Callback<List<Retrofit_Favourite_Model>>() {
            @Override
            public void onResponse(Call<List<Retrofit_Favourite_Model>> call, Response<List<Retrofit_Favourite_Model>> response) {
                if(response.code() == 200){
                    favouritePresenter.getResponseSuccess(response.body());
                }else{
                    favouritePresenter.ShowMessage("Message Of View All Favourite Record",response.code());
                }
            }

            @Override
            public void onFailure(Call<List<Retrofit_Favourite_Model>> call, Throwable t) {
                favouritePresenter.ShowMessage("Message Of View All Favourite Record","Connect Server Fail");
            }
        });
    }
}
