package com.example.navigation.ui.home.record.RecordModel

import com.google.gson.annotations.SerializedName

data class Retrofit_Record_Model(
        @SerializedName("_id")
        val id: String,
        @SerializedName("To")
        val record: String

)
