package com.example.navigation.ui.others;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.example.navigation.R;

public class OthersFragment extends Fragment implements OtherPresenter.View {
    private Button click_bt_licenseInfo, click_bt_parkingInfo;
    private OtherPresenter otherPresenter;
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_others, container, false);

        otherPresenter = new OtherPresenter(this);
        click_bt_licenseInfo = root.findViewById(R.id.bt_licenseInfo);
        click_bt_parkingInfo = root.findViewById(R.id.bt_parkingInfo);

        click_bt_licenseInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otherPresenter.onClickLicenseInfo();
            }
        });

        click_bt_parkingInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otherPresenter.onClickParkingInfo();
            }
        });
        return root;
    }

    @Override
    public void showLicenseInfo(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void showParkingInfo(Intent intent) {
        startActivity(intent);
    }
}