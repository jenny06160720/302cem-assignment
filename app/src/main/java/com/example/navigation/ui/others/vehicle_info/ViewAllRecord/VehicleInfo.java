package com.example.navigation.ui.others.vehicle_info.ViewAllRecord;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import com.example.navigation.R;
import com.example.navigation.ui.others.vehicle_info.Retrofit_Model;

import java.util.ArrayList;
import java.util.List;

public class VehicleInfo extends AppCompatActivity implements VehicleInfoPresenter.View {
    private RecyclerView recyclerView;
    private VehicleInfoRecyclerViewAdapter adapter;
    private VehicleInfoPresenter vehicleInfoPresenter;
    private ArrayList<String> mData = new ArrayList<>();
    private Button bt_Create;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_info);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Vehicle Info");
        vehicleInfoPresenter = new VehicleInfoPresenter(this);
        vehicleInfoPresenter.callAPI();
        bt_Create = (Button) findViewById(R.id.bt_Create);
        bt_Create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Go Create ");
                vehicleInfoPresenter.createForm();
            }
        });
        Log.i("test", "Main: " + this);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        boolean flag = false;
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                flag = true;
                break;
            default:
                flag = super.onOptionsItemSelected(item);
                break;
        }
        return flag;
    }

    public void addToView(List<Retrofit_Model> mData){
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_vehicleInfo);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        adapter = new VehicleInfoRecyclerViewAdapter(mData, vehicleInfoPresenter);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void refreshResult(String message) {
        System.out.println("View."+ message);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setTitle("Message");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                startActivity(getIntent());
                finish();
            }
        });
        builder.create();
        builder.show();
    }

    @Override
    public void createNewPage(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void showMessage(String title, String string) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(string);
        builder.setTitle(title);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();
    }

    @Override
    public void showRecordPage(Intent intent) { startActivity(intent); }
}