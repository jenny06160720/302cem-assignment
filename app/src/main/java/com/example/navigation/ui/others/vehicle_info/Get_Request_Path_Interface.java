package com.example.navigation.ui.others.vehicle_info;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public interface Get_Request_Path_Interface {

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://10.0.2.2:3000")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
