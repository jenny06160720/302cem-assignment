package com.example.navigation.ui.others.parking_info;

import android.content.Context;
import android.content.Intent;
import org.jetbrains.annotations.TestOnly;

import java.util.ArrayList;

public class CarParkPresenter {
    private CarParkModel model;
    private View view;

    public CarParkPresenter(View view) {
        this.model = new CarParkModel();
        this.view = view;
    }

    @TestOnly
    public void callAPI(Context context) { model.API(this, context); }

    @TestOnly
    public String getErrorMessage(){
        return model.getErrorMessage();
    }

    @TestOnly
    public ArrayList<String> getmData(){
        return model.getmData();
    }

    public void addToView(ArrayList<String> mData){
        model.setmData(mData);
        view.addToView(model.getmData());
    }

    public void sendErrorMessage(String str){
        String message = null;

        if(str.equals("JSON")){
            message = "Cannot Get JSON";
        }else if(str.equals("CountOfRecord")){
            message = "No Record";
        }

        model.setErrorMessage(message);
        if(model.getErrorMessage() == null ) return;
        view.showErrorMessage(model.getErrorMessage());
    }

    public void intent(Intent intent) {
        view.intent(intent);
    }

    public void intent(String id_new, String value) {
        Intent intent = new Intent((Context) view, CarParkDetail.class);
        intent.putExtra("carParkId", id_new);
        intent.putExtra("carParkBasicInfo", value);
        view.intent(intent);
    }

    public interface View {
        void addToView(ArrayList<String> list);
        void intent(Intent intent);
        void showErrorMessage(String string);
    }
}
