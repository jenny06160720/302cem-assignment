package com.example.navigation.ui.home.favourite.FavouriteInterface;

import com.example.navigation.ui.home.favourite.FavouriteRecordModel.Retrofit_Favourite_Model;
import com.example.navigation.ui.home.favourite.FavouriteRecordModel.RouteFavouriteCreate_model;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface Get_Favourite_Request_Interface {

    @GET("/favourites")
    Call<List<Retrofit_Favourite_Model>> getAllLicense();
    @DELETE("/favourites/{favouriteID}")
    Call<Retrofit_Favourite_Model> deleteFavouriteData(@Path("favouriteID") String toID);
    @POST("/favourites/")
    Call<RouteFavouriteCreate_model> createFavouriteItem(@Body RouteFavouriteCreate_model favourite);

}