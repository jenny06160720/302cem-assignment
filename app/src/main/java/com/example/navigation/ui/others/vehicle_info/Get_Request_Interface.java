package com.example.navigation.ui.others.vehicle_info;

import com.example.navigation.ui.others.vehicle_info.CreateRecord.VehicleInfoCreateModel;
import com.example.navigation.ui.others.vehicle_info.ShowSelectedRecord.VehicleInfoUpdateModel;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface Get_Request_Interface {

    @GET("/posts")
    Call<List<Retrofit_Model>> getAllLicense();
    @GET("/posts/{postId}")
    Call<List<Retrofit_Model>> getSelectedInfo(@Path("postId") String postId);
    @DELETE("/posts/{postId}")
    Call<Retrofit_Model> deleteSelectedInfo(@Path("postId") String postId);
    @PATCH("/posts/{postId}")
    Call<VehicleInfoUpdateModel> updatePost(@Path("postId") String postId, @Body VehicleInfoUpdateModel post);
    @POST("/posts")
    Call<VehicleInfoCreateModel> createPost(@Body VehicleInfoCreateModel post);
}
