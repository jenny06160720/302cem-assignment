package com.example.navigation.ui.home.record.ViewRecord;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.navigation.R;
import com.example.navigation.ui.home.record.RecordModel.Retrofit_Record_Model;

import java.util.List;

public class RecordView extends AppCompatActivity implements RecordPresenter.View{
    private RecyclerView recyclerView;
    private RecordRecyclerViewAdapter recordRecyclerViewAdapter;
    private RecordPresenter recordPresenter;
    private List<Retrofit_Record_Model> listData ;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.record_recycler);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Record");
        recordPresenter = new RecordPresenter(RecordView.this);
        recordPresenter.callAPI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.record_del, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        System.out.println(listData.size());
        int id = item.getItemId();
        if( id==R.id.delete){
            for (int i = 0; i < listData.size(); i++){
                Retrofit_Record_Model record = listData.get(i);
                recordPresenter.callAPI_delete(record.getId());
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void addToRecordView(List<Retrofit_Record_Model> list){
        recyclerView = (RecyclerView) findViewById(R.id.recylerview_record);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recordRecyclerViewAdapter = new RecordRecyclerViewAdapter(list, recordPresenter);
        listData = list;
        recyclerView.setAdapter(recordRecyclerViewAdapter);
    }

    @Override
    public void ShowMessage(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();
    }


    @Override
    public void reshow(){
        startActivity(getIntent());
    }
}