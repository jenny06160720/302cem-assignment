package com.example.navigation.ui.others.vehicle_info.ShowSelectedRecord;

import com.example.navigation.ui.others.vehicle_info.Get_Request_Interface;
import com.example.navigation.ui.others.vehicle_info.Get_Request_Path_Interface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VehicleInfoUpdateRepository {
    public void API_edit(VehicleInfoUpdatePresenter vehicleInfoUpdatePresenter, String id, String licenseNumber, String type, String annualFee, String licenseExpiryDate, String licenseReminderDate, String policyNumber, String insuranceExpiryDate, String insuranceReminderDate, String lastEngineOilChange, String remindEngineOilChange, String lastGearboxOilChange, String remindGearboxOilChange, String updateTime) {
        VehicleInfoUpdateModel post = new VehicleInfoUpdateModel(licenseNumber, type, annualFee, licenseExpiryDate, licenseReminderDate, policyNumber, insuranceExpiryDate, insuranceReminderDate, lastEngineOilChange, remindEngineOilChange, lastGearboxOilChange, remindGearboxOilChange, updateTime);

        System.out.println(updateTime);

        Get_Request_Interface request = Get_Request_Path_Interface.retrofit.create(Get_Request_Interface.class);
        Call<VehicleInfoUpdateModel> update = request.updatePost(id, post);


        update.enqueue(new Callback<VehicleInfoUpdateModel>() {
            @Override
            public void onResponse(Call<VehicleInfoUpdateModel> call, Response<VehicleInfoUpdateModel> response) {
                System.out.println("Update " + response.code());
                if(response.code() == 200){
                    vehicleInfoUpdatePresenter.API_Create_Message("Message Of Update Record","Success to update record!!");
                }else{
                    vehicleInfoUpdatePresenter.API_Create_Message("Message Of Update Record",response.code());
                }
            }

            @Override
            public void onFailure(Call<VehicleInfoUpdateModel> call, Throwable t) {
                vehicleInfoUpdatePresenter.API_Create_Message("Message Of Update Record","Connect Data Fail");
            }
        });

    }
}
