package com.example.navigation.ui.home.favourite.CreateFavourite;

import com.example.navigation.ui.home.favourite.FavouriteInterface.Get_Favourite_Request_Interface;
import com.example.navigation.ui.home.favourite.FavouriteInterface.Get_Favourite_Request_Path_Interface;
import com.example.navigation.ui.home.favourite.FavouriteRecordModel.RouteFavouriteCreate_model;
import com.example.navigation.ui.home.record.ViewRecord.RecordPresenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavouriteCreateRepository{
    public void API_create(RecordPresenter recordPresenter, String favourite) {
        Get_Favourite_Request_Interface request = Get_Favourite_Request_Path_Interface.retrofit.create(Get_Favourite_Request_Interface.class);
        RouteFavouriteCreate_model postFavourite = new RouteFavouriteCreate_model(favourite);

        System.out.println("API_create: "+favourite);
        Call<RouteFavouriteCreate_model> create = request.createFavouriteItem(postFavourite);

        create.enqueue(new Callback<RouteFavouriteCreate_model>() {

            @Override
            public void onResponse(Call<RouteFavouriteCreate_model> call, Response<RouteFavouriteCreate_model> response) {
                if(response.code() != 200){
                    recordPresenter.ShowMessage("Create Favourite Message",response.code());
                }else{
                    recordPresenter.ShowMessage("Create Favourite Message","Successful");
                }
            }

            @Override
            public void onFailure(Call<RouteFavouriteCreate_model> call, Throwable t) {
                recordPresenter.ShowMessage("Create Favourite Message","Connect Server Fail");
            }
        });
    }
}
