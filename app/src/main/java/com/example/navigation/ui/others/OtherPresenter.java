package com.example.navigation.ui.others;

import android.content.Intent;

public class OtherPresenter {
    OtherPresenter.View view;

    public OtherPresenter(OtherPresenter.View view) {
        this.view = view;
    }

    public void onClickLicenseInfo(){
        Intent car_detail = new Intent().setClassName("com.example.navigation", "com.example.navigation.ui.others.vehicle_info.ViewAllRecord.VehicleInfo");
        view.showLicenseInfo(car_detail);
    }

    public void onClickParkingInfo(){
        Intent intent_toParkingInfo = new Intent().setClassName("com.example.navigation", "com.example.navigation.ui.others.parking_info.ParkingInfo");
        view.showParkingInfo(intent_toParkingInfo);
    }

    public interface View {
        void showLicenseInfo(Intent intent);
        void showParkingInfo(Intent intent);
    }
}
