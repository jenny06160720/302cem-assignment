package com.example.navigation.ui.home.record.ViewRecord;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.navigation.R;
import com.example.navigation.ui.home.favourite.CreateFavourite.FavouriteCreatePresenter;
import com.example.navigation.ui.home.record.RecordModel.Retrofit_Record_Model;

import java.util.List;

public class RecordRecyclerViewAdapter extends RecyclerView.Adapter<RecordRecyclerViewAdapter.ViewHolder>{
    private List<Retrofit_Record_Model> listData;
    public RecordPresenter recordPresenter;

    RecordRecyclerViewAdapter(List<Retrofit_Record_Model> data, RecordPresenter presenter) {
        this.listData = data;
        this.recordPresenter = presenter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_record, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Retrofit_Record_Model obj = listData.get(position);
        holder.tvRecord.setText(obj.getRecord());
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvRecord;
        private Button btnAddFavourite;

        ViewHolder(View itemView) {
            super(itemView);
            tvRecord = (TextView) itemView.findViewById(R.id.tvGoRecord);
            btnAddFavourite = (Button) itemView.findViewById(R.id.btnAddFavourite);
            btnAddFavourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String data = tvRecord.getText().toString();
                    if(data.equals("")){
                        recordPresenter.ShowMessage("Message", "Cannot Detect Data");
                    }else{
                        FavouriteCreatePresenter favouriteCreatePresenter = new FavouriteCreatePresenter();
                        favouriteCreatePresenter.callAPI_create(recordPresenter,data);
                    }
                }
            });
        }
    }
}
