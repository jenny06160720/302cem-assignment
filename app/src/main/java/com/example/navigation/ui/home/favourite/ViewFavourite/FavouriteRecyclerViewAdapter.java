package com.example.navigation.ui.home.favourite.ViewFavourite;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.navigation.R;
import com.example.navigation.ui.home.favourite.FavouriteRecordModel.Retrofit_Favourite_Model;

import java.util.List;

public class FavouriteRecyclerViewAdapter extends RecyclerView.Adapter<FavouriteRecyclerViewAdapter.ViewHolder>{
    private List<Retrofit_Favourite_Model> listData;
    FavouritePresenter favouritePresenter;

    FavouriteRecyclerViewAdapter(List<Retrofit_Favourite_Model> data, FavouritePresenter presenter) {
        listData = data;
        favouritePresenter = presenter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_favourite, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Retrofit_Favourite_Model obj = listData.get(position);
        holder.tvFavourite.setText(obj.getFavourite());
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvFavourite;
        private Button btnDel;

        ViewHolder(View itemView) {
            super(itemView);
            tvFavourite = (TextView) itemView.findViewById(R.id.tvFavouriteGo);
            btnDel = (Button) itemView.findViewById(R.id.btnDeleteFavourite);
            btnDel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Retrofit_Favourite_Model id = listData.get(getAdapterPosition());
                    if(id.equals("")){
                        favouritePresenter.ShowMessage("Message", "Cannot Detect Data");
                    }else{
                        favouritePresenter.callAPI_delete(id.getId());
                    }
                }
            });
        }
    }
}
