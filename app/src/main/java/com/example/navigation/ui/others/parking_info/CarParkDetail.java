package com.example.navigation.ui.others.parking_info;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.MenuItem;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import com.example.navigation.R;

public class CarParkDetail extends AppCompatActivity implements CarParkDetailPresenter.View {
    private TextView carParkTv;
    private CarParkDetailPresenter carParkDetailPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Car Park Detail");

        carParkDetailPresenter = new CarParkDetailPresenter((CarParkDetailPresenter.View) this);

        carParkTv = findViewById(R.id.tvCarParkDetailInfo);
        carParkTv.setMovementMethod(new ScrollingMovementMethod());
        carParkDetailPresenter.putIncoming(getIntent(), this);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        boolean flag = false;
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                flag = true;
                break;
            default:
                flag = super.onOptionsItemSelected(item);
                break;
        }
        return flag;
    }

    @Override
    public void updateTextView(String s) {
        carParkTv.append(s);
    }

    @Override
    public void showError(String string) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(string);
        builder.setTitle("Error");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();
    }

}