package com.example.navigation.ui.home.favourite.FavouriteRecordModel;

import com.google.gson.annotations.SerializedName;

public class RouteFavourite_model {
    @SerializedName("ID")
    public String id;
    @SerializedName("Favourite")
    public String favourite;

    public RouteFavourite_model(String id, String favourite){
        this.id = id;
        this.favourite = favourite;
    }
    public void setId(String id){
        this.id = id;
    }
    public void setFavourite(String favourite){
        this.favourite = favourite;
    }

    public String getId(){
        return this.id;
    }
    public String getFavourite(){
        return this.favourite;
    }
}
