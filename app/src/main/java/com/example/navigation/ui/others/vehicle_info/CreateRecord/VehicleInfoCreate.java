package com.example.navigation.ui.others.vehicle_info.CreateRecord;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.navigation.R;

import java.text.ParseException;
import java.util.Calendar;

public class VehicleInfoCreate extends AppCompatActivity implements VehicleInfoCreatePresenter.View {
    private VehicleInfoCreatePresenter vehicleInfoCreatePresenter;
    private Button bt_create;
    private EditText et_licenseNumber, et_type, et_annualFee, et_licenseExpiryDate, et_licenseReminderDate, et_policyNumber, et_insuranceExpiryDate, et_insuranceReminderDate, et_lastEngineOilChange, et_remindEngineOilChange, et_lastGearboxOilChange, et_remindGearboxOilChange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_create);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Create Record");
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        vehicleInfoCreatePresenter = new VehicleInfoCreatePresenter(this);

        et_licenseNumber = (EditText) findViewById(R.id.et_licenseNumber);
        et_type = (EditText) findViewById(R.id.et_type);
        et_annualFee = (EditText) findViewById(R.id.et_annualFee);
        et_licenseExpiryDate = (EditText) findViewById(R.id.et_licenseExpiryDate);
        et_licenseReminderDate = (EditText) findViewById(R.id.et_licenseReminderDate);
        et_policyNumber = (EditText) findViewById(R.id.et_policyNumber);
        et_insuranceExpiryDate = (EditText) findViewById(R.id.et_insuranceExpiryDate);
        et_insuranceReminderDate = (EditText) findViewById(R.id.et_insuranceReminderDate);
        et_lastEngineOilChange = (EditText) findViewById(R.id.et_lastEngineOilChange);
        et_remindEngineOilChange = (EditText) findViewById(R.id.et_remindEngineOilChange);
        et_lastGearboxOilChange = (EditText) findViewById(R.id.et_lastGearboxOilChange);
        et_remindGearboxOilChange = (EditText) findViewById(R.id.et_remindGearboxOilChange);

        et_licenseExpiryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String field = "licenseExpiryDate";
                String showCalender = "future";
                openCalendar(field, showCalender);
            }
        });

        et_licenseReminderDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String field = "licenseReminderDate";
                String showCalender = "future";
                openCalendar(field, showCalender);
            }
        });

        et_insuranceExpiryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String field = "insuranceExpiryDate";
                String showCalender = "future";
                openCalendar(field, showCalender);
            }
        });

        et_insuranceReminderDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String field = "insuranceReminderDate";
                String showCalender = "future";
                openCalendar(field, showCalender);
            }
        });

        et_lastEngineOilChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String field = "lastEngineOilChange";
                String showCalender = "past";
                openCalendar(field, showCalender);
            }
        });

        et_remindEngineOilChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String field = "remindEngineOilChange";
                String showCalender = "future";
                openCalendar(field, showCalender);
            }
        });

        et_lastGearboxOilChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String field = "lastGearboxOilChange";
                String showCalender = "past";
                openCalendar(field, showCalender);
            }
        });

        et_remindGearboxOilChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String field = "remindGearboxOilChange";
                String showCalender = "future";
                openCalendar(field, showCalender);
            }
        });

        bt_create = findViewById(R.id.bt_Create);
        bt_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String licenseNumber = et_licenseNumber.getText().toString();
                String type = et_type.getText().toString();
                String annualFee = et_annualFee.getText().toString();
                String licenseExpiryDate = et_licenseExpiryDate.getText().toString();
                String licenseReminderDate = et_licenseReminderDate.getText().toString();
                String policyNumber = et_policyNumber.getText().toString();
                String insuranceExpiryDate = et_insuranceExpiryDate.getText().toString();
                String insuranceReminderDate = et_insuranceReminderDate.getText().toString();
                String lastEngineOilChange = et_lastEngineOilChange.getText().toString();
                String remindEngineOilChange = et_remindEngineOilChange.getText().toString();
                String lastGearboxOilChange = et_lastGearboxOilChange.getText().toString();
                String remindGearboxOilChange = et_remindGearboxOilChange.getText().toString();

                try {
                    vehicleInfoCreatePresenter.callAPI_create(licenseNumber, type, annualFee, licenseExpiryDate, licenseReminderDate, policyNumber, insuranceExpiryDate, insuranceReminderDate
                            , lastEngineOilChange, remindEngineOilChange, lastGearboxOilChange, remindGearboxOilChange);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void openCalendar(String field, String showCalender) {
        Calendar calendar = Calendar.getInstance();

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(VehicleInfoCreate.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                vehicleInfoCreatePresenter.combineDate(year, month, dayOfMonth, field);
            }
        }, year, month, day);

        if (showCalender == "future") {
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();
        }
        else if (showCalender == "past"){
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }
    }

    @Override
    public void showListAgain(Intent goBack) {
        startActivity(goBack);
    }

    @Override
    public void showMessage(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                vehicleInfoCreatePresenter.goBackToList();
            }
        });
        builder.create();
        builder.show();
    }

    @Override
    public void showInputMessage(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();
    }

    @Override
    public void setDate(String sDate, String field) {
        if (field == "licenseExpiryDate")
            et_licenseExpiryDate.setText(sDate);
        else if (field == "licenseReminderDate")
            et_licenseReminderDate.setText(sDate);
        else if (field == "insuranceExpiryDate")
            et_insuranceExpiryDate.setText(sDate);
        else if (field == "insuranceReminderDate")
            et_insuranceReminderDate.setText(sDate);
        else if (field == "lastEngineOilChange")
            et_lastEngineOilChange.setText(sDate);
        else if (field == "remindEngineOilChange")
            et_remindEngineOilChange.setText(sDate);
        else if (field == "lastGearboxOilChange")
            et_lastGearboxOilChange.setText(sDate);
        else if (field == "remindGearboxOilChange")
            et_remindGearboxOilChange.setText(sDate);
    }

}
