package com.example.navigation.ui.others.parking_info;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class CarParkDetailModel {
    private CarParkDetailPresenter carParkDetailPresenter;
    private String basicInfo;
    private String vacancy;
    private String errorMessage;

    public CarParkDetailModel(CarParkDetailPresenter carParkDetailPresenter) {
        this.carParkDetailPresenter = carParkDetailPresenter;
    }

    public void setBasicInfo(String basicInfo) {
        this.basicInfo = basicInfo;
    }

    public String getBasicInfo() {
        return basicInfo;
    }

    public void setVacancy(String vacancy) {
        this.vacancy = vacancy;
    }

    public String getVacancy() {
        return vacancy;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void getIncoming(Intent intent, Context context) {
        if (intent.hasExtra("carParkId") && intent.hasExtra("carParkBasicInfo")) {
            Log.d("test", "getIncomingIntent: found intent extras.");
            String id = intent.getStringExtra("carParkId");
            basicInfo = intent.getStringExtra("carParkBasicInfo");
            //            carParkTv.setText(basicInfo + "\n\n\n");

            if (id != "") {
                getData("http://10.0.2.2:3000/getjson_id/" + id, context);
            } else {
                carParkDetailPresenter.showErrorMsg("No ID");
            }
        }
    }

    private String getData(String urlString, Context context) {
        String carParkResult = "";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(urlString, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    //Velloy採非同步作業，Response.Listener  監聽回應
                    public void onResponse(JSONObject response) {
                        Log.d("回傳結果", "結果=" + response.toString());
                        try {
                            getVacancy(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Response.ErrorListener 監聽錯誤
                Log.e("回傳結果", "錯誤訊息：" + error.toString());
                carParkDetailPresenter.showErrorMsg("Cannot Get JSON");
            }
        });
        Volley.newRequestQueue(context).add(jsonObjectRequest);
        return carParkResult;
    }

    private void getVacancy(JSONObject objectJson) throws JSONException {
        String str = "";

        JSONObject rs = objectJson.getJSONArray("results").getJSONObject(0);
        if (rs.length() < 0) {
            carParkDetailPresenter.showErrorMsg("No Record");
        } else {
            if (rs.has("privateCar")) {
                str += "Private Car\n";
                JSONObject privateCar = rs.getJSONArray("privateCar").getJSONObject(0);

                //vacancy_type
                str += getVacancy_type(privateCar.getString("vacancy_type"));

                //vacancy
                str += getVacancy_No(privateCar.getString("vacancy"));

                //lastupdate
                str += "Last Update Time: " + privateCar.getString("lastupdate") + "\n\n\n";
            }
            //LGV
            if (rs.has("LGV")) {
                str += "LGV\n";
                JSONObject privateCar = rs.getJSONArray("LGV").getJSONObject(0);

                //vacancy_type
                str += getVacancy_type(privateCar.getString("vacancy_type"));

                //vacancy
                str += getVacancy_No(privateCar.getString("vacancy"));

                //lastupdate
                str += "Last Update Time: " + privateCar.getString("lastupdate") + "\n\n\n";
            }


            //HGV
            if (rs.has("HGV")) {
                str += "HGV\n";
                JSONObject privateCar = rs.getJSONArray("HGV").getJSONObject(0);

                //vacancy_type
                str += getVacancy_type(privateCar.getString("vacancy_type"));

                //vacancy
                str += getVacancy_No(privateCar.getString("vacancy"));

                //lastupdate
                str += "Last Update Time: " + privateCar.getString("lastupdate") + "\n\n\n";
            }

            //CV
            if (rs.has("CV")) {
                str += "CV\n";
                JSONObject privateCar = rs.getJSONArray("CV").getJSONObject(0);

                //vacancy_type
                str += getVacancy_type(privateCar.getString("vacancy_type"));

                //vacancy
                str += getVacancy_No(privateCar.getString("vacancy"));

                //lastupdate
                str += "Last Update Time: " + privateCar.getString("lastupdate") + "\n\n\n";
            }

            //coach
            if (rs.has("coach")) {
                str += "Coach\n";
                JSONObject privateCar = rs.getJSONArray("coach").getJSONObject(0);

                //vacancy_type
                str += getVacancy_type(privateCar.getString("vacancy_type"));

                //vacancy
                str += getVacancy_No(privateCar.getString("vacancy"));

                //lastupdate
                str += "Last Update Time: " + privateCar.getString("lastupdate") + "\n\n\n";
            }


            //motorCycle
            if (rs.has("motorCycle")) {
                str += "Motor Cycle\n";
                JSONObject privateCar = rs.getJSONArray("motorCycle").getJSONObject(0);

                //vacancy_type
                str += getVacancy_type(privateCar.getString("vacancy_type"));

                //vacancy
                str += getVacancy_No(privateCar.getString("vacancy"));

                //lastupdate
                str += "Last Update Time: " + privateCar.getString("lastupdate") + "\n\n\n";
            }

            carParkDetailPresenter.updateTextView_basicInfo(basicInfo + "\n\n");
            carParkDetailPresenter.updateTextView_vac(str);
        }

    }

    private String getVacancy_No(String vacancyNo) {
        if ("-1".equals(vacancyNo)) {
            return "Vacancy: " + "No data provided by the car park operator\n";
        } else if ("0".equals(vacancyNo)) {
            return "Vacancy: The Car Park is Full\n";
        } else {
            return "Vacancy: " + vacancyNo + "\n";
        }
    }

    private String getVacancy_type(String vacancy_type) {
        if ("A".equals(vacancy_type))
            return "Vacancy Type: " + "A - Parking space availability with actual number\n";
        else if ("B".equals(vacancy_type))
            return "Vacancy Type: " + "B - Parking space availability without actual number\n";
        else if ("C".equals(vacancy_type))
            return "Vacancy Type: " + "C - Car park closed\n";
        else
            return null;
    }
}
