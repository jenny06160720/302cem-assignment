package com.example.navigation.ui.others.vehicle_info.ShowSelectedRecord;

import com.example.navigation.ui.others.vehicle_info.Get_Request_Interface;
import com.example.navigation.ui.others.vehicle_info.Get_Request_Path_Interface;
import com.example.navigation.ui.others.vehicle_info.Retrofit_Model;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VehicleInfoDetailRepository {
    public void API_editShowRecord(VehicleInfoUpdatePresenter vehicleInfoUpdatePresenter, String id) {
        Get_Request_Interface request = Get_Request_Path_Interface.retrofit.create(Get_Request_Interface.class);
        Call<List<Retrofit_Model>>  call = request.getSelectedInfo(id);
        call.enqueue(new Callback<List<Retrofit_Model>>() {
            @Override
            public void onResponse(Call<List<Retrofit_Model>> call, Response<List<Retrofit_Model>> response) {
                if(response.code() == 200){
                    if(response.body().size() == 1){
                        System.out.println("Response:  "+ response.body().get(0));
                        vehicleInfoUpdatePresenter.getResponseSuccess(response.body().get(0));
                    }else{
                        vehicleInfoUpdatePresenter.API_View_Message("Message Of View All Record","Get Data Error");
                    }
                }else{
                    vehicleInfoUpdatePresenter.API_View_Message("Message Of View All Record",response.code());
                }
            }

            @Override
            public void onFailure(Call<List<Retrofit_Model>> call, Throwable t) {
                System.out.println("response error " + t);
                vehicleInfoUpdatePresenter.API_View_Message("Message","Fail to connect to data");
            }
        });



    }
}
