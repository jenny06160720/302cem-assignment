package com.example.navigation.ui.home.record.DeleteRecord;

import com.example.navigation.ui.home.record.RecordInterface.Get_Record_Request_Interface;
import com.example.navigation.ui.home.record.RecordInterface.Get_Record_Request_Path_Interface;
import com.example.navigation.ui.home.record.RecordModel.Retrofit_Record_Model;
import com.example.navigation.ui.home.record.ViewRecord.RecordPresenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecordDeleteRepository {
    public void API_delete(RecordPresenter recordPresenter, String id) {
        Get_Record_Request_Interface request = Get_Record_Request_Path_Interface.retrofit
                .create(Get_Record_Request_Interface.class);

        Call<Retrofit_Record_Model> delete = request.deleteAll(id);

        delete.enqueue(new Callback<Retrofit_Record_Model>() {
            @Override
            public void onResponse(Call<Retrofit_Record_Model> call, Response<Retrofit_Record_Model> response) {
                if (response.code() != 200) {
                    recordPresenter.ShowMessage("Delete Record Message", response.code());
                } else {
                    recordPresenter.reshow();
                }
            }

            @Override
            public void onFailure(Call<Retrofit_Record_Model> call, Throwable t) {
                recordPresenter.ShowMessage("Delete Record Message", "Cannot Connect Server");
            }
        });
    }
}
