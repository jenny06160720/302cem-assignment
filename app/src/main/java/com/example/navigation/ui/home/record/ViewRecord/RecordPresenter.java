package com.example.navigation.ui.home.record.ViewRecord;

import com.example.navigation.ui.home.Message_Model_For_Record;
import com.example.navigation.ui.home.record.DeleteRecord.RecordDeleteRepository;
import com.example.navigation.ui.home.record.RecordModel.Retrofit_Record_Model;

import org.jetbrains.annotations.TestOnly;

import java.util.List;

public class RecordPresenter {
    private RecordRepository recordRepository;
    private RecordDeleteRepository recordDeleteRepository;
    private View view;
    private Message_Model_For_Record messageModel;
    @TestOnly
    public String getMessage() {
        return messageModel.getMessage();
    }
    @TestOnly
    public String getTitle() {
        return messageModel.getTitle();
    }

    public RecordPresenter(View view){
        this.view = view;
        this.recordRepository = new RecordRepository();
        this.recordDeleteRepository = new RecordDeleteRepository();
        this.messageModel = new Message_Model_For_Record();
    }

    public void callAPI(){ recordRepository.API(this); }
    public void callAPI_delete(String id){ recordDeleteRepository.API_delete(this,id); }

    public void getResponseSuccess(List<Retrofit_Record_Model> body){
        view.addToRecordView(body);
    }

    public void ShowMessage(String title, String message){
        messageModel.setTitle(title);
        messageModel.setMessage(message);
        if (messageModel.getTitle() == null || messageModel.getMessage() == null) {
            return;
        }
        view.ShowMessage(messageModel.getTitle(),messageModel.getMessage());
    }

    public void ShowMessage(String title, int code) {
        messageModel.setTitle(title);
        messageModel.setMessage(messageModel.checkCodeNum(code));
        if (messageModel.getTitle() == null || messageModel.getMessage() == null) {
            return;
        }
        view.ShowMessage(messageModel.getTitle(),messageModel.getMessage());
    }

    public void reshow(){
        view.reshow();
    }

    public interface View{
        void addToRecordView(List<Retrofit_Record_Model> list);
        void ShowMessage(String title, String message);
        void reshow();
    }
}



