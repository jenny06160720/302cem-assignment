package com.example.navigation.ui.home;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.example.navigation.MainActivity;
import com.example.navigation.R;
import com.example.navigation.ui.home.record.CreateRecord.RecordCreatePresenter;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class HomeFragment extends Fragment implements RecordCreatePresenter.View {
    private RecordCreatePresenter recordCreatePresenter;
    private SupportMapFragment mapFragment;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private Geocoder gc;
    private GoogleMap mMap;
    private Button btn_save, btn_record, btn_fav, btn_showAllGasStation, btn_showNearbyGasStation, btn_clearMap;
    private Button btn_showGasStationRoute, btn_showFavRoute;
    private EditText toEdit;
    private LatLng currentLoc;
    private String currentName;
    private LatLng aNearbyGasStation;
    private String aNearbyGasStationName = "";
    private String toDestinationName = "";

    private OnMapReadyCallback callback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            mMap = googleMap;
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toEdit = view.findViewById(R.id.tv_GetTo);
        btn_save = view.findViewById(R.id.btn_save);
        btn_record = view.findViewById(R.id.btn_record);
        btn_fav = view.findViewById(R.id.btn_fav);
        btn_showAllGasStation = view.findViewById(R.id.btn_showAllGasStation);
        btn_showNearbyGasStation = view.findViewById(R.id.btn_showNearbyGasStation);
        btn_clearMap = view.findViewById(R.id.btn_clearMap);
        recordCreatePresenter = new RecordCreatePresenter(this);
        btn_showGasStationRoute = view.findViewById(R.id.btn_showGasStationRoute);
        btn_showFavRoute = view.findViewById(R.id.btn_showFavRoute);

        btn_showGasStationRoute.setEnabled(false);
        btn_showFavRoute.setEnabled(false);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String record = toEdit.getText().toString();
                recordCreatePresenter.callAPI_create(record);
                try {
                    addMarkerAtToDestination(record);
                } catch (IOException e) {
                    showAlert("I/O Exception", "Error");
                }
            }
        });
        btn_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent to record
                Intent record = new Intent().setClassName("com.example.navigation", "com.example.navigation.ui.home.record.ViewRecord.RecordView");
                startActivity(record);
            }
        });

        btn_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent to record
                Intent favourite = new Intent().setClassName("com.example.navigation", "com.example.navigation.ui.home.favourite.ViewFavourite.FavouriteView");
                startActivity(favourite);
            }
        });

        btn_showAllGasStation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAllGasStation();
            }
        });

        btn_showNearbyGasStation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMarkerAtaNearbyGasStation();
            }
        });

        btn_clearMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearMap();
            }
        });

        btn_showFavRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawRoute(currentName, toDestinationName);
            }
        });

        btn_showGasStationRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawRoute(currentName, aNearbyGasStationName);
            }
        });

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        if (mapFragment != null) {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());

            if (ActivityCompat.checkSelfPermission(getContext()
                    , Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
            ) {
                loadingLocation();
                mapFragment.getMapAsync(callback);
            } else {
                Log.i("msg", "permission denied");
                //mapsPresenter.setMessageCode(302);
                ActivityCompat.requestPermissions(getActivity()
                        , new String[]{
                                Manifest.permission.ACCESS_FINE_LOCATION}, 44);
            }
        } else {
            shoeMessage("Message", "Map Fragment Is Null");
        }
    }

    public void showAlert(String message, String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
            }
        });
        builder.create();
        builder.show();
    }

    public void drawRoute(String current, String destination) {
        try {
            Uri uri = Uri.parse("https://www.google.co.in/maps/dir/" + current + "/" + destination);

            Intent intent = new Intent(Intent.ACTION_VIEW, uri);

            intent.setPackage("com.google.android.apps.maps");

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.maps");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(intent);
        }
    }

    public void shoeMessage(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();
    }

    public void loadingLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        Log.i("msg", "permission checked");
        //mapsPresenter.setMessageCode(202);
        fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                //mapsPresenter.setMessageCode(102);
                runningOnComplete(task);
            }
        });
    }

    public void runningOnComplete(@NonNull Task<Location> task) {

        System.out.println("enter onComplete");

        //mapsPresenter.setMessageCode(102);

        Location currentLocation;

        if (task.getResult() != null) {
            //mapsPresenter.setMessageCode(201);

            currentLocation = task.getResult();
            double currentLat = 0;
            double currentlng = 0;
            List<Address> currentAddress;
            String[] currentRegion;

            if (currentLocation != null) {
                //mapsPresenter.setMessageCode(201);
                System.out.println("location is not null");
                try {
                    //mapsPresenter.setMessageCode(103);
                    System.out.println("entered try catch");

                    gc = new Geocoder(getContext(), Locale.getDefault());

                    // set current location
                    System.out.println("Getting current location");

                    currentLat = currentLocation.getLatitude();
                    currentlng = currentLocation.getLongitude();

                    this.currentLoc = new LatLng(currentLat, currentlng);

                    currentAddress = gc.getFromLocation(currentLat, currentlng, 1);

                    // get current location
                    // update marker
                    //mapsPresenter.setMarker(new LatLng(currentLocationModel.getCurrentLat(),currentLocationModel.getCurrentLng()),"Current");
                    mMap.addMarker(new MarkerOptions().position(new LatLng(currentLat, currentlng)).title("Current"));
                    System.out.println("Got current location and Marked ");

                    currentRegion = currentAddress.get(0).getAddressLine(0).toString().split("\\,", -1);
                    this.currentName = currentRegion[0];

                    System.out.println("searching nearby gas station");
                    // get gas station 加德士
                    // get gas station 埃索
                    // get gas station 中國石油
                    // get gas station 蜆殻
                    // get gas station 中國石化
                    System.out.println("setting nearby gas station name");

                    //getting a nearby gas station
                    String gasStationName = "fuel station" + currentRegion[currentRegion.length - 2];
                    System.out.println(gasStationName);
                    List<Address> gasStation;
                    double gasStationLat = 0;
                    double gasStationLng = 0;
                    LatLng gasStationLatLng;

                    System.out.println("setting nearby gas station address");

                    gasStation = gc.getFromLocationName(gasStationName, 1);

                    System.out.println("setting nearby gas station location");

                    gasStationLat = gasStation.get(0).getLatitude();
                    gasStationLng = gasStation.get(0).getLongitude();

                    gasStationLatLng = new LatLng(gasStationLat, gasStationLng);

                    this.aNearbyGasStation = gasStationLatLng;
                    this.aNearbyGasStationName = gasStationName;

                    // set camera zoom latlng
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLat, currentlng), 14));

                    //mapsPresenter.setMessageCode(203);

                } catch (IOException e) {
                    //mapsPresenter.setMessageCode(303);
                    e.printStackTrace();
                } catch (Exception e) {
                    //mapsPresenter.setMessageCode(303);
                    e.getMessage();
                }
            } else {
                //mapsPresenter.setMessageCode(301);
                System.out.println("location is null");
            }
        } else {
            //mapsPresenter.setMessageCode(301);
            System.out.println("Task<Location> return null result");
        }
    }

    public void clearMap() {
        mMap.clear();

        // set current loc marker
        mMap.addMarker(new MarkerOptions().position(this.currentLoc).title("Current"));
        // Camera zoom
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(this.currentLoc, 14));

        // button Setting
        btn_showGasStationRoute.setEnabled(false);
        btn_showFavRoute.setEnabled(false);
    }

    public void addMarkerAtToDestination(String str_to_Name) throws IOException {
        mMap.clear();

        // set current loc marker
        mMap.addMarker(new MarkerOptions().position(this.currentLoc).title("Current"));
        // Camera zoom
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(this.currentLoc, 12));

        //get fav destination address
        List<Address> toDestination;
        double toDestinationLat = 0;
        double toDestinationLng = 0;
        LatLng toDestinationLatLng;

        this.toDestinationName = str_to_Name;
        toDestination = gc.getFromLocationName(str_to_Name, 1);

        if (toDestination.size() > 0) {
            toDestinationLat = toDestination.get(0).getLatitude();
            toDestinationLng = toDestination.get(0).getLongitude();
            toDestinationLatLng = new LatLng(toDestinationLat, toDestinationLng);

            // add marker at to destination
            mMap.addMarker(new MarkerOptions().position(toDestinationLatLng).title(str_to_Name));

            // button setting
            btn_showGasStationRoute.setVisibility(View.INVISIBLE);
            btn_showGasStationRoute.setEnabled(false);

            btn_showFavRoute.setVisibility(View.VISIBLE);
            btn_showFavRoute.setEnabled(true);
        } else {
            shoeMessage("Location not found", "Error Location");
        }
    }


    public void addMarkerAtaNearbyGasStation() {
        mMap.clear();

        // set current loc marker
        mMap.addMarker(new MarkerOptions().position(this.currentLoc).title("Current"));
        // Camera zoom
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(this.currentLoc, 14));
        // add marker at a nearby gas station
        mMap.addMarker(new MarkerOptions().position(this.aNearbyGasStation).title(aNearbyGasStationName));

        // button setting
        btn_showFavRoute.setVisibility(View.INVISIBLE);
        btn_showFavRoute.setEnabled(false);

        btn_showGasStationRoute.setVisibility(View.VISIBLE);
        btn_showGasStationRoute.setEnabled(true);
    }

    public void showAllGasStation() {
        mMap.clear();

        // set current loc marker
        mMap.addMarker(new MarkerOptions().position(this.currentLoc).title("Current"));
        // Camera zoom
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(this.currentLoc, 13));

        //Set new marker
        //Hong Kong Island
        LatLng HCaltexSWR = new LatLng(22.267595481361173, 114.1868562448535);
        LatLng HCaltexGR = new LatLng(22.282609979817245, 114.1849083318326);
        LatLng HCaltexTOB = new LatLng(22.28335644654927, 114.22204741066756);
        LatLng HCaltexSYS = new LatLng(22.26659379601521, 114.24715733691889);

        LatLng HEssoSVR = new LatLng(22.222108535518853, 114.21109886315914);
        LatLng HEssoWCHRSS = new LatLng(22.24977145812529, 114.16932445364048);
        LatLng HEssoLR = new LatLng(22.27851936226516, 114.16884613605042);
        LatLng HEssoSWR = new LatLng(22.267369763780916, 114.1870208115513);
        LatLng HEssoWNCGR = new LatLng(22.25915374918992, 114.19206658021857);
        LatLng HEssoSMS = new LatLng(22.27126908867607, 114.24178973350924);
        LatLng HEssoWCHRT = new LatLng(22.248816964551924, 114.16191039190215);
        LatLng HEssoJR = new LatLng(22.292745154316584, 114.20597935464997);
        LatLng HEssoFYS = new LatLng(22.26662647744284, 114.24439559938422);

        LatLng HPetroChinaPFLR = new LatLng(222.281664909000458, 114.13249874599711);
        LatLng HPetroChinaTHR = new LatLng(22.270715444289472, 114.19148318630245);
        LatLng HPetroChinaTSR = new LatLng(22.24927035024799, 114.15539363667394);
        LatLng HPetroChinaRBL = new LatLng(22.238749557804667, 114.1994984106857);

        LatLng HShellER = new LatLng(22.287583793999367, 114.19189688561791);
        LatLng HShellKR = new LatLng(22.287245196485706, 114.21056321498297);
        LatLng HShellVR = new LatLng(22.262136812304327, 114.13305818065467);
        LatLng HShellPFLR = new LatLng(22.27499323830475, 114.1299994567266);

        LatLng HSinopecPFLR = new LatLng(22.28191736044721, 114.13257602982318);
        LatLng HSinopecKRQB = new LatLng(22.28819692814917, 114.20979749813215);
        LatLng HSinopecCRW = new LatLng(22.277141175182972, 114.18133449904714);
        LatLng HSinopecKRWC = new LatLng(22.275264379889844, 114.16740765170546);
        LatLng HSinopecMR = new LatLng(22.275939716122075, 114.15594661723044);
        LatLng HSinopecCFR = new LatLng(22.273825983122837, 114.2425801777878);
        LatLng HSinopecFMR = new LatLng(22.288761402002557, 114.13861493434364);
        LatLng HSinopecRBL = new LatLng(22.268365784806356, 114.1628999368883);
        LatLng HSinopecWNCGR = new LatLng(22.25755973513446, 114.1932464604682);
        LatLng HSinopecVR = new LatLng(22.28131404237329, 114.12174706921945);

        //Kowloon

        LatLng KCaltexWR = new LatLng(22.318986324805607, 114.17603259143688);
        LatLng KCaltexPMR = new LatLng(22.315610670279995, 114.17719964094661);
        LatLng KCaltexTWR = new LatLng(22.308030087470595, 114.18806019858154);
        LatLng KCaltexTHTR = new LatLng(22.32795852265778, 114.17198895864024);
        LatLng KCaltexTPR = new LatLng(22.330849275711202, 114.16535965358523);
        LatLng KCaltexUCS = new LatLng(22.335030403494585, 114.15959585496417);
        LatLng KCaltexJPKVRFSS = new LatLng(22.335030403494585, 114.15959585496417);

        LatLng KEssoKFR = new LatLng(22.321038465960925, 114.20513418470273);
        LatLng KEssoNS = new LatLng(22.33829211877166, 114.14050489508479);
        LatLng KEssoBSMF = new LatLng(22.335334067027944, 114.14167032453851);
        LatLng KEssoCPR = new LatLng(22.33952885299773, 114.1512944964269);
        LatLng KEssoBSKT = new LatLng(22.32704731490681, 114.17648975314647);
        LatLng KEssoMKR = new LatLng(22.320173159219465, 114.16642046928763);
        LatLng KEssoWR = new LatLng(22.318573050813498, 114.17577952248848);
        LatLng KEssoWS = new LatLng(22.308179088152787, 114.18419333020734);
        LatLng KEssoPERE = new LatLng(22.331666602718606, 114.19509032701825);
        LatLng KEssoKFRS = new LatLng(22.321354524650644, 114.20564158490728);
        LatLng KEssoKFRF = new LatLng(22.32103905070992, 114.20513424396474);
        LatLng KEssoHKS = new LatLng(22.31605524741258, 114.23259064471667);

        LatLng KPetroChinaWR = new LatLng(22.343365118658397, 114.18011699204553);
        LatLng KPetroChinaGIC = new LatLng(22.336348115935657, 114.14966846214124);
        LatLng KPetroChinaPHR = new LatLng(22.32125559949831, 114.25524678456854);

        LatLng KShellTPRFEE = new LatLng(22.34412669297604, 114.15417432247331);
        LatLng KShellAS = new LatLng(22.32005257877934, 114.17463302057591);
        LatLng KShellBSKT = new LatLng(22.326709802892633, 114.17737225392425);
        LatLng KShellKFRF = new LatLng(22.321685388890305, 114.2052427244442);
        LatLng KShellLYMR = new LatLng(22.310240364336693, 114.22899616815803);
        LatLng KShellKCS = new LatLng(22.319783566806745, 114.16027204730042);
        LatLng KShellPERE = new LatLng(22.331318856323538, 114.19458468652596);
        LatLng KShellTHTR = new LatLng(22.328239414355814, 114.171924124084);
        LatLng KShellTKTR = new LatLng(22.320684885826456, 114.16218597537505);
        LatLng KShellTPRFF = new LatLng(22.331003184003936, 114.16517756791677);
        LatLng KShellMTWR = new LatLng(22.316865057666913, 114.1878283046297);
        LatLng KShellKFRS = new LatLng(22.320791249572448, 114.20536153128305);
        LatLng KShellMKR = new LatLng(22.320509264941848, 114.16601133932706);
        LatLng KShellBSMK = new LatLng(22.326360961896256, 114.16900307313792);
        LatLng KShellPMR = new LatLng(22.315817698034422, 114.17742302634471);
        LatLng KShellCPR = new LatLng(22.33747919924109, 114.1462308504009);

        LatLng KSinopecPERE = new LatLng(22.324390080136876, 114.1720430471919);
        LatLng KSinopecWR = new LatLng(22.31912927509259, 114.17616195988543);
        LatLng KSinopecCS = new LatLng(22.340166800341247, 114.17345372783721);
        LatLng KSinopecWLS = new LatLng(22.303387145456913, 114.22523134443728);
        LatLng KSinopecWCS = new LatLng(22.322633983515498, 114.20420855650312);
        LatLng KSinopecCYS = new LatLng(22.31648657357468, 114.21181961212007);
        LatLng KSinopecLCKR = new LatLng(22.33407170872235, 114.15300256528678);

        //New Territories

        LatLng NCaltexKFR = new LatLng(22.45098400555222, 114.16388418003751);
        LatLng NCaltexCPRTW = new LatLng(22.37042802214144, 114.12563143080719);
        LatLng NCaltexHHV = new LatLng(22.330632698474137, 114.26282164554164);
        LatLng NCaltexCPRHSK = new LatLng(22.431285533586873, 113.99482150192951);
        LatLng NCaltexTCW = new LatLng(22.42253512078321, 113.98459827335036);
        LatLng NCaltexKTR = new LatLng(22.440361468563022, 114.07448139399115);
        LatLng NCaltexTTRYL = new LatLng(22.4386618156903, 114.02932688229383);
        LatLng NCaltexCB = new LatLng(22.37709749985823, 113.98416041738643);
        LatLng NCaltexTPR = new LatLng(22.37023033347708, 114.17311574741686);
        LatLng NCaltexTYRW = new LatLng(22.355193262603738, 114.09937928123706);
        LatLng NCaltexLH = new LatLng(22.46493056719747, 114.05328939955315);
        LatLng NCaltexCPRFP = new LatLng(22.4728467379706, 114.05649420661074);
        LatLng NCaltexJCR = new LatLng(22.496416067117288, 114.13940682361475);
        LatLng NCaltexHTR = new LatLng(22.44083136775388, 113.99790679348547);
        LatLng NCaltexTYST = new LatLng(22.440381476223394, 114.00831898023402);
        LatLng NCaltexTTRF = new LatLng(22.48653948113983, 114.15001894328546);
        LatLng NCaltexCPRYL = new LatLng(22.443928792984366, 114.01822284142176);
        LatLng NCaltexJMLRMLR = new LatLng(22.377772807109647, 114.18452267754226);

        LatLng NEssoTYR = new LatLng(22.343928718502728, 114.10877143095267);
        LatLng NEssoTYRW = new LatLng(22.35676324791996, 114.10028695282459);
        LatLng NEssoCPRKH = new LatLng(22.369440490381695, 114.13259351334503);
        LatLng NEssoCPRSTN = new LatLng(22.37041739359503, 114.12598897141032);
        LatLng NEssoHWR = new LatLng(22.3802379782456, 113.97126436387373);
        LatLng NEssoCPRLT = new LatLng(22.419330564075103, 113.98263916861369);
        LatLng NEssoTYSTR = new LatLng(22.441243612247952, 114.01086554461887);
        LatLng NEssoL = new LatLng(22.497983087475713, 114.0773136189671);
        LatLng NEssoTHR = new LatLng(22.43552987567675, 113.99277103794044);
        LatLng NEssoFCR = new LatLng(22.4438215887304, 114.03179670463274);
        LatLng NEssoJCR = new LatLng(22.491340375917083, 114.14563087892913);
        LatLng NEssoTWR = new LatLng(22.45358081943538, 114.15977609091279);
        LatLng NEssoKFR = new LatLng(22.450779176972542, 114.16401060893863);
        LatLng NEssoKPS = new LatLng(22.379821265088204, 114.19890582286564);
        LatLng NEssoOPS = new LatLng(22.392561664736203, 114.20834989242817);
        LatLng NEssoHH = new LatLng(22.379296931580072, 114.26638772406848);
        LatLng NEssoHYS = new LatLng(22.414225099418395, 114.22689905426388);
        LatLng NEssoCWBR = new LatLng(22.334785557171582, 114.22987569892268);
        LatLng NEssoCPRSNE = new LatLng(22.37099199499817, 114.12735783864811);
        LatLng NEssoPLRN = new LatLng(22.327312706792398, 114.2546407526431);

        LatLng NPetroChinaYUR = new LatLng(22.366293952350393, 114.11932917037743);
        LatLng NPetroChinaKTR = new LatLng(22.440068987625867, 114.07336385501476);
        LatLng NPetroChinaCYR = new LatLng(22.28019158098759, 113.93960218542952);

        LatLng NShellCPRTLS = new LatLng(22.366367273981844, 114.00395586440649);
        LatLng NShellTPRFEE = new LatLng(22.34411372127217, 114.15417182044946);
        LatLng NShellDDOOF = new LatLng(22.44400385261902, 114.04364137681134);
        LatLng NShellDDS = new LatLng(22.461723076935325, 114.1519588910294);
        LatLng NShellCPRSTO = new LatLng(22.37038795731533, 114.12630306426387);
        LatLng NShellCWKS = new LatLng(22.372553148707215, 114.10885023665841);
        LatLng NShellCPRSET = new LatLng(22.371095422502655, 114.12801720448907);
        LatLng NShellLSS = new LatLng(22.385229300837416, 114.20529813809627);
        LatLng NShellCPRHSK = new LatLng(22.431252922851648, 113.99517073446482);
        LatLng NShellTWR = new LatLng(22.398172176803197, 113.97224628080633);
        LatLng NShellTYSTR = new LatLng(22.439618028959643, 114.0108476052854);
        LatLng NShellCPRPS = new LatLng(22.43779800057251, 114.00285394799454);
        LatLng NShellTYRW = new LatLng(22.355602993387958, 114.09954168350434);
        LatLng NShellPHR = new LatLng(22.32178494682939, 114.25493803700137);
        LatLng NShellKSR = new LatLng(22.425090999751887, 114.07497610465427);
        LatLng NShellOSL = new LatLng(22.418626143609657, 114.23250999552644);
        LatLng NShellTPRSTH = new LatLng(22.36091358260208, 114.16177273642552);
        LatLng NShellSTS = new LatLng(22.401735801457704, 113.9770603933185);
        LatLng NShellLOOSF = new LatLng(22.33983729969989, 114.24726477493144);
        LatLng NShellCLR = new LatLng(22.310737098996736, 113.93892535270199);
        LatLng NShellCWR = new LatLng(22.297191531301593, 113.93160402536519);
        LatLng NShellSTKR = new LatLng(22.500671214878842, 114.14655945443607);

        LatLng NSinopecBRW = new LatLng(22.46453866490645, 114.05336302199791);
        LatLng NSinopecFPB = new LatLng(22.47163226932323, 114.05405489986187);
        LatLng NSinopecTYSYR = new LatLng(22.44053366682733, 114.01135942121752);
        LatLng NSinopecCPRPS = new LatLng(22.44419244761632, 114.03163701970193);
        LatLng NSinopecFHTR = new LatLng(22.42102588729375, 113.98511785706891);
        LatLng NSinopecPHR = new LatLng(22.446351242210284, 113.99462406728274);
        LatLng NSinopecONZ = new LatLng(22.440140401611423, 114.07415074168593);
        LatLng NSinopecOS = new LatLng(22.4438352529564, 114.04959206596317);
        LatLng NSinopecSFA = new LatLng(22.507068495157323, 114.12735199381909);
        LatLng NSinopecSWR = new LatLng(22.49540839176444, 114.13708022080893);
        LatLng NSinopecYMR = new LatLng(22.48540448142444, 114.1439767663384);
        LatLng NSinopecLKR = new LatLng(22.431709985252997, 114.10370605497639);
        LatLng NSinopecSKWR = new LatLng(22.374121825319037, 113.99657869866998);
        LatLng NSinopecCPRSKW = new LatLng(22.370696078442236, 113.99476501798802);
        LatLng NSinopecYOS = new LatLng(22.3818502540683, 114.20742851864134);
        LatLng NSinopecTPRTPK = new LatLng(22.43243718416944, 114.1938266071655);
        LatLng NSinopecTYR = new LatLng(22.330099700662732, 114.10587283371147);
        LatLng NSinopecCPRKC = new LatLng(22.37102658914943, 114.12774175835108);
        LatLng NSinopecHTS = new LatLng(22.29444311665573, 113.95299465517881);
        LatLng NSinopecKCS = new LatLng(22.446043440806218, 114.1738095350199);
        LatLng NSinopecKOS = new LatLng(22.36058921882258, 114.13165755733985);
        LatLng NSinopecHYS = new LatLng(22.4139114014231, 114.2269282569297);
        LatLng NSinopecTYSOS = new LatLng(22.451549918895193, 114.03037298173115);
        LatLng NSinopecTTR = new LatLng(22.454151684173297, 114.16368009313886);
        LatLng NSinopecTYSS = new LatLng(22.394094658544546, 113.97719724961286);

        //Marker Titles

        // Hong Kong Island
        mMap.addMarker(new MarkerOptions().position(HCaltexSWR).title("Caltex - 30 Sing Woo Road, Happy Valley"));
        mMap.addMarker(new MarkerOptions().position(HCaltexGR).title("Caltex - Gloucester Road, Causeway Bay (Junction of Paterson Street)"));
        mMap.addMarker(new MarkerOptions().position(HCaltexTOB).title("Caltex - Tai On Building, Shau Kei Wan"));
        mMap.addMarker(new MarkerOptions().position(HCaltexSYS).title("Caltex - 6 Sun Yip Street, Siu Sai Wan (Honor Industrial Centre)"));

        mMap.addMarker(new MarkerOptions().position(HEssoSVR).title("Esso - 34 Stanley Village Road, Stanley)"));
        mMap.addMarker(new MarkerOptions().position(HEssoWCHRSS).title("Esso - 66 Wong Chuk Hang Road, Aberdeen (Next to Aberdeen Sports Ground)"));
        mMap.addMarker(new MarkerOptions().position(HEssoLR).title("Esso - 1 Lockhart Road, Wan Chai (Junction of Arsenal Street)"));
        mMap.addMarker(new MarkerOptions().position(HEssoSWR).title("Esso - 50 Sing Woo Road, Happy Valley (Next to Police Station)"));
        mMap.addMarker(new MarkerOptions().position(HEssoWNCGR).title("Esso - Wong Nai Chung Gap Road, Wong Nai Chung Gap (Opposite to Hong Kong Tennis Centre)"));
        mMap.addMarker(new MarkerOptions().position(HEssoSMS).title("Esso - 8 Sheung Mau Street, Heng Fa Chuen (Next to City Bus Bus Depot)"));
        mMap.addMarker(new MarkerOptions().position(HEssoWCHRT).title("Esso - 3 Wong Chuk Hang Road, Aberdeen"));
        mMap.addMarker(new MarkerOptions().position(HEssoJR).title("Esso - Java Road, North Point (Next to Kodak House)"));
        mMap.addMarker(new MarkerOptions().position(HEssoFYS).title("Esso - 97 Fung Yip Street, Chai Wan"));

        mMap.addMarker(new MarkerOptions().position(HPetroChinaPFLR).title("PetroChina - 80 Pok Fu Lam Road, Sai Wan (Adjacent to Pokfield Bus Terminus)"));
        mMap.addMarker(new MarkerOptions().position(HPetroChinaPFLR).title("PetroChina - 137 Tai Hang Road, Jardine's Lookout, Happy Valley (Junction of Perkins Road)"));
        mMap.addMarker(new MarkerOptions().position(HPetroChinaPFLR).title("PetroChina - 30 Tung Sing Road, Aberdeen (Near Port Centre)"));
        mMap.addMarker(new MarkerOptions().position(HPetroChinaPFLR).title("PetroChina - Rural Building, Lot No. 1196, South Bay Road, Repulse Bay (Junction of South Bay Road)"));

        mMap.addMarker(new MarkerOptions().position(HShellER).title("Shell - 169-171 Electric Road, North Point"));
        mMap.addMarker(new MarkerOptions().position(HShellKR).title("Shell - 979A King's Road, Quarry Bay"));
        mMap.addMarker(new MarkerOptions().position(HShellVR).title("Shell - 555 Victoria Road, Pok Fu Lam"));
        mMap.addMarker(new MarkerOptions().position(HShellPFLR).title("Shell - Pok Fu Lam Road, Sai Wan (Near Queen Mary Hospital)"));

        mMap.addMarker(new MarkerOptions().position(HSinopecPFLR).title("76 Pok Fu Lam Road, Sai Wan"));
        mMap.addMarker(new MarkerOptions().position(HSinopecKRQB).title("892 King's Road, Quarry Bay"));
        mMap.addMarker(new MarkerOptions().position(HSinopecCRW).title("Canal Road West, Causeway Bay"));
        mMap.addMarker(new MarkerOptions().position(HSinopecKRWC).title("9J Kennedy Road, Wan Chai"));
        mMap.addMarker(new MarkerOptions().position(HSinopecMR).title("3A MacDonnell Road, Central"));
        mMap.addMarker(new MarkerOptions().position(HSinopecCFR).title("2 Chong Fu Road, Chai Wan (Retrofit LPG Filling Services)"));
        mMap.addMarker(new MarkerOptions().position(HSinopecFMR).title("9 Fung Mat Road, Sheung Wan (Dedicated LPG Station)"));
        mMap.addMarker(new MarkerOptions().position(HSinopecRBL).title("Rural Building Lots No. 1184 & 1185, Peak Road, The Peak"));
        mMap.addMarker(new MarkerOptions().position(HSinopecWNCGR).title("145 Wong Nai Chung Gap Road, Happy Valley"));
        mMap.addMarker(new MarkerOptions().position(HSinopecVR).title("61 Victoria Road, Kennedy Town"));

        //Kowloon

        mMap.addMarker(new MarkerOptions().position(KCaltexWR).title("Caltex - Waterloo Road, Kowloon City (Near Pui Ching Road)"));
        mMap.addMarker(new MarkerOptions().position(KCaltexPMR).title("Caltex - Princess Margaret Road, Ho Man Tin (Opposite to Homantin Police Station)"));
        mMap.addMarker(new MarkerOptions().position(KCaltexTWR).title("Caltex - Tai Wan Road, Hung Hom"));
        mMap.addMarker(new MarkerOptions().position(KCaltexTHTR).title("Caltex - 15 Tai Hang Tung Road, Shek Kip Mei (Junction of Yau Yat Chuen Road)"));
        mMap.addMarker(new MarkerOptions().position(KCaltexTPR).title("Caltex - 45 Tai Po Road, Sham Shui Po (Junction of Un Chau Street)"));
        mMap.addMarker(new MarkerOptions().position(KCaltexUCS).title("Caltex - 221 Un Chau Street, Sham Shui Po (Junction of Pratas Street)"));

        mMap.addMarker(new MarkerOptions().position(KEssoKFR).title("Esso - 4 Kai Fuk Road, Kowloon Bay"));
        mMap.addMarker(new MarkerOptions().position(KEssoNS).title("Esso - NKIL 5085, Nassau Street, Mei Foo Sun Chuen Stage 6, Mei Foo"));
        mMap.addMarker(new MarkerOptions().position(KEssoBSMF).title("Esso - 94-132 Broadway Street, Mei Foo Sun Chuen Stage 8, Mei Foo"));
        mMap.addMarker(new MarkerOptions().position(KEssoCPR).title("Esso - 528 - 530 Castle Peak Road, Cheung Sha Wan (Near Caritas Medical Centre)"));
        mMap.addMarker(new MarkerOptions().position(KEssoBSKT).title("Esso - 113 Boundary Street, Kowloon Tong"));
        mMap.addMarker(new MarkerOptions().position(KEssoMKR).title("Esso - 15 Mong Kok Road, Mong Kok"));
        mMap.addMarker(new MarkerOptions().position(KEssoWR).title("Esso - 82 Waterloo Road, Ho Man Tin"));
        mMap.addMarker(new MarkerOptions().position(KEssoWS).title("Esso - 165-171 Wuhu Street, Hung Hom"));
        mMap.addMarker(new MarkerOptions().position(KEssoPERE).title("Esso - 594 Prince Edward Road East, Kowloon City"));
        mMap.addMarker(new MarkerOptions().position(KEssoKFRS).title("Esso - 7 Kai Fuk Road, Kowloon Bay (To Kwun Tong)"));
        mMap.addMarker(new MarkerOptions().position(KEssoKFRF).title("Esso - 4 Kai Fuk Road, Kowloon Bay (To Hung Hom)"));
        mMap.addMarker(new MarkerOptions().position(KEssoHKS).title("Esso - 77 Hiu Kwong Street, Sau Mau Ping, Kwun Tong"));

        mMap.addMarker(new MarkerOptions().position(KPetroChinaWR).title("201 Waterloo Road, Kowloon Tong (Near the entrance of Lion Rock Tunnel northbound tunnel tube)"));
        mMap.addMarker(new MarkerOptions().position(KPetroChinaGIC).title("G/F, Goodluck Industrial Centre, 808 Lai Chi Kok Road, Cheung Sha Wan"));
        mMap.addMarker(new MarkerOptions().position(KPetroChinaPHR).title("110 Po Hong Road, Tseung Kwan O (Opposite to Verbena Heights)"));

        mMap.addMarker(new MarkerOptions().position(KShellTPRFEE).title("Shell - 488 Tai Po Rd, Sham Shui Po"));
        mMap.addMarker(new MarkerOptions().position(KShellAS).title("Shell - 108 Argyle Street, Mong Kok"));
        mMap.addMarker(new MarkerOptions().position(KShellBSKT).title("Shell - 126 - 128 Boundary Street, Kowloon Tong"));
        mMap.addMarker(new MarkerOptions().position(KShellKFRF).title("Shell - 5 Kai Ful Road, Kowloon Bay (Towards Kwun Tong)"));
        mMap.addMarker(new MarkerOptions().position(KShellLYMR).title("Shell - 8 Lei Yue Mun Road, Lam Tin"));
        mMap.addMarker(new MarkerOptions().position(KShellKCS).title("Shell - 11 - 15 Kok Cheung Street, Tai Kok Tsui"));
        mMap.addMarker(new MarkerOptions().position(KShellPERE).title("Shell - 590 Prince Edward Road East, Sau Mau Ping"));
        mMap.addMarker(new MarkerOptions().position(KShellTHTR).title("Shell - 17 Tai Hang Tung Road, Shek Kip Mei"));
        mMap.addMarker(new MarkerOptions().position(KShellTKTR).title("Shell - 82 Tai Kok Tsui Road, Tai Kok Tsui (Junction of Anchor Street)"));
        mMap.addMarker(new MarkerOptions().position(KShellTPRFF).title("Shell - 45B Tai Po Road, Sham Shui Po"));
        mMap.addMarker(new MarkerOptions().position(KShellMTWR).title("Shell - 163 Ma Tau Wai Road, To Kwa Wan"));
        mMap.addMarker(new MarkerOptions().position(KShellKFRS).title("Shell - 6 Kai Fuk Road, Kowloon Bay (Towards To Kwa Wan)"));
        mMap.addMarker(new MarkerOptions().position(KShellMKR).title("Shell - 2 Mong Kok Road, Mong Kok (Junction of Tong Mei Road)"));
        mMap.addMarker(new MarkerOptions().position(KShellBSMK).title("Shell - 48 Boundary Street, Mong Kok"));
        mMap.addMarker(new MarkerOptions().position(KShellPMR).title("Shell - Princess Margaret Road, Ho Man Tin (Towards Kowloon Tong)"));
        mMap.addMarker(new MarkerOptions().position(KShellCPR).title("Shell - 493 - 495 Castle Peak Road, Cheung Sha Wan (Junction of Tung Chau Street)"));

        mMap.addMarker(new MarkerOptions().position(KSinopecPERE).title("Sinopec - 187A Prince Edward Road West, Mong Kok"));
        mMap.addMarker(new MarkerOptions().position(KSinopecWR).title("Sinopec - 82B Waterloo Road, Ho Man Tin"));
        mMap.addMarker(new MarkerOptions().position(KSinopecCS).title("Sinopec - 14E Cornwall Street, Kowloon Tong (Retrofit LPG Filling Services)"));
        mMap.addMarker(new MarkerOptions().position(KSinopecWLS).title("Sinopec - 9 Wai Lok Street, Cha Kwo Ling Road, Kwun Tong (Dedicated LPG Station)"));
        mMap.addMarker(new MarkerOptions().position(KSinopecWCS).title("Sinopec - 16 Wang Chin Street, Kowloon Bay (Retrofit LPG Filling Services)"));
        mMap.addMarker(new MarkerOptions().position(KSinopecCYS).title("Sinopec - 2K Cheung Yip Street, Kowloon Bay (Dedicated LPG Station)"));
        mMap.addMarker(new MarkerOptions().position(KSinopecLCKR).title("Sinopec - 779 Lai Chi Kok Road, Kowloon"));

        //New Territories

        mMap.addMarker(new MarkerOptions().position(NCaltexKFR).title("Caltex - Kwong Fuk Road, Tai Po Market"));
        mMap.addMarker(new MarkerOptions().position(NCaltexCPRTW).title("Caltex - Castle Peak Road, Tsuen Wan (Near Tai Wo Hau Estate)"));
        mMap.addMarker(new MarkerOptions().position(NCaltexHHV).title("Caltex - Hang Hau Village, Clear Water Bay Road, Sai Kung (Near Former TVB City Entrance)"));
        mMap.addMarker(new MarkerOptions().position(NCaltexCPRHSK).title("Caltex - Castle Peak Road, Hung Shui Kiu, Yuen Long"));
        mMap.addMarker(new MarkerOptions().position(NCaltexTCW).title("Caltex - Tsing Chuen Wai, Tuen Mun"));
        mMap.addMarker(new MarkerOptions().position(NCaltexKTR).title("Caltex - Kam Tin Road, Shek Kong, Yuen Long"));
        mMap.addMarker(new MarkerOptions().position(NCaltexTTRYL).title("Caltex - Tai Tong Road, Yuen Long"));
        mMap.addMarker(new MarkerOptions().position(NCaltexCB).title("Caltex - Cafeteria Beach, Castle Peak Road, Tuen Mun"));
        mMap.addMarker(new MarkerOptions().position(NCaltexTPR).title("Caltex - Tai Po Road, Sha Tin (Near Sha Tin Inn)"));
        mMap.addMarker(new MarkerOptions().position(NCaltexTYRW).title("Caltex - 171 Tsing Yi Road West, Tsing Yi"));
        mMap.addMarker(new MarkerOptions().position(NCaltexLH).title("Caltex - Long Ha, Castle Peak Road"));
        mMap.addMarker(new MarkerOptions().position(NCaltexCPRFP).title("Caltex - Castle Peak Road (near Fairview Park)"));
        mMap.addMarker(new MarkerOptions().position(NCaltexJCR).title("Caltex - Jockey Club Road, Sheung Shui (Near Fanling Magistrates' Courts)"));
        mMap.addMarker(new MarkerOptions().position(NCaltexHTR).title("Caltex - Hung Tin Road, Castle Peak Road, Tin Shui Wai, Yuen Long (Shek Po Tsuen)"));
        mMap.addMarker(new MarkerOptions().position(NCaltexTYST).title("Caltex - Tong Yan San Tsuen, Castle Peak Road, Yuen Long (Junction of San Hei Tsuen Street)"));
        mMap.addMarker(new MarkerOptions().position(NCaltexTTRF).title("Caltex - Tai Po Road, Kau Lung Hang, Fanling"));
        mMap.addMarker(new MarkerOptions().position(NCaltexCPRYL).title("Caltex - Castle Peak Road, Yuen Long (Near Shui Pin Tsuen)"));
        mMap.addMarker(new MarkerOptions().position(NCaltexJMLRMLR).title("Caltex - Junction of Man Lam Road and Man Lai Road, Tai Wai"));

        mMap.addMarker(new MarkerOptions().position(NEssoTYR).title("Esso - 15 Tsing Yi Road, Tsing Yi"));
        mMap.addMarker(new MarkerOptions().position(NEssoTYRW).title("Esso - 183 Tsing Yi Road West, Tsing Yi"));
        mMap.addMarker(new MarkerOptions().position(NEssoCPRKH).title("Esso - 551-561 Castle Peak Road, Kwai Hing"));
        mMap.addMarker(new MarkerOptions().position(NEssoCPRSTN).title("Esso - 739-745 Castle Peak Road, Tsuen Wan"));
        mMap.addMarker(new MarkerOptions().position(NEssoHWR).title("Esso - 38 Hoi Wah Road, Tuen Mun"));
        mMap.addMarker(new MarkerOptions().position(NEssoCPRLT).title("Esso - 121 Castle Peak Road, Lam Tei"));
        mMap.addMarker(new MarkerOptions().position(NEssoTYSTR).title("Esso - 4 Tong Yan San Tsuen Road, Ping Shan"));
        mMap.addMarker(new MarkerOptions().position(NEssoL).title("Esso - Lot 3465, Castle Peak Road, San Tin, Yuen Long"));
        mMap.addMarker(new MarkerOptions().position(NEssoTHR).title("Esso - 70 Tin Ha Road, Yuen Long"));
        mMap.addMarker(new MarkerOptions().position(NEssoFCR).title("Esso - 2-6 Fung Cheung Road, Yuen Long"));
        mMap.addMarker(new MarkerOptions().position(NEssoJCR).title("Esso - 339 Jockey Club Road, Fanling"));
        mMap.addMarker(new MarkerOptions().position(NEssoTWR).title("Esso - 9 Tai Wo Road, Tai Po"));
        mMap.addMarker(new MarkerOptions().position(NEssoKFR).title("Esso - 1M Kwong Fuk Road, Tai Po"));
        mMap.addMarker(new MarkerOptions().position(NEssoKPS).title("Esso - 18 Kong Pui Street, Sha Tin"));
        mMap.addMarker(new MarkerOptions().position(NEssoOPS).title("Esso - 11 On Ping Street, Sha Tin"));
        mMap.addMarker(new MarkerOptions().position(NEssoHH).title("Esso - Hiram's Highway, Sai Kung (Near Sai Kung Centre)"));
        mMap.addMarker(new MarkerOptions().position(NEssoHYS).title("Esso - 9 Hang Yiu Street, Ma On Shan"));
        mMap.addMarker(new MarkerOptions().position(NEssoCWBR).title("Esso - Clear Water Bay Road, Sai Kung (Near Anderson Road)"));
        mMap.addMarker(new MarkerOptions().position(NEssoCPRSNE).title("Esso - 698‐704 Castle Peak Road, Tsuen Wan (Towards Kowloon)"));
        mMap.addMarker(new MarkerOptions().position(NEssoPLRN).title("Esso - 21 Po Lam Road North, Tseung Kwan O"));

        mMap.addMarker(new MarkerOptions().position(NPetroChinaYUR).title("PetroChina - 171 Yeung Uk Road, Tsuen Wan (Junction of Wang Lung Street)"));
        mMap.addMarker(new MarkerOptions().position(NPetroChinaKTR).title("PetroChina - 188 Kam Tin Road, Yuen Long (Near Shek Kong Camp)"));
        mMap.addMarker(new MarkerOptions().position(NPetroChinaCYR).title("PetroChina - 3 Chung Yan Road, Tung Chung, Lantau Island (Junction of Yu Tung Road)"));

        mMap.addMarker(new MarkerOptions().position(NShellCPRTLS).title("Shell - 98 Castle Peak Road, Tai Lam Section"));
        mMap.addMarker(new MarkerOptions().position(NShellTPRFEE).title("Shell - 488 Tai Po Road"));
        mMap.addMarker(new MarkerOptions().position(NShellDDOOF).title("Shell - DD 115, Lot No. 1010-1015 & 1045 SB1, Castle Peak Road, Au Tau, Yuen Long"));
        mMap.addMarker(new MarkerOptions().position(NShellDDS).title("Shell - DD 7, Lot No. 1945, Hong Lok Yuen, Tai Po"));
        mMap.addMarker(new MarkerOptions().position(NShellCPRSTO).title("Shell - 731 - 737 Castle Peak Road, Tai Wo Hau (Towards Tuen Mun)"));
        mMap.addMarker(new MarkerOptions().position(NShellCWKS).title("Shell - 84 - 92 Chai Wan Kok Street, Tsuen Wan"));
        mMap.addMarker(new MarkerOptions().position(NShellCPRSET).title("Shell - 682 - 688 Castle Peak Road, Tai Wo Hau (Towards Kowloon)"));
        mMap.addMarker(new MarkerOptions().position(NShellLSS).title("Shell - 5-7 Lok Shing Street, Sha Tin City One, Sha Tin"));
        mMap.addMarker(new MarkerOptions().position(NShellCPRHSK).title("Shell - Castle Peak Road, Hung Shui Kiu, Yuen Longo"));
        mMap.addMarker(new MarkerOptions().position(NShellTWR).title("Shell - 5 Tsun Wen Road, Tuen Mun"));
        mMap.addMarker(new MarkerOptions().position(NShellTYSTR).title("Shell - 20 Tong Yan San Tsuen Road, Yuen Long"));
        mMap.addMarker(new MarkerOptions().position(NShellCPRPS).title("Shell - 11 Castle Peak Road, Ping Shan, Yuen Long"));
        mMap.addMarker(new MarkerOptions().position(NShellTYRW).title("Shell - 173 Tsing Yi Road West, Tsing Yi"));
        mMap.addMarker(new MarkerOptions().position(NShellPHR).title("Shell - 100 Po Hong Road, Tseung Kwan O"));
        mMap.addMarker(new MarkerOptions().position(NShellKSR).title("Shell - Kam Sheung Road, Yuen Long  (Junction of Pak Heung Street)"));
        mMap.addMarker(new MarkerOptions().position(NShellOSL).title("Shell - 21 On Shan Lane, Ma On Shan"));
        mMap.addMarker(new MarkerOptions().position(NShellTPRSTH).title("Shell - 8500 Tai Po Road, Sha Tin Heights, Sha Tin"));
        mMap.addMarker(new MarkerOptions().position(NShellSTS).title("Shell - 3 San Tak Street, Tuen Mun"));
        mMap.addMarker(new MarkerOptions().position(NShellLOOSF).title("Shell - Lot No. 1174, Pik Uk, Clear Water Bay Road, Sai Kung"));
        mMap.addMarker(new MarkerOptions().position(NShellCLR).title("Shell - 6 Cheong Lin Road, H.K. International Airport, Chek Lap Kok"));
        mMap.addMarker(new MarkerOptions().position(NShellCWR).title("Shell - 20 Chun Wan Road, H.K. International Airport, Chek Lap Kok"));
        mMap.addMarker(new MarkerOptions().position(NShellSTKR).title("Shell - Sha Tau Kok Road, Lung Yeuk Tau, Fanling "));

        mMap.addMarker(new MarkerOptions().position(NSinopecBRW).title("Sinopec - 139 Bauhinia Road West, Pok Wai, Yuen Long (Near Fairview Park)"));
        mMap.addMarker(new MarkerOptions().position(NSinopecFPB).title("Sinopec - 2A Fairview Park Boulevard, Yuen Long"));
        mMap.addMarker(new MarkerOptions().position(NSinopecTYSYR).title("Sinopec - 9 Tong Yan San Tsuen Road, Ping Shan, Yuen Long (Retrofit LPG Filling Services)"));
        mMap.addMarker(new MarkerOptions().position(NSinopecCPRPS).title("Sinopec - 10 Castle Peak Road, Ping Shan, Yuen Long (Retrofit LPG Filling Services)"));
        mMap.addMarker(new MarkerOptions().position(NSinopecFHTR).title("Sinopec - 2 Fuk Hang Tsuen Road, Lam Tei, Tuen Mun (Retrofit LPG Filling Services)"));
        mMap.addMarker(new MarkerOptions().position(NSinopecPHR).title("Sinopec - 123 Ping Ha Road, Tin Shui Wai, Yuen Long (Retrofit LPG Filling Services)"));
        mMap.addMarker(new MarkerOptions().position(NSinopecONZ).title("Sinopec - 190 Kam Tin Road, Shek Kong, Yuen Long (Retrofit LPG Filling Services)"));
        mMap.addMarker(new MarkerOptions().position(NSinopecOS).title("Sinopec - 17 Kam Tin Road, Yuen Long (Retrofit LPG Filling Services)"));
        mMap.addMarker(new MarkerOptions().position(NSinopecSFA).title("Sinopec - 5 San Fung Avenue, Sheung Shui"));
        mMap.addMarker(new MarkerOptions().position(NSinopecSWR).title("Sinopec - 70 San Wan Road, Fanling Nam Wai, Fanling"));
        mMap.addMarker(new MarkerOptions().position(NSinopecYMR).title("Sinopec - 30 Yat Ming Road, Fanling (Retrofit LPG Filling Services)"));
        mMap.addMarker(new MarkerOptions().position(NSinopecLKR).title("Sinopec - 5 Lam Kam Road, Yuen Long"));
        mMap.addMarker(new MarkerOptions().position(NSinopecSKWR).title("Sinopec - 22 So Kwun Wat Road, So Kwun Tan, Tuen Mun"));
        mMap.addMarker(new MarkerOptions().position(NSinopecCPRSKW).title("Sinopec - 38 Castle Peak Rd, So Kwun Wat"));
        mMap.addMarker(new MarkerOptions().position(NSinopecYOS).title("Sinopec - 12 Yuen On Street, Sha Tin (Retrofit LPG Filling Services)"));
        mMap.addMarker(new MarkerOptions().position(NSinopecTPRTPK).title("Sinopec - 151 Tai Po Road, Tai Po Kau, Tai Po (Near Deerhill Bay) (Retrofit LPG Filling Services)"));
        mMap.addMarker(new MarkerOptions().position(NSinopecTYR).title("Sinopec - 55 Tsing Yi Road, Tsing Yi (Retrofit LPG Filling Services)"));
        mMap.addMarker(new MarkerOptions().position(NSinopecCPRKC).title("Sinopec - 690 Castle Peak Road, Kwai Chung (Retrofit LPG Filling Services)"));
        mMap.addMarker(new MarkerOptions().position(NSinopecHTS).title("Sinopec - Hei Tung Street, Tung Chung New Town, Lantau Island (Retrofit LPG Filling Services)"));
        mMap.addMarker(new MarkerOptions().position(NSinopecKCS).title("Sinopec - 1 Kwong Chun Street, Tai Po (Dedicated LPG Station)"));
        mMap.addMarker(new MarkerOptions().position(NSinopecKOS).title("Sinopec - 2 Kwai On Street, Kwai Chung (Dedicated LPG Station)"));
        mMap.addMarker(new MarkerOptions().position(NSinopecHYS).title("Sinopec - 11 Hang Yiu Street, Ma On Shan (Dedicated LPG Station)"));
        mMap.addMarker(new MarkerOptions().position(NSinopecTYSOS).title("Sinopec - 16 Tak Yip Street, Tung Tau Industrial Area, Yuen Long (Dedicated LPG Station)"));
        mMap.addMarker(new MarkerOptions().position(NSinopecTTR).title("Sinopec - 10 Ting Tai Road, Tai Po (Dedicated LPG Station)"));
        mMap.addMarker(new MarkerOptions().position(NSinopecTYSS).title("Sinopec - 6 Tuen Yan Street, Tuen Mun"));
    }

    @Override
    public void showMessage(String title, String message) {
        shoeMessage(title, message);
    }
}