package com.example.navigation.ui.home.favourite.ViewFavourite;

import com.android.volley.Response;
import com.example.navigation.ui.home.Message_Model_For_Record;
import com.example.navigation.ui.home.favourite.DeleteFavourite.FavouriteDeleteRepository;
import com.example.navigation.ui.home.favourite.FavouriteRecordModel.Retrofit_Favourite_Model;
import com.example.navigation.ui.home.favourite.FavouriteRecordModel.RouteFavourite_model;

import org.jetbrains.annotations.TestOnly;

import java.util.List;

public class FavouritePresenter {
    private FavouriteRepository recordRepository;
    private FavouriteDeleteRepository favouriteDeleteRepository;
    private Message_Model_For_Record messageModel;
    private View view;
    private Response<List<RouteFavourite_model>> response;
    @TestOnly
    public String getMessage() {
        return messageModel.getMessage();
    }
    @TestOnly
    public String getTitle() {
        return messageModel.getTitle();
    }
    public FavouritePresenter(View view){
        this.view = view;
        this.recordRepository = new FavouriteRepository();
        this.favouriteDeleteRepository = new FavouriteDeleteRepository();
        this.messageModel = new Message_Model_For_Record();
    }

    public void callAPI(){ recordRepository.API(this); }
    public void callAPI_delete(String s){ favouriteDeleteRepository.API_delete(this,s); }

    public void getResponseSuccess(List<Retrofit_Favourite_Model> body){
        view.addToFavouriteView(body);
    }

    public void ShowMessage(String title, String message){
        messageModel.setTitle(title);
        messageModel.setMessage(message);
        if (messageModel.getTitle() == null || messageModel.getMessage() == null) {
            return;
        }
        view.ShowMessage(messageModel.getTitle(),messageModel.getMessage());
    }

    public void ShowMessage(String title, int code) {
        messageModel.setTitle(title);
        messageModel.setMessage(messageModel.checkCodeNum(code));
        if (messageModel.getTitle() == null || messageModel.getMessage() == null) {
            return;
        }
        view.ShowMessage(messageModel.getTitle(),messageModel.getMessage());
    }

    public void ShowMessageForRefresh(String title, String message){
        messageModel.setTitle(title);
        messageModel.setMessage(message);
        if (messageModel.getTitle() == null || messageModel.getMessage() == null) {
            return;
        }
        view.ShowMessageAfterDelete(messageModel.getTitle(),messageModel.getMessage());
    }

    public interface View{
        void addToFavouriteView(List<Retrofit_Favourite_Model> list);
        void ShowMessage(String title, String message);
        void ShowMessageAfterDelete(String title, String message);
    }
}



