package com.example.navigation.ui.news;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import com.example.navigation.R;

public class NewsFragment extends Fragment implements TrafficnewsPresenter.View {
    public Trafficnews trafficnews;
    public TrafficnewsPresenter trafficnewsPresenter;
    public TextView tvNewsHeader, tvNewsNo, tvNewsDetail, tvNewsDate, tvNewsContent;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_news, container, false);
        trafficnews = new Trafficnews();
        trafficnewsPresenter = new TrafficnewsPresenter(trafficnews, this);

        tvNewsHeader = root.findViewById(R.id.tvNewsHeader);
        tvNewsDetail = root.findViewById(R.id.tvNewsDetail);
        tvNewsNo = root.findViewById(R.id.tvNewsNo);
        tvNewsDate = root.findViewById(R.id.tvNewsDate);
        tvNewsContent = root.findViewById(R.id.tvNewsContent);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        trafficnewsPresenter.callAPI();
        return root;
    }


    @Override
    public void updatetvNewsHeader_EN(String string) {
        tvNewsHeader.append(Html.fromHtml(string));
    }

    @Override
    public void updatetvNewsDetail_EN(String string) {
        tvNewsDetail.append(string);
    }

    @Override
    public void updatetvNewsNo(String string) {
        tvNewsNo.append(string);
    }

    @Override
    public void updatetvNewsDate(String string) {
        tvNewsDate.append(string);
    }

    @Override
    public void updatetvNewsContent_EN(String string) {
        tvNewsContent.append(string);
    }

    @Override
    public void showErrorMessage(String string, int i) {
        tvNewsHeader.setVisibility(i);
        tvNewsDetail.setVisibility(i);
        tvNewsNo.setVisibility(i);
        tvNewsDate.setVisibility(i);
        tvNewsContent.setVisibility(i);
        tvNewsContent.setVisibility(i);
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        builder.setMessage(string);
        builder.setTitle("Error");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();
    }

}