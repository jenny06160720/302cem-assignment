package com.example.navigation.ui.home.favourite.DeleteFavourite;

import com.example.navigation.ui.home.favourite.FavouriteInterface.Get_Favourite_Request_Interface;
import com.example.navigation.ui.home.favourite.FavouriteInterface.Get_Favourite_Request_Path_Interface;
import com.example.navigation.ui.home.favourite.FavouriteRecordModel.Retrofit_Favourite_Model;
import com.example.navigation.ui.home.favourite.ViewFavourite.FavouritePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavouriteDeleteRepository {
    public void API_delete(FavouritePresenter favouritePresenter, String s) {
        Get_Favourite_Request_Interface request = Get_Favourite_Request_Path_Interface.retrofit.create(Get_Favourite_Request_Interface.class);
        Call<Retrofit_Favourite_Model> delete = request.deleteFavouriteData(s);

        delete.enqueue(new Callback<Retrofit_Favourite_Model>() {
            @Override
            public void onResponse(Call<Retrofit_Favourite_Model> call, Response<Retrofit_Favourite_Model> response) {
                if(response.code() != 200){
                    favouritePresenter.ShowMessage("Delete Favourite Message",response.code());
                }else{
                    favouritePresenter.ShowMessageForRefresh("Delete Favourite Message","Successful");
                }
            }

            @Override
            public void onFailure(Call<Retrofit_Favourite_Model> call, Throwable t) {
                favouritePresenter.ShowMessage("Delete Favourite Message","Connect Server Fail");
            }
        });
    }
}
