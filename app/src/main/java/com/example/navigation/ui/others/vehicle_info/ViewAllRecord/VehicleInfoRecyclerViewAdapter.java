package com.example.navigation.ui.others.vehicle_info.ViewAllRecord;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.navigation.R;
import com.example.navigation.ui.others.vehicle_info.Retrofit_Model;
import java.util.ArrayList;
import java.util.List;

public class VehicleInfoRecyclerViewAdapter extends RecyclerView.Adapter<VehicleInfoRecyclerViewAdapter.ViewHolder> {
    private List<String> oldData = new ArrayList<>();;
    private List<Retrofit_Model> listData;
    VehicleInfoPresenter vehicleInfoPresenter;

    VehicleInfoRecyclerViewAdapter(List<Retrofit_Model> data, VehicleInfoPresenter infoPresenter) {
        listData = data;
        vehicleInfoPresenter = infoPresenter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item2, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Retrofit_Model obj = listData.get(position);
        oldData.add(obj.ID);
        holder.txtItem.setText("License No.: "+obj.License_Plate_Number+"\n\n"+"Type: "+obj.Type_Of_License_Plate+"\n");
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtItem;
        private Button bt_Detail, bt_Delete;

        ViewHolder(View itemView) {
            super(itemView);

            txtItem = (TextView) itemView.findViewById(R.id.tvInfoItem);
            bt_Detail = (Button) itemView.findViewById(R.id.bt_Detail);
            bt_Delete = (Button) itemView.findViewById(R.id.bt_Delete);

            bt_Detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    System.out.println("Edit Test " + oldData.get(getAdapterPosition()));
                    vehicleInfoPresenter.showSelectRecord(oldData.get(getAdapterPosition()));
                }
            });

            bt_Delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    vehicleInfoPresenter.callAPI_delete(oldData.get(getAdapterPosition()));
                }
            });
        }
    }
}
