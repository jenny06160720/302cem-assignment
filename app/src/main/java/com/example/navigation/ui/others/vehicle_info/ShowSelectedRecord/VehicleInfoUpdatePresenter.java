package com.example.navigation.ui.others.vehicle_info.ShowSelectedRecord;

import android.content.Intent;
import android.provider.CalendarContract;
import com.example.navigation.ui.others.vehicle_info.MessageModel;
import com.example.navigation.ui.others.vehicle_info.Retrofit_Model;

import org.jetbrains.annotations.TestOnly;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class VehicleInfoUpdatePresenter {
    private VehicleInfoUpdateRepository vehicleInfoUpdateRepository;
    private VehicleInfoDetailRepository vehicleInfoDetailRepository;
    private View view;
    private MessageModel messageModel;
    @TestOnly
    public String getMessage() {
        return messageModel.getMessage();
    }
    @TestOnly
    public String getTitle() {
        return messageModel.getTitle();
    }
    public VehicleInfoUpdatePresenter(View view) {
        this.view = view;
        this.vehicleInfoUpdateRepository = new VehicleInfoUpdateRepository();
        this.vehicleInfoDetailRepository = new VehicleInfoDetailRepository();
        this.messageModel = new MessageModel();
    }

    public void getRecord(String id) {
        vehicleInfoDetailRepository.API_editShowRecord(this, id);
    }

    public void getResponseSuccess(Retrofit_Model body) {
        view.addToView(body);
    }

    public void API_View_Message(String title, int code) {
        messageModel.setTitle(title);
        messageModel.setMessage(messageModel.checkCodeNum(code));
        if (messageModel.getTitle() == null || messageModel.getMessage() == null) {
            return;
        }
        view.showMessage(messageModel.getTitle(), messageModel.getMessage());
    }

    public void API_View_Message(String title, String code){
        messageModel.setTitle(title);
        messageModel.setMessage(code);
        if (messageModel.getTitle() == null || messageModel.getMessage() == null) {
            return;
        }
        view.showMessage(messageModel.getTitle(), messageModel.getMessage());
    }

    public void combineDate(int year, int month, int dayOfMonth, String field) {
        String sDate = year+"-"+(month+1)+"-"+dayOfMonth;
        view.setDate(sDate, field);
    }

    public void API_Create_Message(String title, String message) {
        messageModel.setTitle(title);
        messageModel.setMessage(message);
        if (messageModel.getTitle() == null || messageModel.getMessage() == null) {
            return;
        }
        view.showMessage(messageModel.getTitle(), messageModel.getMessage());
    }

    public void API_Create_Message(String title, int code) {
        messageModel.setTitle(title);
        messageModel.setMessage(messageModel.checkCodeNum(code));
        if (messageModel.getTitle() == null || messageModel.getMessage() == null) {
            return;
        }
        view.showMessage(messageModel.getTitle(), messageModel.getMessage());
    }

    public void goBackToList() {
        Intent goBack = new Intent().setClassName("com.example.navigation", "com.example.navigation.ui.others.vehicle_info.ViewAllRecord.VehicleInfo");
        view.showListAgain(goBack);
    }

    public void callAPI_edit(String id, String licenseNumber, String type,
                             String annualFee, String licenseExpiryDate, String licenseReminderDate,
                             String policyNumber, String insuranceExpiryDate, String insuranceReminderDate,
                             String lastEngineOilChange, String remindEngineOilChange, String lastGearboxOilChange,
                             String remindGearboxOilChange) throws ParseException {

        SimpleDateFormat DateSDF = new SimpleDateFormat("yyyy-MM-dd");
        String updateTime = DateSDF.format(new java.util.Date());
        Calendar calendar = Calendar.getInstance();
        Date dateAdd;

        dateAdd = DateSDF.parse(licenseExpiryDate);
        calendar.setTime(dateAdd);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        licenseExpiryDate = DateSDF.format(dateAdd);

        dateAdd = DateSDF.parse(licenseReminderDate);
        calendar.setTime(dateAdd);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        licenseReminderDate = DateSDF.format(dateAdd);

        dateAdd = DateSDF.parse(insuranceExpiryDate);
        calendar.setTime(dateAdd);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        insuranceExpiryDate = DateSDF.format(dateAdd);

        dateAdd = DateSDF.parse(insuranceReminderDate);
        calendar.setTime(dateAdd);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        insuranceReminderDate = DateSDF.format(dateAdd);

        dateAdd = DateSDF.parse(lastEngineOilChange);
        calendar.setTime(dateAdd);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        lastEngineOilChange = DateSDF.format(dateAdd);

        dateAdd = DateSDF.parse(remindEngineOilChange);
        calendar.setTime(dateAdd);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        remindEngineOilChange = DateSDF.format(dateAdd);

        dateAdd = DateSDF.parse(lastGearboxOilChange);
        calendar.setTime(dateAdd);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        lastGearboxOilChange = DateSDF.format(dateAdd);

        dateAdd = DateSDF.parse(remindGearboxOilChange);
        calendar.setTime(dateAdd);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        remindGearboxOilChange = DateSDF.format(dateAdd);

        vehicleInfoUpdateRepository.API_edit(this,
                id, licenseNumber, type, annualFee, licenseExpiryDate, licenseReminderDate,
                policyNumber, insuranceExpiryDate, insuranceReminderDate,
                lastEngineOilChange, remindEngineOilChange, lastGearboxOilChange,
                remindGearboxOilChange, updateTime);
    }

    public void saveToCalender(String title, String date) {
        try {
            DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
            Date getDate = f.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(getDate);
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            Intent calendarIntent = new Intent(Intent.ACTION_INSERT, CalendarContract.Events.CONTENT_URI);
            calendar.set(year, month, day);
            calendarIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, calendar.getTimeInMillis());
            calendar.set(year, month, day + 1);
            calendarIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, calendar.getTimeInMillis());
            calendarIntent.putExtra(CalendarContract.Events.ALL_DAY, true);
            calendarIntent.putExtra(CalendarContract.Events.TITLE, title);

            view.addToCalendar(calendarIntent);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public interface View {
        void addToView(Retrofit_Model data);
        void showMessage(String title, String string);
        void showListAgain(Intent goBack);
        void addToCalendar(Intent calendarIntent);
        void setDate(String sDate, String field);
    }
}
