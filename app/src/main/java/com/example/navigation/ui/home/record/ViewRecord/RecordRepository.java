package com.example.navigation.ui.home.record.ViewRecord;

import com.example.navigation.ui.home.record.RecordInterface.Get_Record_Request_Interface;
import com.example.navigation.ui.home.record.RecordInterface.Get_Record_Request_Path_Interface;
import com.example.navigation.ui.home.record.RecordModel.Retrofit_Record_Model;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecordRepository {
    public void API(RecordPresenter recordPresenter){
        Get_Record_Request_Interface request = Get_Record_Request_Path_Interface.retrofit.create(Get_Record_Request_Interface.class);
        Call<List<Retrofit_Record_Model>> call = request.getAllLicense();

        call.enqueue(new Callback<List<Retrofit_Record_Model>>() {
            @Override
            public void onResponse(Call<List<Retrofit_Record_Model>> call, Response<List<Retrofit_Record_Model>> response) {
                if(response.code() == 200){
                    recordPresenter.getResponseSuccess(response.body());
                }else{
                    recordPresenter.ShowMessage("Message Of View All Record",response.code());
                }
            }

            @Override
            public void onFailure(Call<List<Retrofit_Record_Model>> call, Throwable t) {
                recordPresenter.ShowMessage("Message Of View All Record","Connect Server Fail");
            }
        });
    }
}
