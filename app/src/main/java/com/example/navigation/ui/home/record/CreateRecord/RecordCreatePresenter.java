package com.example.navigation.ui.home.record.CreateRecord;


import com.example.navigation.ui.home.Message_Model_For_Record;

import org.jetbrains.annotations.TestOnly;

public class RecordCreatePresenter{
    private RecordCreateRepository recordCreateRepository = new RecordCreateRepository();
    private View view;
    private Message_Model_For_Record messageModel;
    @TestOnly
    public String getMessage() {
        return messageModel.getMessage();
    }
    @TestOnly
    public String getTitle() {
        return messageModel.getTitle();
    }

    public RecordCreatePresenter(View view){
        this.view = view;
        this.messageModel = new Message_Model_For_Record();
    }

    public void callAPI_create(String record) {
        recordCreateRepository.API_create(view, record);
    }

    public void showMessage(String title, int code) {
        messageModel.setTitle(title);
        messageModel.setMessage(messageModel.checkCodeNum(code));
        if (messageModel.getTitle() == null || messageModel.getMessage() == null) {
            return;
        }
        view.showMessage(messageModel.getTitle(),messageModel.getMessage());
    }

    public void showMessage(String title, String message) {
        messageModel.setTitle(title);
        messageModel.setMessage(message);
        if (messageModel.getTitle() == null || messageModel.getMessage() == null) {
            return;
        }
        view.showMessage(messageModel.getTitle(),messageModel.getMessage());
    }

    public interface View{
        void showMessage(String title,String message);
    }
}
