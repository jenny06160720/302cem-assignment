package com.example.navigation.ui.others.vehicle_info.ViewAllRecord;

import android.content.Intent;
import com.example.navigation.ui.others.vehicle_info.MessageModel;
import com.example.navigation.ui.others.vehicle_info.DeleteRecord.VehicleInfoDeleteRepository;
import com.example.navigation.ui.others.vehicle_info.Retrofit_Model;
import org.jetbrains.annotations.TestOnly;
import java.util.List;
import retrofit2.Response;

public class VehicleInfoPresenter {
    private VehicleInfoRepository vehicleInfoRepository;
    private VehicleInfoDeleteRepository vehicleInfoDeleteRepository;
    private View view;
    private Response<List<VehicleInfoModel>> response;
    private MessageModel messageModel;

    @TestOnly
    public String getMessage() {
        return messageModel.getMessage();
    }
    @TestOnly
    public String getTitle() {
        return messageModel.getTitle();
    }

    public VehicleInfoPresenter(View view) {
        //  this.model = new Car_Detail_Model();
        this.view = view;
        this.vehicleInfoRepository = new VehicleInfoRepository();
        this.vehicleInfoDeleteRepository = new VehicleInfoDeleteRepository();
        this.messageModel = new MessageModel();
    }

    public void callAPI() {
//        View all record
        vehicleInfoRepository.API(this);
    }

    public void callAPI_delete(String deleteByID) {
        if (deleteByID.equals(""))
            API_View_Message("Delete Record Message","Cannot Get ID For Delete Record");
        else
            vehicleInfoDeleteRepository.API_delete(this, deleteByID);
    }

    public void API_View_Message(String title, String message) {
        messageModel.setTitle(title);
        messageModel.setMessage(message);
        if (messageModel.getTitle() == null || messageModel.getMessage() == null) {
            return;
        }
        view.showMessage(messageModel.getTitle(),messageModel.getMessage());
    }

    public void API_View_Message(String title, int code) {
        messageModel.setTitle(title);
        messageModel.setMessage(messageModel.checkCodeNum(code));
        if (messageModel.getTitle() == null || messageModel.getMessage() == null) {
            return;
        }
        view.showMessage(messageModel.getTitle(),messageModel.getMessage());
    }

    public void createForm() {
        // View - Click to show create record activity
        Intent createItem = new Intent().setClassName("com.example.navigation", "com.example.navigation.ui.others.vehicle_info.CreateRecord.VehicleInfoCreate");
        view.createNewPage(createItem);
    }

    public void getResponseSuccess(List<Retrofit_Model> body) {
        view.addToView(body);
    }

    public void afterDelete(String deleteMessage) {
        messageModel.setMessage(deleteMessage);
        if (messageModel.getMessage() == null) {
            return;
        }
        view.refreshResult(messageModel.getMessage());
    }

    public void showSelectRecord(String id) {
        if(id.equals("")){return;}
        Intent showRecord = new Intent().setClassName("com.example.navigation", "com.example.navigation.ui.others.vehicle_info.ShowSelectedRecord.VehicleInfoDetailUpdate");
        showRecord.putExtra("id", id);
        view.showRecordPage(showRecord);
    }

    public interface View {
        void addToView(List<Retrofit_Model> list);
        void refreshResult(String string);
        void createNewPage(Intent intent);
        void showMessage(String title,String string);
        void showRecordPage(Intent showRecord);
    }
}
