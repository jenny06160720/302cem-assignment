package com.example.navigation.ui.others.vehicle_info;

import com.google.gson.annotations.SerializedName;

public class Retrofit_Model {
    @SerializedName("_id")
    public String ID;
    @SerializedName("License_Plate_Number")
    public String License_Plate_Number;
    @SerializedName("Type_Of_License_Plate")
    public String Type_Of_License_Plate;
    @SerializedName("Fee_Of_Vehicle_License")
    public String Fee_Of_Vehicle_License;
    @SerializedName("Expiry_Date_Of_Vehicle_License")
    public String Expiry_Date_Of_Vehicle_License;
    @SerializedName("Reminder_Date_Of_Vehicle_License")
    public String Reminder_Date_Of_Vehicle_License;
    @SerializedName("Vehicle_Insurance_Number")
    public String Vehicle_Insurance_Number;
    @SerializedName("Expiry_Date_Of_Vehicle_Insurance")
    public String Expiry_Date_Of_Vehicle_Insurance;
    @SerializedName("Reminder_Date_Of_Vehicle_Insurance")
    public String Reminder_Date_Of_Vehicle_Insurance;
    @SerializedName("Date_Of_Engine_Oil_Change")
    public String Date_Of_Engine_Oil_Change;
    @SerializedName("Reminder_Date_Of_Engine_Oil_Change")
    public String Reminder_Date_Of_Engine_Oil_Change;
    @SerializedName("Date_Of_Gearbox_Oil_Change")
    public String Date_Of_Gearbox_Oil_Change;
    @SerializedName("Reminder_Date_Of_Gearbox_Oil_Change")
    public String Reminder_Date_Of_Gearbox_Oil_Change;
    @SerializedName("Create_Time")
    public String Create_Time;
    @SerializedName("Update_Time")
    public String Update_Time;
    @SerializedName("body")
    private String text;
    public Retrofit_Model(){}
    public Retrofit_Model(String id, String licenseNumber, String type, String annualFee, String licenseExpiryDate, String licenseReminderDate,
                          String policyNumber, String insuranceExpiryDate, String insuranceReminderDate, String lastEngineOilChange,
                          String remindEngineOilChange, String lastGearboxOilChange, String remindGearboxOilChange, String Create_Time,String Update_Time) {
        this.ID = id;
        this.License_Plate_Number = licenseNumber;
        this.Type_Of_License_Plate = type;
        this.Fee_Of_Vehicle_License = annualFee;
        this.Expiry_Date_Of_Vehicle_License = licenseExpiryDate;
        this.Reminder_Date_Of_Vehicle_License = licenseReminderDate;
        this.Vehicle_Insurance_Number = policyNumber;
        this.Expiry_Date_Of_Vehicle_Insurance = insuranceExpiryDate;
        this.Reminder_Date_Of_Vehicle_Insurance = insuranceReminderDate;
        this.Date_Of_Engine_Oil_Change = lastEngineOilChange;
        this.Reminder_Date_Of_Engine_Oil_Change = remindEngineOilChange;
        this.Date_Of_Gearbox_Oil_Change = lastGearboxOilChange;
        this.Reminder_Date_Of_Gearbox_Oil_Change = remindGearboxOilChange;
        this.Create_Time = Create_Time;
        this.Update_Time = Update_Time;
    }


    public void setID(String id){ this.ID = id; }
    public void setLicense_Plate_Number(String value){ this.License_Plate_Number = value; }
    public void setType_Of_License_Plate(String value){ this.Type_Of_License_Plate = value; }
    public void setFee_Of_Vehicle_License(String value){this.Fee_Of_Vehicle_License = value;}
    public void setExpiry_Date_Of_Vehicle_License(String value){this.Expiry_Date_Of_Vehicle_License =value;}
    public void setReminder_Date_Of_Vehicle_License(String value){this.Reminder_Date_Of_Vehicle_License = value;}
    public void setVehicle_Insurance_Number(String s){this.Vehicle_Insurance_Number = s;}
    public void setExpiry_Date_Of_Vehicle_Insurance(String value){this.Expiry_Date_Of_Vehicle_Insurance = value;}
    public void setReminder_Date_Of_Vehicle_Insurance(String value){this.Reminder_Date_Of_Vehicle_Insurance= value;}
    public void setDate_Of_Engine_Oil_Change(String value){this.Date_Of_Engine_Oil_Change = value;}
    public void setReminder_Date_Of_Engine_Oil_Change(String value){this.Reminder_Date_Of_Engine_Oil_Change = value;}
    public void setDate_Of_Gearbox_Oil_Change(String value){this.Date_Of_Gearbox_Oil_Change = value;}
    public void setReminder_Date_Of_Gearbox_Oil_Change(String value){this.Reminder_Date_Of_Gearbox_Oil_Change = value;}
    public void setCreate_Time(String value){this.Create_Time = value;}
    public void setUpdate_Time(String value){this.Update_Time = value;}

    public String getID(){return this.ID;}
    public String getLicense_Plate_Number(){return this.License_Plate_Number;}
    public String getType_Of_License_Plate(){return this.Type_Of_License_Plate;}
    public String getFee_Of_Vehicle_License(){return this.Fee_Of_Vehicle_License;}
    public String getExpiry_Date_Of_Vehicle_License(){return this.Expiry_Date_Of_Vehicle_License;}
    public String getReminder_Date_Of_Vehicle_License(){return this.Reminder_Date_Of_Vehicle_License;}
    public String getVehicle_Insurance_Number(){return this.Vehicle_Insurance_Number;}
    public String getExpiry_Date_Of_Vehicle_Insurance(){return this.Expiry_Date_Of_Vehicle_Insurance;}
    public String getReminder_Date_Of_Vehicle_Insurance(){return this.Reminder_Date_Of_Vehicle_Insurance;}
    public String getDate_Of_Engine_Oil_Change(){return this.Date_Of_Engine_Oil_Change;}
    public String getReminder_Date_Of_Engine_Oil_Change(){return this.Reminder_Date_Of_Engine_Oil_Change;}
    public String getDate_Of_Gearbox_Oil_Change(){return this.Date_Of_Gearbox_Oil_Change;}
    public String getReminder_Date_Of_Gearbox_Oil_Change(){return this.Reminder_Date_Of_Gearbox_Oil_Change;}
    public String getCreate_Time(){return this.Create_Time;}
    public String getUpdate_Time(){return this.Update_Time;}
}
