package com.example.navigation.ui.others.vehicle_info.CreateRecord;

import com.example.navigation.ui.others.vehicle_info.Get_Request_Interface;
import com.example.navigation.ui.others.vehicle_info.Get_Request_Path_Interface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VehicleInfoCreateRepository {
    public void API_create(VehicleInfoCreatePresenter vehicleInfoCreatePresenter, String licenseNumber, String type, String annualFee, String licenseExpiryDate, String licenseReminderDate, String policyNumber, String insuranceExpiryDate, String insuranceReminderDate, String lastEngineOilChange, String remindEngineOilChange, String lastGearboxOilChange, String remindGearboxOilChange) {
        VehicleInfoCreateModel post = new VehicleInfoCreateModel(licenseNumber, type, annualFee, licenseExpiryDate, licenseReminderDate, policyNumber, insuranceExpiryDate, insuranceReminderDate, lastEngineOilChange, remindEngineOilChange, lastGearboxOilChange, remindGearboxOilChange);
        Get_Request_Interface request = Get_Request_Path_Interface.retrofit.create(Get_Request_Interface.class);
        System.out.println("API_create: " + licenseNumber + type + annualFee + licenseExpiryDate + licenseReminderDate + policyNumber + insuranceExpiryDate + insuranceReminderDate + lastEngineOilChange + remindEngineOilChange + lastGearboxOilChange + remindGearboxOilChange);
        System.out.println("\n\n\n"+post.toString());
        Call<VehicleInfoCreateModel> create = request.createPost(post);

        create.enqueue(new Callback<VehicleInfoCreateModel>() {
            @Override
            public void onResponse(Call<VehicleInfoCreateModel> call, Response<VehicleInfoCreateModel> response) {
                System.out.println("Create " + response.code());
                if(response.code() == 200){
                    vehicleInfoCreatePresenter.API_Create_Message("Message Of Create Record","Success to create record!!");
                }else{
                    vehicleInfoCreatePresenter.API_Create_Message("Message Of Create Record",response.code());
                }
            }

            @Override
            public void onFailure(Call<VehicleInfoCreateModel> call, Throwable t) {
                System.out.println("response error " + t);
                vehicleInfoCreatePresenter.API_Create_Message("Message Of Create Record","Connect Data Fail");
            }
        });
    }
}
