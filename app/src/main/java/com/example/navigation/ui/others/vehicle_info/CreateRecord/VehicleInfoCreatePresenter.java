package com.example.navigation.ui.others.vehicle_info.CreateRecord;

import android.content.Intent;
import com.example.navigation.ui.others.vehicle_info.MessageModel;
import org.jetbrains.annotations.TestOnly;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class VehicleInfoCreatePresenter {
    private VehicleInfoCreateRepository vehicleInfoCreateRepository;
    private VehicleInfoCreateModel model;
    private MessageModel messageModel;
    private View view;

    @TestOnly
    public String getMessage() {
        return messageModel.getMessage();
    }
    @TestOnly
    public String getTitle() {
        return messageModel.getTitle();
    }

    public VehicleInfoCreatePresenter(View view) {
        this.view = view;
        this.vehicleInfoCreateRepository = new VehicleInfoCreateRepository();
        this.messageModel = new MessageModel();
    }

    public void callAPI_create(
            String licenseNumber, String type,
            String annualFee, String licenseExpiryDate,
            String licenseReminderDate, String policyNumber,
            String insuranceExpiryDate, String insuranceReminderDate,
            String lastEngineOilChange, String remindEngineOilChange,
            String lastGearboxOilChange, String remindGearboxOilChange) throws ParseException {
        if (licenseNumber.equals("") || type.equals("") || annualFee.equals("") || licenseExpiryDate.equals("") || licenseReminderDate.equals("") ||
                policyNumber.equals("") || insuranceExpiryDate.equals("") || insuranceReminderDate.equals("") || lastEngineOilChange.equals("") ||
                remindEngineOilChange.equals("") || lastGearboxOilChange.equals("") || remindGearboxOilChange.equals("")) {
            API_Create_Input_Message("Message","No Blank Input!");
        } else {
            SimpleDateFormat DateSDF = new SimpleDateFormat("yyyy-MM-dd");
            Calendar calendar = Calendar.getInstance();
            Date dateAdd;

            dateAdd = DateSDF.parse(licenseExpiryDate);
            calendar.setTime(dateAdd);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            licenseExpiryDate = DateSDF.format(dateAdd);

            dateAdd = DateSDF.parse(licenseReminderDate);
            calendar.setTime(dateAdd);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            licenseReminderDate = DateSDF.format(dateAdd);

            dateAdd = DateSDF.parse(insuranceExpiryDate);
            calendar.setTime(dateAdd);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            insuranceExpiryDate = DateSDF.format(dateAdd);

            dateAdd = DateSDF.parse(insuranceReminderDate);
            calendar.setTime(dateAdd);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            insuranceReminderDate = DateSDF.format(dateAdd);

            dateAdd = DateSDF.parse(lastEngineOilChange);
            calendar.setTime(dateAdd);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            lastEngineOilChange = DateSDF.format(dateAdd);

            dateAdd = DateSDF.parse(remindEngineOilChange);
            calendar.setTime(dateAdd);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            remindEngineOilChange = DateSDF.format(dateAdd);

            dateAdd = DateSDF.parse(lastGearboxOilChange);
            calendar.setTime(dateAdd);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            lastGearboxOilChange = DateSDF.format(dateAdd);

            dateAdd = DateSDF.parse(remindGearboxOilChange);
            calendar.setTime(dateAdd);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            remindGearboxOilChange = DateSDF.format(dateAdd);

            vehicleInfoCreateRepository.API_create(this,
                    licenseNumber, type, annualFee, licenseExpiryDate, licenseReminderDate,
                    policyNumber, insuranceExpiryDate, insuranceReminderDate, lastEngineOilChange,
                    remindEngineOilChange, lastGearboxOilChange, remindGearboxOilChange);
        }
    }

    public void goBackToList() {
        Intent goBack = new Intent().setClassName("com.example.navigation", "com.example.navigation.ui.others.vehicle_info.ViewAllRecord.VehicleInfo");
        view.showListAgain(goBack);
    }

    public void API_Create_Input_Message(String title, String message) {
        messageModel.setTitle(title);
        messageModel.setMessage(message);
        if (messageModel.getTitle() == null || messageModel.getMessage() == null) {
            return;
        }
        view.showInputMessage(messageModel.getTitle(),messageModel.getMessage());
    }

    public void API_Create_Message(String title, String message) {
        messageModel.setTitle(title);
        messageModel.setMessage(message);
        if (messageModel.getTitle() == null || messageModel.getMessage() == null) {
            return;
        }
        view.showMessage(messageModel.getTitle(),messageModel.getMessage());
    }

    public void API_Create_Message(String title, int code) {
        messageModel.setTitle(title);
        messageModel.setMessage(messageModel.checkCodeNum(code));
        if (messageModel.getTitle() == null || messageModel.getMessage() == null) {
            return;
        }
        view.showMessage(messageModel.getTitle(),messageModel.getMessage());
    }

    public void combineDate(int year, int month, int dayOfMonth, String field) {
        String sDate = year+"-"+(month+1)+"-"+dayOfMonth;
        view.setDate(sDate, field);
    }

    public interface View {
        void showListAgain(Intent goBack);
        void showMessage(String title, String message);
        void showInputMessage(String title, String message);
        void setDate(String sDate, String field);
    }


}
