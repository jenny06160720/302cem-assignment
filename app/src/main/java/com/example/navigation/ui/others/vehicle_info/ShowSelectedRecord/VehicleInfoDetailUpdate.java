package com.example.navigation.ui.others.vehicle_info.ShowSelectedRecord;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import com.example.navigation.R;
import com.example.navigation.ui.others.vehicle_info.Retrofit_Model;
import java.text.ParseException;
import java.util.Calendar;

public class VehicleInfoDetailUpdate extends AppCompatActivity implements VehicleInfoUpdatePresenter.View {
    private VehicleInfoUpdatePresenter vehicleInfoUpdatePresenter;
    private Button bt_update, bt_save;
    private Button bt_addToCalendarLicenseExpiryDate, bt_addToCalendarLicenseReminderDate, bt_addToCalendarInsuranceExpiryDate, bt_addToCalendarInsuranceReminderDate, bt_addToCalendarRemindEngineOilChange, bt_addToCalendarRemindGearboxOilChange;
    private TextView tv_id, tv_updateMode;
    private EditText et_licenseNumber, et_type, et_annualFee, et_licenseExpiryDate, et_licenseReminderDate, et_policyNumber, et_insuranceExpiryDate, et_insuranceReminderDate, et_lastEngineOilChange, et_remindEngineOilChange, et_lastGearboxOilChange, et_remindGearboxOilChange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_detail_edit);
        Bundle bundle = getIntent().getExtras();
        String id = bundle.getString("id");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Record Detail");
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        vehicleInfoUpdatePresenter = new VehicleInfoUpdatePresenter(this);

        tv_id = findViewById(R.id.tv_id);
        tv_updateMode = findViewById(R.id.tv_updateMode);
        tv_id.setText(id);
        et_licenseNumber = findViewById(R.id.et_licenseNumber);
        et_type = findViewById(R.id.et_type);
        et_annualFee = findViewById(R.id.et_annualFee);
        et_licenseExpiryDate = findViewById(R.id.et_licenseExpiryDate);
        et_licenseReminderDate = findViewById(R.id.et_licenseReminderDate);
        et_policyNumber = findViewById(R.id.et_policyNumber);
        et_insuranceExpiryDate = findViewById(R.id.et_insuranceExpiryDate);
        et_insuranceReminderDate = findViewById(R.id.et_insuranceReminderDate);
        et_lastEngineOilChange = findViewById(R.id.et_lastEngineOilChange);
        et_remindEngineOilChange = findViewById(R.id.et_remindEngineOilChange);
        et_lastGearboxOilChange = findViewById(R.id.et_lastGearboxOilChange);
        et_remindGearboxOilChange = findViewById(R.id.et_remindGearboxOilChange);

        vehicleInfoUpdatePresenter.getRecord(id);

        bt_addToCalendarLicenseExpiryDate = findViewById(R.id.bt_addToCalendarLicenseExpiryDate);
        bt_addToCalendarLicenseReminderDate = findViewById(R.id.bt_addToCalendarLicenseRemindDate);
        bt_addToCalendarInsuranceExpiryDate = findViewById(R.id.bt_addToCalendarInsuranceExpiryDate);
        bt_addToCalendarInsuranceReminderDate = findViewById(R.id.bt_addToCalendarInsuranceRemindDate);
        bt_addToCalendarRemindEngineOilChange = findViewById(R.id.bt_addToCalendarRemindEngineOilChange);
        bt_addToCalendarRemindGearboxOilChange = findViewById(R.id.bt_addToCalendarRemindGearboxOilChange);

        bt_update = findViewById(R.id.bt_Update);
        bt_save = findViewById(R.id.bt_Save);

        bt_addToCalendarLicenseExpiryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = "License Expiry Date";
                String date = et_licenseExpiryDate.getText().toString();
                vehicleInfoUpdatePresenter.saveToCalender(title, date);
            }
        });

        bt_addToCalendarLicenseReminderDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = "License Expiry Reminder";
                String date = et_licenseReminderDate.getText().toString();
                vehicleInfoUpdatePresenter.saveToCalender(title, date);
            }
        });

        bt_addToCalendarInsuranceExpiryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = "Insurance Expiry Date";
                String date = et_insuranceExpiryDate.getText().toString();
                vehicleInfoUpdatePresenter.saveToCalender(title, date);
            }
        });

        bt_addToCalendarInsuranceReminderDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = "Insurance Expiry Reminder";
                String date = et_insuranceReminderDate.getText().toString();
                vehicleInfoUpdatePresenter.saveToCalender(title, date);
            }
        });

        bt_addToCalendarRemindEngineOilChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = "Engine Oil Change Reminder";
                String date = et_remindEngineOilChange.getText().toString();
                vehicleInfoUpdatePresenter.saveToCalender(title, date);
            }
        });

        bt_addToCalendarRemindGearboxOilChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = "Gearbox Oil Change Reminder";
                String date = et_remindGearboxOilChange.getText().toString();
                vehicleInfoUpdatePresenter.saveToCalender(title, date);
            }
        });

        bt_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTitle("Update Record");
                tv_updateMode.setVisibility(View.VISIBLE);
                bt_update.setVisibility(View.INVISIBLE);
                bt_update.setEnabled(false);
                bt_update.setClickable(false);
                bt_save.setVisibility(View.VISIBLE);
                bt_save.setEnabled(true);
                bt_save.setClickable(true);
                bt_addToCalendarLicenseExpiryDate.setVisibility(View.GONE);
                bt_addToCalendarLicenseExpiryDate.setEnabled(false);
                bt_addToCalendarLicenseExpiryDate.setClickable(false);
                bt_addToCalendarLicenseReminderDate.setVisibility(View.GONE);
                bt_addToCalendarLicenseReminderDate.setEnabled(false);
                bt_addToCalendarLicenseReminderDate.setClickable(false);
                bt_addToCalendarInsuranceExpiryDate.setVisibility(View.GONE);
                bt_addToCalendarInsuranceExpiryDate.setEnabled(false);
                bt_addToCalendarInsuranceExpiryDate.setClickable(false);
                bt_addToCalendarInsuranceReminderDate.setVisibility(View.GONE);
                bt_addToCalendarInsuranceReminderDate.setEnabled(false);
                bt_addToCalendarInsuranceReminderDate.setClickable(false);
                bt_addToCalendarRemindEngineOilChange.setVisibility(View.GONE);
                bt_addToCalendarRemindEngineOilChange.setEnabled(false);
                bt_addToCalendarRemindEngineOilChange.setClickable(false);
                bt_addToCalendarRemindGearboxOilChange.setVisibility(View.GONE);
                bt_addToCalendarRemindGearboxOilChange.setEnabled(false);
                bt_addToCalendarRemindGearboxOilChange.setClickable(false);
                et_licenseNumber.setFocusable(true);
                et_licenseNumber.setFocusableInTouchMode(true);
                et_licenseNumber.setClickable(true);
                et_licenseNumber.setCursorVisible(true);
                et_type.setFocusable(true);
                et_type.setFocusableInTouchMode(true);
                et_type.setClickable(true);
                et_type.setCursorVisible(true);
                et_annualFee.setFocusable(true);
                et_annualFee.setFocusableInTouchMode(true);
                et_annualFee.setClickable(true);
                et_annualFee.setCursorVisible(true);
                et_licenseExpiryDate.setClickable(true);
                et_licenseReminderDate.setClickable(true);
                et_policyNumber.setFocusable(true);
                et_policyNumber.setFocusableInTouchMode(true);
                et_policyNumber.setClickable(true);
                et_policyNumber.setCursorVisible(true);
                et_insuranceExpiryDate.setClickable(true);
                et_insuranceReminderDate.setClickable(true);
                et_lastEngineOilChange.setClickable(true);
                et_remindEngineOilChange.setClickable(true);
                et_lastGearboxOilChange.setClickable(true);
                et_remindGearboxOilChange.setClickable(true);

                et_licenseExpiryDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String field = "licenseExpiryDate";
                        String showCalender = "future";
                        openCalendar(field, showCalender);
                    }
                });

                et_licenseReminderDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String field = "licenseReminderDate";
                        String showCalender = "future";
                        openCalendar(field, showCalender);
                    }
                });

                et_insuranceExpiryDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String field = "insuranceExpiryDate";
                        String showCalender = "future";
                        openCalendar(field, showCalender);
                    }
                });

                et_insuranceReminderDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String field = "insuranceReminderDate";
                        String showCalender = "future";
                        openCalendar(field, showCalender);
                    }
                });

                et_lastEngineOilChange.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String field = "lastEngineOilChange";
                        String showCalender = "past";
                        openCalendar(field, showCalender);
                    }
                });

                et_remindEngineOilChange.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String field = "remindEngineOilChange";
                        String showCalender = "future";
                        openCalendar(field, showCalender);
                    }
                });

                et_lastGearboxOilChange.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String field = "lastGearboxOilChange";
                        String showCalender = "past";
                        openCalendar(field, showCalender);
                    }
                });

                et_remindGearboxOilChange.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String field = "remindGearboxOilChange";
                        String showCalender = "future";
                        openCalendar(field, showCalender);
                    }
                });
            }
        });
        bt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = tv_id.getText().toString();
                String licenseNumber = et_licenseNumber.getText().toString();
                String type = et_type.getText().toString();
                String annualFee = et_annualFee.getText().toString();
                String licenseExpiryDate = et_licenseExpiryDate.getText().toString();
                String licenseReminderDate = et_licenseReminderDate.getText().toString();
                String policyNumber = et_policyNumber.getText().toString();
                String insuranceExpiryDate = et_insuranceExpiryDate.getText().toString();
                String insuranceReminderDate = et_insuranceReminderDate.getText().toString();
                String lastEngineOilChange = et_lastEngineOilChange.getText().toString();
                String remindEngineOilChange = et_remindEngineOilChange.getText().toString();
                String lastGearboxOilChange = et_lastGearboxOilChange.getText().toString();
                String remindGearboxOilChange = et_remindGearboxOilChange.getText().toString();

                try {
                    vehicleInfoUpdatePresenter.callAPI_edit(id, licenseNumber, type, annualFee, licenseExpiryDate, licenseReminderDate, policyNumber, insuranceExpiryDate, insuranceReminderDate
                            , lastEngineOilChange, remindEngineOilChange, lastGearboxOilChange, remindGearboxOilChange);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void showListAgain(Intent goBack) {
        startActivity(goBack);
    }

    @Override
    public void addToCalendar(Intent calendarIntent) {
        startActivity(calendarIntent);
    }

    @Override
    public void setDate(String sDate, String field) {
        if (field == "licenseExpiryDate")
            et_licenseExpiryDate.setText(sDate);
        else if (field == "licenseReminderDate")
            et_licenseReminderDate.setText(sDate);
        else if (field == "insuranceExpiryDate")
            et_insuranceExpiryDate.setText(sDate);
        else if (field == "insuranceReminderDate")
            et_insuranceReminderDate.setText(sDate);
        else if (field == "lastEngineOilChange")
            et_lastEngineOilChange.setText(sDate);
        else if (field == "remindEngineOilChange")
            et_remindEngineOilChange.setText(sDate);
        else if (field == "lastGearboxOilChange")
            et_lastGearboxOilChange.setText(sDate);
        else if (field == "remindGearboxOilChange")
            et_remindGearboxOilChange.setText(sDate);
    }

    @Override
    public void addToView(Retrofit_Model data) {
        tv_id.setText(data.ID);
        et_licenseNumber.setText(data.License_Plate_Number);
        et_type.setText(data.Type_Of_License_Plate);
        et_annualFee.setText(data.Fee_Of_Vehicle_License);
        et_licenseExpiryDate.setText(data.Expiry_Date_Of_Vehicle_License.substring(0,10));
        et_licenseReminderDate.setText(data.Reminder_Date_Of_Vehicle_License.substring(0,10));
        et_policyNumber.setText(data.Vehicle_Insurance_Number);
        et_insuranceExpiryDate.setText(data.Expiry_Date_Of_Vehicle_Insurance.substring(0,10));
        et_insuranceReminderDate.setText(data.Reminder_Date_Of_Vehicle_Insurance.substring(0,10));
        et_lastEngineOilChange.setText(data.Date_Of_Engine_Oil_Change.substring(0,10));
        et_remindEngineOilChange.setText(data.Reminder_Date_Of_Engine_Oil_Change.substring(0,10));
        et_lastGearboxOilChange.setText(data.Date_Of_Gearbox_Oil_Change.substring(0,10));
        et_remindGearboxOilChange.setText(data.Reminder_Date_Of_Gearbox_Oil_Change.substring(0,10));
    }

    private void openCalendar(String field, String showCalender) {
        Calendar calendar = Calendar.getInstance();

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(VehicleInfoDetailUpdate.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                vehicleInfoUpdatePresenter.combineDate(year, month, dayOfMonth, field);
            }
        }, year, month, day);

        if (showCalender == "future") {
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();
        }
        else if (showCalender == "past"){
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }
    }

    @Override
    public void showMessage(String title, String string) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(string);
        builder.setTitle(title);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                vehicleInfoUpdatePresenter.goBackToList();
            }
        });
        builder.create();
        builder.show();
    }
}
