package com.example.navigation.ui.home.record.RecordInterface;

import com.example.navigation.ui.home.record.RecordModel.Retrofit_Record_Model;
import com.example.navigation.ui.home.record.RecordModel.RouteRecordCreate_model;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface Get_Record_Request_Interface {
    @GET("/to")
    Call<List<Retrofit_Record_Model>> getAllLicense();
    @DELETE("/to/{toID}")
    Call<Retrofit_Record_Model> deleteAll(@Path("toID") String toID);
    @POST("/to")
    Call<RouteRecordCreate_model> createRecordItem(@Body RouteRecordCreate_model postRecord);
}