package com.example.navigation.ui.others.parking_info;

import android.content.Context;
import android.util.Log;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CarParkModel {
    private ArrayList<String> mData;
    private String ErrorMessage;

    public CarParkModel() {
        mData = new ArrayList<>();
    }

    public void setErrorMessage(String ErrorMessage) {
        this.ErrorMessage = ErrorMessage;
    }

    public String getErrorMessage() {
        return ErrorMessage;
    }

    public void setmData(ArrayList<String> mData) {
        this.mData = mData;
    }

    public ArrayList<String> getmData() {
        return mData;
    }

    public void API(CarParkPresenter carParkPresenter, Context context) {
        getInfoData(carParkPresenter, context);
    }

    private String getInfoData(CarParkPresenter carParkPresenter, Context context) {
        String carParkResult = "";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest("http://10.0.2.2:3000/getjson", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("回傳結果", "結果=" + response.toString());
                        try {
                            getCarParkInfo(response, carParkPresenter);
                        } catch (JSONException e) {
                            Log.e("回傳結果", "錯誤訊息：JSON");
                            carParkPresenter.sendErrorMessage("JSON");
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("回傳結果", "錯誤訊息：JSON - " + error.toString());
                carParkPresenter.sendErrorMessage("JSON");
            }
        });
        Volley.newRequestQueue(context).add(jsonObjectRequest);
        return carParkResult;
    }

    private void getCarParkInfo(JSONObject objectJson, CarParkPresenter carParkPresenter) throws JSONException {
        ArrayList<String> list = new ArrayList();
        JSONArray rs = objectJson.getJSONArray("results");

        if (rs.length() < 0) {
            carParkPresenter.sendErrorMessage("CountOfRecord");
        } else {
            for (int i = 0; i < rs.length(); i++) {
                JSONObject o = rs.getJSONObject(i);

                String str = "ID: " + o.getString("park_Id") + "\n";
                str += "Name: " + o.getString("name") + "\n";

                if (o.has("openingHours")) {
                    JSONObject carParkOpeningHours = o.getJSONArray("openingHours").getJSONObject(0);
                    if (!carParkOpeningHours.getString("periodStart").equals(carParkOpeningHours.getString("periodEnd"))) {
                        str += "Opening Hours: " + carParkOpeningHours.getString("periodStart") + " - " + carParkOpeningHours.getString("periodEnd") + "\n";
                    }
                }

                if (o.has("contactNo"))
                    str += "Contact No.: " + o.getString("contactNo") + "\n";

                if (o.has("paymentMethods")) {
                    str += "Payment: ";
                    JSONArray carParkPayment = o.getJSONArray("paymentMethods");
                    for (int x = 0; x < carParkPayment.length(); x++) {
                        str += (String) carParkPayment.get(x) + "    ";
                    }
                    str += "\n";
                }

                if (o.has("displayAddress"))
                    str += "Address: " + o.getString("displayAddress") + "\n";


                list.add(str);

            }
            carParkPresenter.addToView(list);
        }
    }
}
