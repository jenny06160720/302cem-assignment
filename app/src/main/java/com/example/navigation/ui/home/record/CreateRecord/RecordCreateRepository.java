package com.example.navigation.ui.home.record.CreateRecord;

import com.example.navigation.ui.home.record.RecordInterface.Get_Record_Request_Interface;
import com.example.navigation.ui.home.record.RecordInterface.Get_Record_Request_Path_Interface;
import com.example.navigation.ui.home.record.RecordModel.RouteRecordCreate_model;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecordCreateRepository {
    public void API_create(RecordCreatePresenter.View view, String record) {
        Get_Record_Request_Interface request = Get_Record_Request_Path_Interface.retrofit.create(Get_Record_Request_Interface.class);
        RouteRecordCreate_model postRecord = new RouteRecordCreate_model(record);
        Call<RouteRecordCreate_model> create = request.createRecordItem(postRecord);
        create.enqueue(new Callback<RouteRecordCreate_model>() {
            public RecordCreatePresenter recordCreatePresenter = new RecordCreatePresenter(view);

            @Override
            public void onResponse(Call<RouteRecordCreate_model> call, Response<RouteRecordCreate_model> response) {
                if (response.code() != 200) {
                    recordCreatePresenter.showMessage("Create Record Message", response.code());
                }
            }

            @Override
            public void onFailure(Call<RouteRecordCreate_model> call, Throwable t) {
                recordCreatePresenter.showMessage("Create Record Message", "Connect Server Fail");
            }
        });
    }
}
