package com.example.navigation.ui.others.vehicle_info.CreateRecord;

import com.google.gson.annotations.SerializedName;

public class VehicleInfoCreateModel {
    @SerializedName("ID")
    public String ID;
    @SerializedName("License_Plate_Number")
    public String License_Plate_Number;
    @SerializedName("Type_Of_License_Plate")
    public String Type_Of_License_Plate;
    @SerializedName("Fee_Of_Vehicle_License")
    public String Fee_Of_Vehicle_License;
    @SerializedName("Expiry_Date_Of_Vehicle_License")
    public String Expiry_Date_Of_Vehicle_License;
    @SerializedName("Reminder_Date_Of_Vehicle_License")
    public String Reminder_Date_Of_Vehicle_License;
    @SerializedName("Vehicle_Insurance_Number")
    public String Vehicle_Insurance_Number;
    @SerializedName("Expiry_Date_Of_Vehicle_Insurance")
    public String Expiry_Date_Of_Vehicle_Insurance;
    @SerializedName("Reminder_Date_Of_Vehicle_Insurance")
    public String Reminder_Date_Of_Vehicle_Insurance;
    @SerializedName("Date_Of_Engine_Oil_Change")
    public String Date_Of_Engine_Oil_Change;
    @SerializedName("Reminder_Date_Of_Engine_Oil_Change")
    public String Reminder_Date_Of_Engine_Oil_Change;
    @SerializedName("Date_Of_Gearbox_Oil_Change")
    public String Date_Of_Gearbox_Oil_Change;
    @SerializedName("Reminder_Date_Of_Gearbox_Oil_Change")
    public String Reminder_Date_Of_Gearbox_Oil_Change;
    @SerializedName("Create_Time")
    public String Create_Time;
    @SerializedName("Update_Time")
    public String Update_Time;
    @SerializedName("body")
    private String text;

    public VehicleInfoCreateModel(String licenseNumber, String type, String annualFee, String licenseExpiryDate, String licenseReminderDate, String policyNumber, String insuranceExpiryDate, String insuranceReminderDate, String lastEngineOilChange, String remindEngineOilChange, String lastGearboxOilChange, String remindGearboxOilChange) {
        this.License_Plate_Number = licenseNumber;
        this.Type_Of_License_Plate = type;
        this.Fee_Of_Vehicle_License = annualFee;
        this.Expiry_Date_Of_Vehicle_License = licenseExpiryDate;
        this.Reminder_Date_Of_Vehicle_License = licenseReminderDate;
        this.Vehicle_Insurance_Number = policyNumber;
        this.Expiry_Date_Of_Vehicle_Insurance = insuranceExpiryDate;
        this.Reminder_Date_Of_Vehicle_Insurance = insuranceReminderDate;
        this.Date_Of_Engine_Oil_Change = lastEngineOilChange;
        this.Reminder_Date_Of_Engine_Oil_Change = remindEngineOilChange;
        this.Date_Of_Gearbox_Oil_Change = lastGearboxOilChange;
        this.Reminder_Date_Of_Gearbox_Oil_Change = remindGearboxOilChange;
    }

    public void setID(String id){ this.ID = id; }
    public String getID(){return this.ID;}


}
