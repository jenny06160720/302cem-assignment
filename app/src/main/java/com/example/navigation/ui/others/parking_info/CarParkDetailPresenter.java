package com.example.navigation.ui.others.parking_info;

import android.content.Context;
import android.content.Intent;
import org.jetbrains.annotations.TestOnly;

public class CarParkDetailPresenter {
    private CarParkDetailModel model;
    private View view;

    public CarParkDetailPresenter(View view) {
        this.model = new CarParkDetailModel(this);
        this.view = view;
    }

    @TestOnly
    public String getBasicInfo(){return model.getBasicInfo();}

    @TestOnly
    public String getVacancy(){return model.getVacancy();}

    @TestOnly
    public String getErrorMessage() { return model.getErrorMessage();}

    public void putIncoming(Intent intent, Context context) {
        model.getIncoming(intent, context);
    }

    public void updateTextView_basicInfo(String s) {
        model.setBasicInfo(s);
        view.updateTextView(model.getBasicInfo());
    }

    public void updateTextView_vac(String s){
        model.setVacancy(s);
        view.updateTextView(model.getVacancy());
    }

    public void showErrorMsg(String s){
        String message = null;
        if(s.equals("No ID")){
            message = s;
        }else if(s.equals("Cannot Get JSON")){
            message = s;
        }else{
            message = "Error";
        }
        model.setErrorMessage(message);
        if(model.getErrorMessage() == null ) return;
        view.showError(model.getErrorMessage());
    }

    public interface View{
        void updateTextView(String s);
        void showError(String s);
    }
}
