package com.example.navigation.ui.others.vehicle_info.ViewAllRecord;

import com.example.navigation.ui.others.vehicle_info.Get_Request_Interface;
import com.example.navigation.ui.others.vehicle_info.Get_Request_Path_Interface;
import com.example.navigation.ui.others.vehicle_info.Retrofit_Model;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VehicleInfoRepository {
    public void API(VehicleInfoPresenter vehicleInfoPresenter) {
        Get_Request_Interface request = Get_Request_Path_Interface.retrofit.create(Get_Request_Interface.class);
        Call<List<Retrofit_Model>> call = request.getAllLicense();
        call.enqueue(new Callback<List<Retrofit_Model>>() {
            @Override
            public void onResponse(Call<List<Retrofit_Model>> call, Response<List<Retrofit_Model>> response) {
                System.out.println("response " + response.body());
                if(response.code() == 200){
                    vehicleInfoPresenter.getResponseSuccess(response.body());
                }else{
                    vehicleInfoPresenter.API_View_Message("Message Of View All Record",response.code());
                }
            }

            @Override
            public void onFailure(Call<List<Retrofit_Model>> call, Throwable t) {
                System.out.println("response error " + t);
                vehicleInfoPresenter.API_View_Message("Message","Fail to connect to data");
            }
        });
    }
}
