package com.example.navigation.ui.others.parking_info;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import com.example.navigation.R;
import java.util.ArrayList;

public class ParkingInfo extends AppCompatActivity implements CarParkPresenter.View{
    private RecyclerView recycler_view;
    private CarParkRecyclerViewAdapter adapter;

    private CarParkPresenter carParkPresenter;
    private ArrayList<String> mData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_info);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Car Park List");

        carParkPresenter = new CarParkPresenter(this);
        carParkPresenter.callAPI(this);
        Log.i("test", "Main: " + this);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        boolean flag = false;
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                flag = true;
                break;
            default:
                flag = super.onOptionsItemSelected(item);
                break;
        }
        return flag;
    }

    public void addToView(ArrayList<String> mData){
        Log.i("test", "Main: " + carParkPresenter.getmData().toString());
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view_carPark);
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        adapter = new CarParkRecyclerViewAdapter(mData, carParkPresenter);
        recycler_view.setAdapter(adapter);
    }

    @Override
    public void intent(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void showErrorMessage(String string) {
//        Toast.makeText(getBaseContext(), string, Toast.LENGTH_LONG).show();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(string);
        builder.setTitle("Error");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();
    }
}